package com.voodoo.lib_frame.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.voodoo.lib_frame.R;

public class LoadingView extends View {

    /**
     * 动画状态：加载中、成功、失败
     */
    public static final int STATUS_LOADING = 1;
    public static final int STATUS_SUCCESS = 2;
    public static final int STATUS_FAIL = 3;

    private int curStatus; // 当前动画的状态
    private int paintWidth = 3; // 画笔宽度

    /**
     * loading 动画变量
     */
    private PathMeasure mPathMeasure;
    private Path mDstPath;
    private int mCurRotate = 0;
    private float mProgress;
    private boolean hasCanvasSaved = false;
    private boolean hasCanvasRestored = false;

    /**
     * Success / Fail 动画变量
     */
    private PathMeasure mSuccessPathMeasure;
    private Path mSuccessDstPath;
    private PathMeasure mFailPathMeasure;
    private Path mFailDstPath;

    /**
     * 动画
     */
    private ValueAnimator mLoadingAnimator;
    private ValueAnimator mSuccessAnimator;
    private ValueAnimator mFailAnimator;

    private Paint mBluePaint;
    private Paint mRedPaint;

    private int mCenterX, mCenterY;

    private int loadingColor; // 加载中颜色
    private int loadingSuccessColor; // 加载成功颜色
    private int loadingFailColor; // 加载失败颜色

    public LoadingView(Context context) {
        this(context, null);
    }

    public LoadingView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LoadingView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        // 获取属性集合 TypedArray
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.MyLoadingView);
        // 获取颜色值，后面是默认值
        loadingColor = typedArray.getColor(R.styleable.MyLoadingView_loadingColor, Color.parseColor("#108DE7"));
        loadingSuccessColor = typedArray.getColor(R.styleable.MyLoadingView_loadingSuccessColor, Color.parseColor("#00B000"));
        loadingFailColor = typedArray.getColor(R.styleable.MyLoadingView_loadingFailColor, Color.parseColor("#DE267A"));

        init();
    }

    public void setLoadingColor(int loadingColor) {
        this.loadingColor = loadingColor;
    }

    public void setLoadingColor(ColorStateList colors) {
        this.loadingColor = colors.getColorForState(getDrawableState(), 0);
        init();
    }

    public void setLoadingSuccessColor(int loadingSuccessColor) {
        this.loadingSuccessColor = loadingSuccessColor;
    }

    public void setLoadingSuccessColor(ColorStateList colors) {
        this.loadingSuccessColor = colors.getColorForState(getDrawableState(), 0);
        init();
    }

    public void setLoadingFailColor(int loadingFailColor) {
        this.loadingFailColor = loadingFailColor;
    }

    public void setLoadingFailColor(ColorStateList colors) {
        this.loadingFailColor = colors.getColorForState(getDrawableState(), 0);
        init();
    }

    private void init() {
        // 取消硬件加速
        setLayerType(LAYER_TYPE_SOFTWARE, null);

        // 初始化画笔
        if (mBluePaint == null) {
            mBluePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        }
        mBluePaint.setColor(loadingColor);
        mBluePaint.setStyle(Paint.Style.STROKE);
        mBluePaint.setStrokeCap(Paint.Cap.ROUND);
//        mBluePaint.setStrokeWidth(paintWidth);

        if (mRedPaint == null) {
            mRedPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        }
        mRedPaint.setColor(loadingFailColor);
        mRedPaint.setStyle(Paint.Style.STROKE);
        mRedPaint.setStrokeCap(Paint.Cap.ROUND);
//        mRedPaint.setStrokeWidth(paintWidth);

        // 初始化时, 动画为加载状态
        curStatus = STATUS_LOADING;

        // 新建 Loading 动画并 start
        mLoadingAnimator = ValueAnimator.ofFloat(0, 1);
        mLoadingAnimator.setDuration(2000);
        mLoadingAnimator.addUpdateListener(animatorUpdateListener);
        mLoadingAnimator.addListener(animatorListener);
        mLoadingAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        mLoadingAnimator.setRepeatCount(ValueAnimator.INFINITE);
        mLoadingAnimator.setRepeatMode(ValueAnimator.RESTART);
        mLoadingAnimator.start();

        // 新建 success 动画
        mSuccessAnimator = ValueAnimator.ofFloat(0, 2);
        mSuccessAnimator.setDuration(1600);
        mSuccessAnimator.addUpdateListener(animatorUpdateListener);

        // 新建 fail 动画
        mFailAnimator = ValueAnimator.ofFloat(0, 3);
        mFailAnimator.setDuration(2100);
        mFailAnimator.addUpdateListener(animatorUpdateListener);
        mSuccessAnimator.addListener(successListenerAdapter);
        mFailAnimator.addListener(failListenerAdapter);
    }

    ValueAnimator.AnimatorUpdateListener animatorUpdateListener = new ValueAnimator.AnimatorUpdateListener() {
        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            mProgress = (float) animation.getAnimatedValue();
            invalidate();
        }
    };

//    ValueAnimator.AnimatorUpdateListener successUpdateListener = new ValueAnimator.AnimatorUpdateListener() {
//        @Override
//        public void onAnimationUpdate(ValueAnimator animation) {
//            mProgress = (float) animation.getAnimatedValue();
//            invalidate();
//        }
//    };
//
//    ValueAnimator.AnimatorUpdateListener failUpdateListener = new ValueAnimator.AnimatorUpdateListener() {
//        @Override
//        public void onAnimationUpdate(ValueAnimator animation) {
//            mProgress = (float) animation.getAnimatedValue();
//            invalidate();
//        }
//    };

    AnimatorListenerAdapter successListenerAdapter = new AnimatorListenerAdapter() {
        @Override
        public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            mSuccessAnimator.cancel();
        }
    };

    AnimatorListenerAdapter failListenerAdapter = new AnimatorListenerAdapter() {
        @Override
        public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            mFailAnimator.cancel();
        }
    };

    Animator.AnimatorListener animatorListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animation) {

        }

        @Override
        public void onAnimationEnd(Animator animation) {
            mLoadingAnimator.cancel();
            if (curStatus == STATUS_SUCCESS) {
                mBluePaint.setColor(loadingSuccessColor);
                mSuccessAnimator.start();
            } else if (curStatus == STATUS_FAIL) {
                mFailAnimator.start();
            }
        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    };

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mCenterX = w / 2;
        mCenterY = h / 2;
        int tempPaintWidth = (int) (Math.min(w, h) * 0.04);
        paintWidth = tempPaintWidth > paintWidth ? tempPaintWidth : paintWidth;
        mBluePaint.setStrokeWidth(paintWidth);
        mRedPaint.setStrokeWidth(paintWidth);
        int radius = (int) (Math.min(w, h) / 2.0 * 0.9); // 半径为最小长度的一半的90%
        // 在获取宽高之后设置加载框的位置和大小
        Path circlePath = new Path();
        circlePath.addCircle(mCenterX, mCenterY, radius, Path.Direction.CW);
        mPathMeasure = new PathMeasure(circlePath, true);
        mDstPath = new Path();
        // 设置 success 动画的 path
        Path successPath = new Path();
        successPath.addCircle(mCenterX, mCenterY, radius, Path.Direction.CW);
        successPath.moveTo(mCenterX - radius * 0.4f, mCenterY - radius * 0.14f);
        successPath.lineTo(mCenterX - radius * 0.1f, mCenterY + radius * 0.38f);
        successPath.lineTo(mCenterX + radius * 0.38f, mCenterY - radius * 0.4f);
        mSuccessPathMeasure = new PathMeasure(successPath, false);
        mSuccessDstPath = new Path();
        // 设置 fail 动画的 path
        Path failPath = new Path();
        failPath.addCircle(mCenterX, mCenterY, radius, Path.Direction.CW);
        failPath.moveTo(mCenterX - radius / 3, mCenterY - radius / 3);
        failPath.lineTo(mCenterX + radius / 3, mCenterY + radius / 3);
        failPath.moveTo(mCenterX + radius / 3, mCenterY - radius / 3);
        failPath.lineTo(mCenterX - radius / 3, mCenterY + radius / 3);
        mFailPathMeasure = new PathMeasure(failPath, false);
        mFailDstPath = new Path();
    }

    /**
     * 将动画的状态从 Loading 变为 success 或 fail
     */
    public void setStatus(int status) {
        if (curStatus == STATUS_LOADING && status != STATUS_LOADING) {
            curStatus = status;
            mLoadingAnimator.end();
        }
    }

    private int mSuccessIndex = 1;
    private int mFailIndex = 1;

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // 在 Loading 状态下 Canvas 会被旋转, 需要在第一次进入时保存
        if (!hasCanvasSaved) {
            canvas.save();
            hasCanvasSaved = true;
        }
        // 判断当前动画的状态并绘制相应动画
        if (curStatus == STATUS_LOADING) {
            mDstPath.reset();
            float length = mPathMeasure.getLength();
            float stop = mProgress * length;
            float start = (float) (stop - (0.5 - Math.abs(mProgress - 0.5)) * length);
            mPathMeasure.getSegment(start, stop, mDstPath, true);
            // 旋转画布
            mCurRotate = (mCurRotate + 2) % 360;
            canvas.rotate(mCurRotate, mCenterX, mCenterY);
            canvas.drawPath(mDstPath, mBluePaint);
        } else if (curStatus == STATUS_SUCCESS) {
            if (!hasCanvasRestored) {
                canvas.restore();
                hasCanvasRestored = true;
            }
            if (mProgress < 1) {
                float stop = mSuccessPathMeasure.getLength() * mProgress;
                mSuccessPathMeasure.getSegment(0, stop, mSuccessDstPath, true);
            } else {
                if (mSuccessIndex == 1) {
                    mSuccessIndex = 2;
                    mSuccessPathMeasure.getSegment(0, mSuccessPathMeasure.getLength(), mSuccessDstPath, true);
                    mSuccessPathMeasure.nextContour();
                }
                float stop = mSuccessPathMeasure.getLength() * (mProgress - 1);
                mSuccessPathMeasure.getSegment(0, stop, mSuccessDstPath, true);
            }
            canvas.drawPath(mSuccessDstPath, mBluePaint);
        } else if (curStatus == STATUS_FAIL) {
            if (!hasCanvasRestored) {
                canvas.restore();
                hasCanvasRestored = true;
            }
            if (mProgress < 1) {
                float stop = mFailPathMeasure.getLength() * mProgress;
                mFailPathMeasure.getSegment(0, stop, mFailDstPath, true);
            } else if (mProgress < 2) {
                if (mFailIndex == 1) {
                    mFailIndex = 2;
                    mFailPathMeasure.getSegment(0, mFailPathMeasure.getLength(), mFailDstPath, true);
                    mFailPathMeasure.nextContour();
                }
                float stop = mFailPathMeasure.getLength() * (mProgress - 1);
                mFailPathMeasure.getSegment(0, stop, mFailDstPath, true);
            } else {
                if (mFailIndex == 2) {
                    mFailIndex = 3;
                    mFailPathMeasure.getSegment(0, mFailPathMeasure.getLength(), mFailDstPath, true);
                    mFailPathMeasure.nextContour();
                }
                float stop = mFailPathMeasure.getLength() * (mProgress - 2);
                mFailPathMeasure.getSegment(0, stop, mFailDstPath, true);
            }
            canvas.drawPath(mFailDstPath, mRedPaint);
        }
    }
}
