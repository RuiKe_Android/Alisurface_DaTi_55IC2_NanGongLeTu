package com.voodoo.lib_frame;

import android.app.Application;
import android.content.Context;

import com.voodoo.lib_utils.L;

import java.util.ArrayList;

/**
 * Author: voodoo
 * CreateDate: 2020-03-25 025 下午 02:45
 * Description:
 */
public class FrameApplication extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        context = FrameApplication.this;
        initLog();

    }

    public static Context getFrameContext() {
        return context;
    }

    private void initLog() {
        L.init(this)
                .setLogSwitch(Config.Log.IS_SHOW_LOG) // 设置log总开关，包括输出到控制台和文件，默认开
                .setConsoleSwitch(Config.Log.IS_SHOW_CONSOLE_LOG) // 设置是否输出到控制台开关，默认开
                .setLog2FileSwitch(Config.Log.IS_SAVE_LOG_FILE) // 打印log时是否存到文件的开关，默认关
                .setGlobalTag(Config.Log.LOG_TAG) // 设置log全局标签，默认为空
                .setLogHeadSwitch(true) // 设置log头信息开关，默认为开
                .setDir(Config.Log.SAVE_FILE_PATH) // 当自定义路径为空时，写入应用的/cache/log/目录中
                .setFilePrefix("") // 当文件前缀为空时，默认为""，即写入文件为"yyyy-MM-dd.log"
                .setBorderSwitch(true) // 输出日志是否带边框开关，默认开
                .setSingleTagSwitch(true) // 一条日志仅输出一条，默认开，为美化 AS 3.1.0 的 Logcat
                .setConsoleFilter(L.V) // log的控制台过滤器，和logcat过滤器同理，默认Verbose
                .setFileFilter(L.I) // log文件过滤器，和logcat过滤器同理，默认Verbose
                .setStackDeep(1) // log 栈深度，默认为 1
                .setStackOffset(0) // 设置栈偏移，比如二次封装的话就需要设置，默认为 0
                .setSaveDays(10) // 设置日志可保留天数，默认为 -1 表示无限时长
                // 新增 ArrayList 格式化器，默认已支持 Array, Throwable, Bundle, Intent 的格式化输出
                .addFormatter(new L.IFormatter<ArrayList>() {
                    @Override
                    public String format(ArrayList list) {
                        return "ALog Formatter ArrayList { " + list.toString() + " }";
                    }
                });
        L.i(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> APP启动");
    }
}
