package com.voodoo.lib_frame.base;

import java.io.File;

/**
 * Author: voodoo
 * CreateDate: 2019/12/5 005 03:04 下午
 * Description:
 */
public interface BaseView {

    /**
     * 开始请求
     *
     * @param loadingStr 要显示的文字提示
     */
    void onStarted(String loadingStr);

    /**
     * 加载中（下载）
     *
     * @param total   总长度
     * @param current 当前长度
     */
    void onLoading(long total, long current);

    /**
     * 完成
     */
    void onComplete();

    /**
     * 显示错误提示
     *
     * @param errorStr
     */
    void onErrorTip(String errorStr);

    /**
     * 请求成功
     *
     * @param requestUrl     请求的Url
     * @param requestJsonStr 返回的Json字符串
     */
    void onSuccess(String requestUrl, String requestJsonStr);

    /**
     * 请求失败
     *
     * @param requestUrl 请求的Url
     * @param requestMsg 返回的失败信息
     */
    void onFailure(String requestUrl, String requestMsg);


    /**
     * 产生错误
     *
     * @param errorMsg 错误信息
     */
    void onError(String requestUrl,String errorMsg);
    /**
     * 未知异常
     *
     * @param exception 产生的异常信息
     */
    void onException(Exception exception);

    /**
     * @param file 下载成功后的文件
     */
    void onDownloadSuccess( String requestUrl,File file);

    /**
     * @param progress 下载进度
     */
    void onDownloading(String requestUrl,int progress);

    /**
     * @param e 下载异常信息
     */
    void onDownloadFailed(String requestUrl,Exception e);
}
