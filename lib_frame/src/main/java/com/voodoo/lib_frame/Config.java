package com.voodoo.lib_frame;

import android.os.Environment;
import java.io.File;

/**
 * Author: voodoo
 * CreateDate: 2019/08/29 029 下午 12:48:56
 * Description: 项目配置常量类
 */
public class Config {

    // 显示页面路径
    public static boolean isShowRootTitle = BuildConfig.DEBUG;
    // 网络请求超时时间
    public static int TIME_OUT = 60;
    // Mqtt心跳间隔时间 检测是否在线
    public static int MQTT_HEARTBEAT_TIME = 60;
    // Mqtt 超时时间
    public static int MQTT_TIME_OUT = 3000;
    /**
     * Log日志配置
     */
    public static class Log {
        // 是否展示Log总开关
        public static boolean IS_SHOW_LOG = true;
        // 是否展示到控制台
        public static boolean IS_SHOW_CONSOLE_LOG = true;
        // 是否将日志保存到文件
        public static boolean IS_SAVE_LOG_FILE = true;
        // TAG
        public static String LOG_TAG = "===voodoo===";
        // Log日志保存的文件路径
        public static String SAVE_FILE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "Alogs" + File.separator + "com.ruike.alisurface";
    }

}
