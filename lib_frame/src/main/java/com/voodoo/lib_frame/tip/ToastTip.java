package com.voodoo.lib_frame.tip;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.voodoo.lib_frame.R;

/**
 * Author: voodoo
 * CreateDate: 2019/08/30 030 上午 10:41:19
 * Description: 自定义Toast
 */
public class ToastTip extends Toast {

    public ToastTip(Context context) {
        super(context);
    }

    public static ToastTip makeText(Context context, CharSequence text, int duration) {
        ToastTip resule = new ToastTip(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_toast_tip, null);
        TextView tv = view.findViewById(R.id.tips_msg);
        ImageView imgv = view.findViewById(R.id.tips_icon);
        tv.setText(text);
        imgv.setImageResource(R.drawable.icon_right_tip);

        resule.setView(view);
        // 设置位置，此处为垂直居中
        resule.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        resule.setDuration(duration);

        return resule;
    }

    // 设置资源文字
    public static ToastTip makeText(Context context, int resId, int duration) throws Resources.NotFoundException {
        return makeText(context, context.getResources().getText(resId), duration);
    }

    /**
     * 设置图标
     *
     * @param tipBitmap Bitmap
     */
    public void setIcon(int tipBitmap) {
        if (getView() == null) {
            throw new RuntimeException("Toast未被创建，请创建。。。");
        }
        ImageView iv = getView().findViewById(R.id.tips_icon);
        if (iv == null) {
            throw new RuntimeException("ImageView未被创建，请创建。。。");
        }
        iv.setImageResource(tipBitmap);
    }

    @Override
    public void setText(CharSequence s) {
        if (getView() == null) {
            throw new RuntimeException("Toast未被创建，请创建。。。");
        }
        TextView tv = getView().findViewById(R.id.tips_msg);
        if (tv == null) {
            throw new RuntimeException("TextView未被创建，请创建。。。");
        }
        tv.setText(s);
    }

}
