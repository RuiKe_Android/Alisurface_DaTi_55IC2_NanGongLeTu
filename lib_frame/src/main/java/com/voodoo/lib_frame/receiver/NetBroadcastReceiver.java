package com.voodoo.lib_frame.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

import com.voodoo.lib_frame.base.BaseActivity;
import com.voodoo.lib_utils.L;
import com.voodoo.lib_utils.NetUtil;

/**
 * Author: voodoo
 * CreateDate: 2020-03-26 026 下午 04:05
 * Description: 网络状态广播监听
 */
public class NetBroadcastReceiver extends BroadcastReceiver {

    public NetEvent event;
    int netWorkState;

    @Override
    public void onReceive(Context context, Intent intent) {
        // 如果相等的话就说明网络状态发生了变化
        if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {

            netWorkState = NetUtil.getNetWorkState(context);

            // 接口回调传过去状态的类型
            if (event != null) {
                event.onNetChange(netWorkState, netWorkState == 1 || netWorkState == 0);
                L.i("网络状态广播监听", netWorkState);

            }
        }
    }

    public void setNetEvent(NetEvent event) {
        this.event = event;
    }

    /**
     * 自定义网络切换接口
     */
    public interface NetEvent {
        void onNetChange(int netMobile, boolean isConnect);
    }

}
