package com.voodoo.lib_frame.httpTools;

/**
 * 开发者：  voodoo_jie
 * <br>创建时间： 2018/06/24 024 下午 11:34.
 * <br>功能描述： 网络请求返回的Code详解
 */
public class HttpRequestCode {

    /**
     * 解释回传的Code值
     *
     * @param requestCode 回传的Code
     * @return 对应Code的解释
     */
    public static String code2String(int requestCode) {

        String returnStr = "";

        switch (requestCode) {
            // 201-206都表示服务器成功处理了请求的状态代码，说明网页可以正常访问。
            case 200:
                returnStr = "【成功】服务器已成功处理了请求。";
                break;
            case 201:
                returnStr = "【已创建】请求成功且服务器已创建了新的资源。";
                break;
            case 202:
                returnStr = "【已接受】服务器已接受了请求，但尚未对其进行处理。";
                break;
            case 203:
                returnStr = "【非授权信息】服务器已成功处理了请求，但返回了可能来自另一来源的信息。";
                break;
            case 204:
                returnStr = "【无内容】服务器成功处理了请求，但未返回任何内容。";
                break;
            case 205:
                returnStr = "【重置内容】服务器成功处理了请求，但未返回任何内容。与 204 响应不同，此响应要求请求者重置文档视图（例如清除表单内容以输入新内容）";
                break;
            case 206:
                returnStr = "【部分内容】服务器成功处理了部分 GET 请求。";
                break;
            // 300-3007表示的意思是：要完成请求，您需要进一步进行操作。通常，这些状态代码是永远重定向的。
            case 300:
                returnStr = "【多种选择】服务器根据请求可执行多种操作。服务器可根据请求者 来选择一项操作，或提供操作列表供其选择。";
                break;
            case 301:
                returnStr = "【永久移动】请求的网页已被永久移动到新位置。服务器返回此响应时，会自动将请求者转到新位置。您应使用此代码通知搜索引擎蜘蛛网页或网站已被永久移动到新位置。";
                break;
            case 302:
                returnStr = "【临时移动】服务器目前正从不同位置的网页响应请求，但请求者应继续使用原有位置来进行以后的请求。会自动将请求者转到不同的位置。但由于搜索引擎会继续抓取原有位置并将其编入索引，因此您不应使用此代码来告诉搜索引擎页面或网站已被移动。";
                break;
            case 303:
                returnStr = "【查看其他位置】当请求者应对不同的位置进行单独的 GET 请求以检索响应时，服务器会返回此代码。对于除 HEAD 请求之外的所有请求，服务器会自动转到其他位置。";
                break;
            case 304:
                returnStr = "【未修改】自从上次请求后，请求的网页未被修改过。服务器返回此响应时，不会返回网页内容。";
                break;
            case 305:
                returnStr = "【使用代理】请求者只能使用代理访问请求的网页。如果服务器返回此响应，那么，服务器还会指明请求者应当使用的代理。";
                break;
            case 307:
                returnStr = "【临时重定向】服务器目前正从不同位置的网页响应请求，但请求者应继续使用原有位置来进行以后的请求。会自动将请求者转到不同的位置。但由于搜索引擎会继续抓取原有位置并将其编入索引，因此您不应使用此代码来告诉搜索引擎某个页面或网站已被移动。";
                break;
            // 4XX HTTP状态码表示请求可能出错，会妨碍服务器的处理。
            case 400:
                returnStr = "【错误请求】服务器不理解请求的语法。";
                break;
            case 401:
                returnStr = "【身份验证错误】此页要求授权。您可能不希望将此网页纳入索引。";
                break;
            case 403:
                returnStr = "【禁止】服务器拒绝请求。";
                break;
            case 404:
                returnStr = "【未找到】服务器找不到请求的网页。";
                break;
            case 405:
                returnStr = "【方法禁用】禁用请求中指定的方法。";
                break;
            case 406:
                returnStr = "【不接受】无法使用请求的内容特性响应请求的网页。";
                break;
            case 407:
                returnStr = "【需要代理授权】此状态码与 401 类似，但指定请求者必须授权使用代理。如果服务器返回此响应，还表示请求者应当使用代理。";
                break;
            case 408:
                returnStr = "【请求超时】服务器等候请求时发生超时。";
                break;
            case 409:
                returnStr = "【冲突】服务器在完成请求时发生冲突。服务器必须在响应中包含有关冲突的信息。服务器在响应与前一个请求相冲突的 PUT 请求时可能会返回此代码，以及两个请求的差异列表。";
                break;
            case 410:
                returnStr = "【已删除】请求的资源永久删除后，服务器返回此响应。该代码与 404（未找到）代码相似，但在资源以前存在而现在不存在的情况下，有时会用来替代 404 代码。如果资源已永久删除，您应当使用 301 指定资源的新位置。";
                break;
            case 411:
                returnStr = "【需要有效长度】服务器不接受不含有效内容长度标头字段的请求。";
                break;
            case 412:
                returnStr = "【未满足前提条件】服务器未满足请求者在请求中设置的其中一个前提条件。";
                break;
            case 413:
                returnStr = "【请求实体过大】服务器无法处理请求，因为请求实体过大，超出服务器的处理能力。";
                break;
            case 414:
                returnStr = "【请求的 URI 过长】 请求的 URI（通常为网址）过长，服务器无法处理。";
                break;
            case 415:
                returnStr = "【不支持的媒体类型】请求的格式不受请求页面的支持。";
                break;
            case 4116:
                returnStr = "【请求范围不符合要求】如果页面无法提供请求的范围，则服务器会返回此状态码。";
                break;
            case 417:
                returnStr = "【未满足期望值】服务器未满足\"期望\"请求标头字段的要求。";
                break;
            // 500至505表示的意思是：服务器在尝试处理请求时发生内部错误。这些错误可能是服务器本身的错误，而不是请求出错。
            case 500:
                returnStr = "【服务器内部错误】服务器遇到错误，无法完成请求。";
                break;
            case 501:
                returnStr = "【尚未实施】服务器不具备完成请求的功能。例如，当服务器无法识别请求方法时，服务器可能会返回此代码。";
                break;
            case 502:
                returnStr = "【错误网关】服务器作为网关或代理，从上游服务器收到了无效的响应。";
                break;
            case 503:
                returnStr = "【服务不可用】目前无法使用服务器（由于超载或进行停机维护）。通常，这只是一种暂时的状态。";
                break;
            case 504:
                returnStr = "【网关超时】服务器作为网关或代理，未及时从上游服务器接收请求。";
                break;
            case 505:
                returnStr = "【HTTP 版本不受支持】服务器不支持请求中所使用的 HTTP 协议版本";
                break;
            default:
                returnStr = "【未知】请求回传的Code未知异常。";
                break;
        }
        return new StringBuffer().append(requestCode).append(" ---").append(returnStr).toString();
    }
}
