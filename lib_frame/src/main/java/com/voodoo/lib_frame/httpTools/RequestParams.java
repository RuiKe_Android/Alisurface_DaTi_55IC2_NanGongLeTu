package com.voodoo.lib_frame.httpTools;

import com.voodoo.lib_utils.L;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 开发者：  voodoo_jie
 * <br>创建时间： 2018/06/24 024 下午 11:34.
 * <br>功能描述： 设置上传参数
 */
public class RequestParams {

    public ConcurrentHashMap<String, Object> paramsMap;

    public RequestParams() {
        paramsMap = new ConcurrentHashMap<>();
    }

    /**
     * 向ConcurrentHashMap中添加要上传的参数
     *
     * @param key 指定的key
     * @param obj 上传的参数，如果是File以外的其他类型参数，直接将其转换成String
     */
    public void put(String key, Object obj) {
        if (key != null && !key.isEmpty() && obj != null) {
            if (obj instanceof File) {
                putFile(key, (File) obj);
            } else {
                putString(key, new StringBuffer().append(obj).toString());
            }
        }
    }

    public void put(HashMap<String, Object> map) {
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            L.i("方法一：key =" + entry.getKey() + "---value=" + entry.getValue());
            String key = entry.getKey();
            Object obj = entry.getValue();
            if (key != null && !key.isEmpty() && obj != null) {
                if (obj instanceof File) {
                    putFile(key, (File) obj);
                } else {
                    putString(key, new StringBuffer().append(obj).toString());
                }
            }
        }
    }

    /**
     * 真正向Map中添加String的参数
     */
    private void putString(String key, String value) {
        paramsMap.put(key, value);
    }

    /**
     * 真正向Map中添加File的参数
     */
    private void putFile(String key, File file) {
        paramsMap.put(key, file);
    }

    /**
     * 获取Map中是否有参数集合
     *
     * @return 有参数则返回true，否则返回false
     */
    public boolean hasParams() {
        return paramsMap != null && paramsMap.size() > 0;
    }

}
