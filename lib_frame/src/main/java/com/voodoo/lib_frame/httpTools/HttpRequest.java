package com.voodoo.lib_frame.httpTools;

import com.voodoo.lib_utils.L;

import java.io.File;
import java.util.Map;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * 开发者：  voodoo_jie
 * <br>创建时间： 2018/06/24 024 下午 11:34.
 * <br>功能描述： 创建HTTP Request对象
 */
public class HttpRequest {

    private static final MediaType FILE_TYPE = MediaType.parse("application/octet-stream");
    private static final MediaType JSON_TYPE = MediaType.parse("application/json");

    /**
     * 创建Post Request
     *
     * @param url    请求地址
     * @param params 请求参数集合
     * @return Request
     */
    public static Request createPostRequest(String url, RequestParams params) {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        if (params != null && params.hasParams()) {
            for (Map.Entry<String, Object> entry : params.paramsMap.entrySet()) {
                String keyStr = entry.getKey();
                Object value = entry.getValue();
                if (value instanceof File) { // 如果参数是File类型的
                    File file = (File) value;
                    builder.addFormDataPart(keyStr, file.getName(), RequestBody.create(FILE_TYPE, file));
                } else { // 否则直接添加String类型的参数
                    builder.addFormDataPart(keyStr, value.toString());
                }
            }
        }
        if (params != null) {
            L.i("发送POST请求：" + url, "请求参数：" + params.paramsMap.toString());
        }
        return new Request.Builder()
                .url(url)
                .tag(url)
                .post(builder.build())
                .build();
    }

    /**
     * 创建Post Request
     *
     * @param url        请求地址
     * @param jsonParams JSON字符串参数
     * @return Request
     */
    public static Request createPostJsonRequest(String url, String jsonParams) {

        // MediaType 设置 Content-Type 标头中包含的媒体类型值
        RequestBody requestBody = FormBody.create(MediaType.parse("application/json; charset=utf-8"), jsonParams);

        L.i("发送POST请求：" + url, "请求参数：" + jsonParams);

        return new Request.Builder()
                .url(url)//请求的url
                .tag(url)
                .post(requestBody)
                .build();
    }

    /**
     * 创建Get Request
     *
     * @param url    请求地址
     * @param params 请求参数
     * @return 创建好的Request
     */
    public static Request createGetRequest(String url, RequestParams params) {
        StringBuilder urlBuilder = new StringBuilder(url).append("?");
        if (params != null && params.hasParams()) {
            for (Map.Entry<String, Object> entry : params.paramsMap.entrySet()) {
                urlBuilder.append(entry.getKey())
                        .append("=")
                        .append(entry.getValue().toString())
                        .append("&");
            }
        }
        String requestUrl = urlBuilder.toString();
        if (requestUrl.substring(requestUrl.length() - 1).equals("&")) {
            requestUrl = requestUrl.substring(0, requestUrl.length() - 1);
        }
        L.i("发送GET请求，url包括参数：" + requestUrl);
        return new Request.Builder()
                .url(requestUrl)
                .tag(requestUrl)
                .get()
                .build();
    }

}
