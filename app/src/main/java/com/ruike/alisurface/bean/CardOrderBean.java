package com.ruike.alisurface.bean;

import java.util.List;

/**
 * Author: voodoo
 * CreateDate: 2020-05-18 018下午 01:57
 * Description: 刷卡创建订单返回的对象
 */
public class CardOrderBean {

    /**
     * data : {"id":"2020070715503300391349","pay_fee":0.03,"discount_fee":0,"promotion_fee":0,"total_fee":0.03,"details":[{"slotIndex":1,"quantity":3}]}
     * code : 0
     * msg : Success
     * sendData : 2020-07-07 15:50:33
     */

    private DataBean data;
    private int code;
    private String msg;
    private String sendData;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSendData() {
        return sendData;
    }

    public void setSendData(String sendData) {
        this.sendData = sendData;
    }

    public static class DataBean {
        /**
         * id : 2020070715503300391349
         * pay_fee : 0.03
         * discount_fee : 0.0
         * promotion_fee : 0.0
         * total_fee : 0.03
         * details : [{"slotIndex":1,"quantity":3}]
         */

        private String id;
        private double pay_fee;
        private double discount_fee;
        private double promotion_fee;
        private double total_fee;
        private List<DetailsBean> details;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public double getPay_fee() {
            return pay_fee;
        }

        public void setPay_fee(double pay_fee) {
            this.pay_fee = pay_fee;
        }

        public double getDiscount_fee() {
            return discount_fee;
        }

        public void setDiscount_fee(double discount_fee) {
            this.discount_fee = discount_fee;
        }

        public double getPromotion_fee() {
            return promotion_fee;
        }

        public void setPromotion_fee(double promotion_fee) {
            this.promotion_fee = promotion_fee;
        }

        public double getTotal_fee() {
            return total_fee;
        }

        public void setTotal_fee(double total_fee) {
            this.total_fee = total_fee;
        }

        public List<DetailsBean> getDetails() {
            return details;
        }

        public void setDetails(List<DetailsBean> details) {
            this.details = details;
        }

        public static class DetailsBean {
            /**
             * slotIndex : 1
             * quantity : 3
             * slotTypeId : 21001
             */

            private int slotIndex;
            private int quantity;
            private int slotTypeId;

            public int getSlotIndex() {
                return slotIndex;
            }

            public void setSlotIndex(int slotIndex) {
                this.slotIndex = slotIndex;
            }

            public int getQuantity() {
                return quantity;
            }

            public void setQuantity(int quantity) {
                this.quantity = quantity;
            }

            public int getSlotTypeId() {
                return slotTypeId;
            }

            public void setSlotTypeId(int slotTypeId) {
                this.slotTypeId = slotTypeId;
            }

            @Override
            public String toString() {
                return "DetailsBean{" +
                        "slotIndex=" + slotIndex +
                        ", quantity=" + quantity +
                        ", slotTypeId=" + slotTypeId +
                        '}';
            }
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "id='" + id + '\'' +
                    ", pay_fee=" + pay_fee +
                    ", discount_fee=" + discount_fee +
                    ", promotion_fee=" + promotion_fee +
                    ", total_fee=" + total_fee +
                    ", details=" + details +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "CardOrderBean{" +
                "data=" + data +
                ", code=" + code +
                ", msg='" + msg + '\'' +
                ", sendData='" + sendData + '\'' +
                '}';
    }
}
