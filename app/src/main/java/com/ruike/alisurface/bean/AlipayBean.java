package com.ruike.alisurface.bean;

import java.util.List;

/**
 * Created by czb on 2019-02-15.
 * <p>
 * 支付宝扫脸支付实体类
 */
public class AlipayBean {
    /**
     * msg : SUCCESS
     * code : 0
     * data : {"order":{"OrderNo":"2019021310100801782400","MchId":"20181015013837","Fee_typeId":103,"O_state":2,"Pay_time":"2019-02-13 10:10:09","Total_fee":0.01,"Pay_fee":0.01,"Get_fee":0.01,"Charge_fee":0,"Discount_fee":0,"Currency":"CNY","Remark":"交易订单","AppId":null,"ThirdMchId":null,"Extra":null,"OpenId":null,"TradeNo":"2019021322001429431015086148","OrderType":201,"PayParams":null,"OrderDetails":[{"ProductName":"图片测试1","ProductImg":"http://120.78.219.34:8018/UploadFile/product/8dc54969-b939-49e8-b2af-a5b57105c366.png","Price":0.01,"Quantity":1,"ProductId":"6fd9b8eb-6946-4507-aaaf-10cd7174c18b","SlotIndex":1,"OrderId":"2019021310100801782400","OutQuantity":null,"SlotTypeId":21003,"IsUpdate":false,"Id":"0b89ce20-34c7-44b6-8a39-b08cee2325de","CreateOn":"2019-02-13 10:10:08","UpdateOn":"2019-02-13 10:10:08","CreateBy":null,"UpdateBy":null,"IsDelete":false,"Verion":null}],"BaseUserId":"2d42f4e0-9a58-45e0-8490-6e813fe45098","Id":"2019021310100801782400","CreateOn":"2019-02-13 10:10:08","UpdateOn":"2019-02-13 10:10:08","CreateBy":null,"UpdateBy":null,"IsDelete":false,"Verion":null}}
     */

    private String msg;
    private int code;
    private DataBean data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * order : {"OrderNo":"2019021310100801782400","MchId":"20181015013837","Fee_typeId":103,"O_state":2,"Pay_time":"2019-02-13 10:10:09","Total_fee":0.01,"Pay_fee":0.01,"Get_fee":0.01,"Charge_fee":0,"Discount_fee":0,"Currency":"CNY","Remark":"交易订单","AppId":null,"ThirdMchId":null,"Extra":null,"OpenId":null,"TradeNo":"2019021322001429431015086148","OrderType":201,"PayParams":null,"OrderDetails":[{"ProductName":"图片测试1","ProductImg":"http://120.78.219.34:8018/UploadFile/product/8dc54969-b939-49e8-b2af-a5b57105c366.png","Price":0.01,"Quantity":1,"ProductId":"6fd9b8eb-6946-4507-aaaf-10cd7174c18b","SlotIndex":1,"OrderId":"2019021310100801782400","OutQuantity":null,"SlotTypeId":21003,"IsUpdate":false,"Id":"0b89ce20-34c7-44b6-8a39-b08cee2325de","CreateOn":"2019-02-13 10:10:08","UpdateOn":"2019-02-13 10:10:08","CreateBy":null,"UpdateBy":null,"IsDelete":false,"Verion":null}],"BaseUserId":"2d42f4e0-9a58-45e0-8490-6e813fe45098","Id":"2019021310100801782400","CreateOn":"2019-02-13 10:10:08","UpdateOn":"2019-02-13 10:10:08","CreateBy":null,"UpdateBy":null,"IsDelete":false,"Verion":null}
         */

        private OrderBean order;

        public OrderBean getOrder() {
            return order;
        }

        public void setOrder(OrderBean order) {
            this.order = order;
        }

        public static class OrderBean {
            @Override
            public String toString() {
                return "OrderBean{" +
                        "OrderNo='" + OrderNo + '\'' +
                        ", MchId='" + MchId + '\'' +
                        ", Fee_typeId=" + Fee_typeId +
                        ", O_state=" + O_state +
                        ", Pay_time='" + Pay_time + '\'' +
                        ", Total_fee=" + Total_fee +
                        ", Pay_fee=" + Pay_fee +
                        ", Get_fee=" + Get_fee +
                        ", Charge_fee=" + Charge_fee +
                        ", Discount_fee=" + Discount_fee +
                        ", Currency='" + Currency + '\'' +
                        ", Remark='" + Remark + '\'' +
                        ", AppId=" + AppId +
                        ", ThirdMchId=" + ThirdMchId +
                        ", Extra=" + Extra +
                        ", OpenId=" + OpenId +
                        ", TradeNo='" + TradeNo + '\'' +
                        ", OrderType=" + OrderType +
                        ", PayParams=" + PayParams +
                        ", BaseUserId='" + BaseUserId + '\'' +
                        ", Id='" + Id + '\'' +
                        ", CreateOn='" + CreateOn + '\'' +
                        ", UpdateOn='" + UpdateOn + '\'' +
                        ", CreateBy=" + CreateBy +
                        ", UpdateBy=" + UpdateBy +
                        ", IsDelete=" + IsDelete +
                        ", Verion=" + Verion +
                        ", OrderDetails=" + OrderDetails +
                        '}';
            }

            /**
             * OrderNo : 2019021310100801782400
             * MchId : 20181015013837
             * Fee_typeId : 103
             * O_state : 2
             * Pay_time : 2019-02-13 10:10:09
             * Total_fee : 0.01
             * Pay_fee : 0.01
             * Get_fee : 0.01
             * Charge_fee : 0
             * Discount_fee : 0
             * Currency : CNY
             * Remark : 交易订单
             * AppId : null
             * ThirdMchId : null
             * Extra : null
             * OpenId : null
             * TradeNo : 2019021322001429431015086148
             * OrderType : 201
             * PayParams : null
             * OrderDetails : [{"ProductName":"图片测试1","ProductImg":"http://120.78.219.34:8018/UploadFile/product/8dc54969-b939-49e8-b2af-a5b57105c366.png","Price":0.01,"Quantity":1,"ProductId":"6fd9b8eb-6946-4507-aaaf-10cd7174c18b","SlotIndex":1,"OrderId":"2019021310100801782400","OutQuantity":null,"SlotTypeId":21003,"IsUpdate":false,"Id":"0b89ce20-34c7-44b6-8a39-b08cee2325de","CreateOn":"2019-02-13 10:10:08","UpdateOn":"2019-02-13 10:10:08","CreateBy":null,"UpdateBy":null,"IsDelete":false,"Verion":null}]
             * BaseUserId : 2d42f4e0-9a58-45e0-8490-6e813fe45098
             * Id : 2019021310100801782400
             * CreateOn : 2019-02-13 10:10:08
             * UpdateOn : 2019-02-13 10:10:08
             * CreateBy : null
             * UpdateBy : null
             * IsDelete : false
             * Verion : null
             */


            private String activity_id;

            public String getActivity_id() {
                return activity_id;
            }

            public void setActivity_id(String activity_id) {
                this.activity_id = activity_id;
            }

            public String getActivity_config() {
                return activity_config;
            }

            public void setActivity_config(String activity_config) {
                this.activity_config = activity_config;
            }

            private String activity_config;
            private String OrderNo;
            private String MchId;
            private int Fee_typeId;
            private int O_state;
            private String Pay_time;
            private double Total_fee;
            private double Pay_fee;
            private double Get_fee;
            private double Charge_fee;
            private double Discount_fee;
            private String Currency;
            private String Remark;
            private Object AppId;
            private Object ThirdMchId;
            private Object Extra;
            private Object OpenId;
            private String TradeNo;
            private int OrderType;
            private Object PayParams;
            private String BaseUserId;
            private String Id;
            private String CreateOn;
            private String UpdateOn;
            private Object CreateBy;
            private Object UpdateBy;
            private boolean IsDelete;
            private Object Verion;
            private List<ShopDetailBean.OrderDetailsBean> OrderDetails;

            public String getOrderNo() {
                return OrderNo;
            }

            public void setOrderNo(String OrderNo) {
                this.OrderNo = OrderNo;
            }

            public String getMchId() {
                return MchId;
            }

            public void setMchId(String MchId) {
                this.MchId = MchId;
            }

            public int getFee_typeId() {
                return Fee_typeId;
            }

            public void setFee_typeId(int Fee_typeId) {
                this.Fee_typeId = Fee_typeId;
            }

            public int getO_state() {
                return O_state;
            }

            public void setO_state(int O_state) {
                this.O_state = O_state;
            }

            public String getPay_time() {
                return Pay_time;
            }

            public void setPay_time(String Pay_time) {
                this.Pay_time = Pay_time;
            }

            public double getTotal_fee() {
                return Total_fee;
            }

            public void setTotal_fee(double Total_fee) {
                this.Total_fee = Total_fee;
            }

            public double getPay_fee() {
                return Pay_fee;
            }

            public void setPay_fee(double Pay_fee) {
                this.Pay_fee = Pay_fee;
            }

            public double getGet_fee() {
                return Get_fee;
            }

            public void setGet_fee(double Get_fee) {
                this.Get_fee = Get_fee;
            }

            public double getCharge_fee() {
                return Charge_fee;
            }

            public void setCharge_fee(double Charge_fee) {
                this.Charge_fee = Charge_fee;
            }

            public double getDiscount_fee() {
                return Discount_fee;
            }

            public void setDiscount_fee(double Discount_fee) {
                this.Discount_fee = Discount_fee;
            }

            public String getCurrency() {
                return Currency;
            }

            public void setCurrency(String Currency) {
                this.Currency = Currency;
            }

            public String getRemark() {
                return Remark;
            }

            public void setRemark(String Remark) {
                this.Remark = Remark;
            }

            public Object getAppId() {
                return AppId;
            }

            public void setAppId(Object AppId) {
                this.AppId = AppId;
            }

            public Object getThirdMchId() {
                return ThirdMchId;
            }

            public void setThirdMchId(Object ThirdMchId) {
                this.ThirdMchId = ThirdMchId;
            }

            public Object getExtra() {
                return Extra;
            }

            public void setExtra(Object Extra) {
                this.Extra = Extra;
            }

            public Object getOpenId() {
                return OpenId;
            }

            public void setOpenId(Object OpenId) {
                this.OpenId = OpenId;
            }

            public String getTradeNo() {
                return TradeNo;
            }

            public void setTradeNo(String TradeNo) {
                this.TradeNo = TradeNo;
            }

            public int getOrderType() {
                return OrderType;
            }

            public void setOrderType(int OrderType) {
                this.OrderType = OrderType;
            }

            public Object getPayParams() {
                return PayParams;
            }

            public void setPayParams(Object PayParams) {
                this.PayParams = PayParams;
            }

            public String getBaseUserId() {
                return BaseUserId;
            }

            public void setBaseUserId(String BaseUserId) {
                this.BaseUserId = BaseUserId;
            }

            public String getId() {
                return Id;
            }

            public void setId(String Id) {
                this.Id = Id;
            }

            public String getCreateOn() {
                return CreateOn;
            }

            public void setCreateOn(String CreateOn) {
                this.CreateOn = CreateOn;
            }

            public String getUpdateOn() {
                return UpdateOn;
            }

            public void setUpdateOn(String UpdateOn) {
                this.UpdateOn = UpdateOn;
            }

            public Object getCreateBy() {
                return CreateBy;
            }

            public void setCreateBy(Object CreateBy) {
                this.CreateBy = CreateBy;
            }

            public Object getUpdateBy() {
                return UpdateBy;
            }

            public void setUpdateBy(Object UpdateBy) {
                this.UpdateBy = UpdateBy;
            }

            public boolean isIsDelete() {
                return IsDelete;
            }

            public void setIsDelete(boolean IsDelete) {
                this.IsDelete = IsDelete;
            }

            public Object getVerion() {
                return Verion;
            }

            public void setVerion(Object Verion) {
                this.Verion = Verion;
            }

            public List<ShopDetailBean.OrderDetailsBean> getOrderDetails() {
                return OrderDetails;
            }

            public void setOrderDetails(List<ShopDetailBean.OrderDetailsBean> OrderDetails) {
                this.OrderDetails = OrderDetails;
            }


        }
    }
}
