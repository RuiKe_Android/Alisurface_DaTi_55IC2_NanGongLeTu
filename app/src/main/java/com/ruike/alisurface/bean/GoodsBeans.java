package com.ruike.alisurface.bean;

import java.io.Serializable;
import java.util.List;

/**
 *
 *
 * 商品类型
 */

public class GoodsBeans implements Serializable {


    /**
     * msg : SUCCESS
     * code : 0
     * data : [{"productname":"可乐测试","productimg":"http://120.78.219.34:8018/UploadFile/product/b0fdf20f-f21b-46d5-a81a-b5e0ea967999.jpg","productid":"86475b88-c6a5-4870-b44b-805edc188f89","price":0.01,"count":1,"slotIndexs":[1]},{"productname":"小明同学橙","productimg":"http://120.78.219.34:8018/UploadFile/product/a4589b35-e801-4415-b6fd-0a90d68a3c2aSlotPic (25).png","productid":"016d79e8-c97b-4a05-b64b-e4129ec42e7b","price":0.01,"count":208,"slotIndexs":[10,11,12,13,14,15,16,17,18,19,2,20,3,4,5,6,7,8,9]},{"productname":"小明同学橙","productimg":"http://120.78.219.34:8018/UploadFile/product/a4589b35-e801-4415-b6fd-0a90d68a3c2aSlotPic (25).png","productid":"016d79e8-c97b-4a05-b64b-e4129ec42e7b","price":1,"count":3,"slotIndexs":[1000,1001,1002]},{"productname":"午后奶茶","productimg":"http://120.78.219.34:8018/UploadFile/product/4ca36031-a46d-4b47-ac68-f40d732a334aSlotPic (22).png","productid":"1055","price":1,"count":200,"slotIndexs":[21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40]},{"productname":"小明同学绿","productimg":"http://120.78.219.34:8018/UploadFile/product/580f3ea7-30d9-4223-9e01-12966e67a5f3SlotPic (27).png","productid":"06658642-b510-4bb1-888f-1a2af2c56443","price":1,"count":190,"slotIndexs":[41,42,43,44,45,46,47,49,50,51,52,53,54,55,56,57,58,59,60]},{"productname":"测试商品","productimg":"http://120.78.219.34:8018/UploadFile/product/08b958a4-5460-47cb-a219-cb3c90f62a9dSlotPic (28).png","productid":"NULL","price":0.01,"count":10,"slotIndexs":[48]}]
     */

    /**
     * productname : 可乐测试
     * productimg : http://120.78.219.34:8018/UploadFile/product/b0fdf20f-f21b-46d5-a81a-b5e0ea967999.jpg
     * productid : 86475b88-c6a5-4870-b44b-805edc188f89
     * price : 0.01
     * count : 1
     * slotIndexs : [1]
     */


    private String productname;
    private String productimg;
    private String productid;
    private double price;
    private int count;

    public List<Integer> getSlotIndexs() {
        return slotIndexs;
    }

    public void setSlotIndexs(List<Integer> slotIndexs) {
        this.slotIndexs = slotIndexs;
    }

    private List<Integer> slotIndexs;

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getProductimg() {
        return productimg;
    }

    public void setProductimg(String productimg) {
        this.productimg = productimg;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "GoodsBeans{" +
                "productname='" + productname + '\'' +
                ", productimg='" + productimg + '\'' +
                ", productid='" + productid + '\'' +
                ", price=" + price +
                ", count=" + count +
                ", slotIndexs=" + slotIndexs +
                '}';
    }
}
