package com.ruike.alisurface.bean;

import java.io.Serializable;

/**
 * Author: voodoo
 * CreateDate: 2020/07/03 003 09:45 上午
 * Description: 创建卡充值回传对象
 */
public class CardTopUpBean implements Serializable {

    /**
     * msg : success
     * code : 0
     * data : {"url":"http://www.shouhuojiyun.com/Rknew/App/order/Pay?OrderId=2020070117370202251098&TradeType=JdPay","order":{"OrderNo":"2020070117370202251098","MchId":"WP1928011831","Fee_typeId":0,"O_state":0,"Pay_time":null,"Total_fee":0.01,"Pay_fee":0.01,"Get_fee":0.01,"Charge_fee":0,"Discount_fee":0,"Promotion_fee":0,"Currency":"CNY","Remark":null,"AppId":null,"ThirdMchId":"666105800008231","MchName":"66666","Extra":"23323323","OpenId":null,"TradeNo":null,"OrderType":208,"PayParams":null,"OrderDetails":null,"Result":0,"Refund_fee":0,"TradeType":"JdPay","MchVerion":"android.v1","SN":"333333","TransferAble":false,"IsTransferSuccess":false,"TransferId":null,"NotifyUrl":null,"ParentId":null,"DropCheckStr":null,"IsSetDropCheck":false,"IsDropCheck":0,"IsBad":false,"CheckOutCount":0,"IsEnd":false,"Details":"卡23323323在设备66666充值0.01元","WxTmpMsgNotifyToUserOpenIds":"opcFO0WboRgIdPq80Lv_pGmJCD_k","Total_Cost":0.01,"activity_id":null,"key_id":null,"activity_config":null,"BaseUserId":"RK","CreateOn":"2020-07-01 17:37:02","UpdateOn":"2020-07-01 17:37:02","CreateBy":null,"UpdateBy":null,"IsDelete":false,"Id":"2020070117370202251098"}}
     */

    private String msg;
    private int code;
    private DataBean data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * url : http://www.shouhuojiyun.com/Rknew/App/order/Pay?OrderId=2020070117370202251098&TradeType=JdPay
         * order : {"OrderNo":"2020070117370202251098","MchId":"WP1928011831","Fee_typeId":0,"O_state":0,"Pay_time":null,"Total_fee":0.01,"Pay_fee":0.01,"Get_fee":0.01,"Charge_fee":0,"Discount_fee":0,"Promotion_fee":0,"Currency":"CNY","Remark":null,"AppId":null,"ThirdMchId":"666105800008231","MchName":"66666","Extra":"23323323","OpenId":null,"TradeNo":null,"OrderType":208,"PayParams":null,"OrderDetails":null,"Result":0,"Refund_fee":0,"TradeType":"JdPay","MchVerion":"android.v1","SN":"333333","TransferAble":false,"IsTransferSuccess":false,"TransferId":null,"NotifyUrl":null,"ParentId":null,"DropCheckStr":null,"IsSetDropCheck":false,"IsDropCheck":0,"IsBad":false,"CheckOutCount":0,"IsEnd":false,"Details":"卡23323323在设备66666充值0.01元","WxTmpMsgNotifyToUserOpenIds":"opcFO0WboRgIdPq80Lv_pGmJCD_k","Total_Cost":0.01,"activity_id":null,"key_id":null,"activity_config":null,"BaseUserId":"RK","CreateOn":"2020-07-01 17:37:02","UpdateOn":"2020-07-01 17:37:02","CreateBy":null,"UpdateBy":null,"IsDelete":false,"Id":"2020070117370202251098"}
         */

        private String url;
        private OrderBean order;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public OrderBean getOrder() {
            return order;
        }

        public void setOrder(OrderBean order) {
            this.order = order;
        }

        public static class OrderBean implements Serializable  {
            /**
             * OrderNo : 2020070117370202251098
             * MchId : WP1928011831
             * Fee_typeId : 0
             * O_state : 0
             * Pay_time : null
             * Total_fee : 0.01
             * Pay_fee : 0.01
             * Get_fee : 0.01
             * Charge_fee : 0
             * Discount_fee : 0
             * Promotion_fee : 0
             * Currency : CNY
             * Remark : null
             * AppId : null
             * ThirdMchId : 666105800008231
             * MchName : 66666
             * Extra : 23323323
             * OpenId : null
             * TradeNo : null
             * OrderType : 208
             * PayParams : null
             * OrderDetails : null
             * Result : 0
             * Refund_fee : 0
             * TradeType : JdPay
             * MchVerion : android.v1
             * SN : 333333
             * TransferAble : false
             * IsTransferSuccess : false
             * TransferId : null
             * NotifyUrl : null
             * ParentId : null
             * DropCheckStr : null
             * IsSetDropCheck : false
             * IsDropCheck : 0
             * IsBad : false
             * CheckOutCount : 0
             * IsEnd : false
             * Details : 卡23323323在设备66666充值0.01元
             * WxTmpMsgNotifyToUserOpenIds : opcFO0WboRgIdPq80Lv_pGmJCD_k
             * Total_Cost : 0.01
             * activity_id : null
             * key_id : null
             * activity_config : null
             * BaseUserId : RK
             * CreateOn : 2020-07-01 17:37:02
             * UpdateOn : 2020-07-01 17:37:02
             * CreateBy : null
             * UpdateBy : null
             * IsDelete : false
             * Id : 2020070117370202251098
             */

            private String OrderNo;
            private String MchId;
            private int Fee_typeId;
            private int O_state;
            private Object Pay_time;
            private double Total_fee;
            private double Pay_fee;
            private double Get_fee;
            private int Charge_fee;
            private int Discount_fee;
            private int Promotion_fee;
            private String Currency;
            private Object Remark;
            private Object AppId;
            private String ThirdMchId;
            private String MchName;
            private String Extra;
            private Object OpenId;
            private Object TradeNo;
            private int OrderType;
            private Object PayParams;
            private Object OrderDetails;
            private int Result;
            private int Refund_fee;
            private String TradeType;
            private String MchVerion;
            private String SN;
            private boolean TransferAble;
            private boolean IsTransferSuccess;
            private Object TransferId;
            private Object NotifyUrl;
            private Object ParentId;
            private Object DropCheckStr;
            private boolean IsSetDropCheck;
            private int IsDropCheck;
            private boolean IsBad;
            private int CheckOutCount;
            private boolean IsEnd;
            private String Details;
            private String WxTmpMsgNotifyToUserOpenIds;
            private double Total_Cost;
            private Object activity_id;
            private Object key_id;
            private Object activity_config;
            private String BaseUserId;
            private String CreateOn;
            private String UpdateOn;
            private Object CreateBy;
            private Object UpdateBy;
            private boolean IsDelete;
            private String Id;

            public String getOrderNo() {
                return OrderNo;
            }

            public void setOrderNo(String OrderNo) {
                this.OrderNo = OrderNo;
            }

            public String getMchId() {
                return MchId;
            }

            public void setMchId(String MchId) {
                this.MchId = MchId;
            }

            public int getFee_typeId() {
                return Fee_typeId;
            }

            public void setFee_typeId(int Fee_typeId) {
                this.Fee_typeId = Fee_typeId;
            }

            public int getO_state() {
                return O_state;
            }

            public void setO_state(int O_state) {
                this.O_state = O_state;
            }

            public Object getPay_time() {
                return Pay_time;
            }

            public void setPay_time(Object Pay_time) {
                this.Pay_time = Pay_time;
            }

            public double getTotal_fee() {
                return Total_fee;
            }

            public void setTotal_fee(double Total_fee) {
                this.Total_fee = Total_fee;
            }

            public double getPay_fee() {
                return Pay_fee;
            }

            public void setPay_fee(double Pay_fee) {
                this.Pay_fee = Pay_fee;
            }

            public double getGet_fee() {
                return Get_fee;
            }

            public void setGet_fee(double Get_fee) {
                this.Get_fee = Get_fee;
            }

            public int getCharge_fee() {
                return Charge_fee;
            }

            public void setCharge_fee(int Charge_fee) {
                this.Charge_fee = Charge_fee;
            }

            public int getDiscount_fee() {
                return Discount_fee;
            }

            public void setDiscount_fee(int Discount_fee) {
                this.Discount_fee = Discount_fee;
            }

            public int getPromotion_fee() {
                return Promotion_fee;
            }

            public void setPromotion_fee(int Promotion_fee) {
                this.Promotion_fee = Promotion_fee;
            }

            public String getCurrency() {
                return Currency;
            }

            public void setCurrency(String Currency) {
                this.Currency = Currency;
            }

            public Object getRemark() {
                return Remark;
            }

            public void setRemark(Object Remark) {
                this.Remark = Remark;
            }

            public Object getAppId() {
                return AppId;
            }

            public void setAppId(Object AppId) {
                this.AppId = AppId;
            }

            public String getThirdMchId() {
                return ThirdMchId;
            }

            public void setThirdMchId(String ThirdMchId) {
                this.ThirdMchId = ThirdMchId;
            }

            public String getMchName() {
                return MchName;
            }

            public void setMchName(String MchName) {
                this.MchName = MchName;
            }

            public String getExtra() {
                return Extra;
            }

            public void setExtra(String Extra) {
                this.Extra = Extra;
            }

            public Object getOpenId() {
                return OpenId;
            }

            public void setOpenId(Object OpenId) {
                this.OpenId = OpenId;
            }

            public Object getTradeNo() {
                return TradeNo;
            }

            public void setTradeNo(Object TradeNo) {
                this.TradeNo = TradeNo;
            }

            public int getOrderType() {
                return OrderType;
            }

            public void setOrderType(int OrderType) {
                this.OrderType = OrderType;
            }

            public Object getPayParams() {
                return PayParams;
            }

            public void setPayParams(Object PayParams) {
                this.PayParams = PayParams;
            }

            public Object getOrderDetails() {
                return OrderDetails;
            }

            public void setOrderDetails(Object OrderDetails) {
                this.OrderDetails = OrderDetails;
            }

            public int getResult() {
                return Result;
            }

            public void setResult(int Result) {
                this.Result = Result;
            }

            public int getRefund_fee() {
                return Refund_fee;
            }

            public void setRefund_fee(int Refund_fee) {
                this.Refund_fee = Refund_fee;
            }

            public String getTradeType() {
                return TradeType;
            }

            public void setTradeType(String TradeType) {
                this.TradeType = TradeType;
            }

            public String getMchVerion() {
                return MchVerion;
            }

            public void setMchVerion(String MchVerion) {
                this.MchVerion = MchVerion;
            }

            public String getSN() {
                return SN;
            }

            public void setSN(String SN) {
                this.SN = SN;
            }

            public boolean isTransferAble() {
                return TransferAble;
            }

            public void setTransferAble(boolean TransferAble) {
                this.TransferAble = TransferAble;
            }

            public boolean isIsTransferSuccess() {
                return IsTransferSuccess;
            }

            public void setIsTransferSuccess(boolean IsTransferSuccess) {
                this.IsTransferSuccess = IsTransferSuccess;
            }

            public Object getTransferId() {
                return TransferId;
            }

            public void setTransferId(Object TransferId) {
                this.TransferId = TransferId;
            }

            public Object getNotifyUrl() {
                return NotifyUrl;
            }

            public void setNotifyUrl(Object NotifyUrl) {
                this.NotifyUrl = NotifyUrl;
            }

            public Object getParentId() {
                return ParentId;
            }

            public void setParentId(Object ParentId) {
                this.ParentId = ParentId;
            }

            public Object getDropCheckStr() {
                return DropCheckStr;
            }

            public void setDropCheckStr(Object DropCheckStr) {
                this.DropCheckStr = DropCheckStr;
            }

            public boolean isIsSetDropCheck() {
                return IsSetDropCheck;
            }

            public void setIsSetDropCheck(boolean IsSetDropCheck) {
                this.IsSetDropCheck = IsSetDropCheck;
            }

            public int getIsDropCheck() {
                return IsDropCheck;
            }

            public void setIsDropCheck(int IsDropCheck) {
                this.IsDropCheck = IsDropCheck;
            }

            public boolean isIsBad() {
                return IsBad;
            }

            public void setIsBad(boolean IsBad) {
                this.IsBad = IsBad;
            }

            public int getCheckOutCount() {
                return CheckOutCount;
            }

            public void setCheckOutCount(int CheckOutCount) {
                this.CheckOutCount = CheckOutCount;
            }

            public boolean isIsEnd() {
                return IsEnd;
            }

            public void setIsEnd(boolean IsEnd) {
                this.IsEnd = IsEnd;
            }

            public String getDetails() {
                return Details;
            }

            public void setDetails(String Details) {
                this.Details = Details;
            }

            public String getWxTmpMsgNotifyToUserOpenIds() {
                return WxTmpMsgNotifyToUserOpenIds;
            }

            public void setWxTmpMsgNotifyToUserOpenIds(String WxTmpMsgNotifyToUserOpenIds) {
                this.WxTmpMsgNotifyToUserOpenIds = WxTmpMsgNotifyToUserOpenIds;
            }

            public double getTotal_Cost() {
                return Total_Cost;
            }

            public void setTotal_Cost(double Total_Cost) {
                this.Total_Cost = Total_Cost;
            }

            public Object getActivity_id() {
                return activity_id;
            }

            public void setActivity_id(Object activity_id) {
                this.activity_id = activity_id;
            }

            public Object getKey_id() {
                return key_id;
            }

            public void setKey_id(Object key_id) {
                this.key_id = key_id;
            }

            public Object getActivity_config() {
                return activity_config;
            }

            public void setActivity_config(Object activity_config) {
                this.activity_config = activity_config;
            }

            public String getBaseUserId() {
                return BaseUserId;
            }

            public void setBaseUserId(String BaseUserId) {
                this.BaseUserId = BaseUserId;
            }

            public String getCreateOn() {
                return CreateOn;
            }

            public void setCreateOn(String CreateOn) {
                this.CreateOn = CreateOn;
            }

            public String getUpdateOn() {
                return UpdateOn;
            }

            public void setUpdateOn(String UpdateOn) {
                this.UpdateOn = UpdateOn;
            }

            public Object getCreateBy() {
                return CreateBy;
            }

            public void setCreateBy(Object CreateBy) {
                this.CreateBy = CreateBy;
            }

            public Object getUpdateBy() {
                return UpdateBy;
            }

            public void setUpdateBy(Object UpdateBy) {
                this.UpdateBy = UpdateBy;
            }

            public boolean isIsDelete() {
                return IsDelete;
            }

            public void setIsDelete(boolean IsDelete) {
                this.IsDelete = IsDelete;
            }

            public String getId() {
                return Id;
            }

            public void setId(String Id) {
                this.Id = Id;
            }

            @Override
            public String toString() {
                return "OrderBean{" +
                        "OrderNo='" + OrderNo + '\'' +
                        ", MchId='" + MchId + '\'' +
                        ", Fee_typeId=" + Fee_typeId +
                        ", O_state=" + O_state +
                        ", Pay_time=" + Pay_time +
                        ", Total_fee=" + Total_fee +
                        ", Pay_fee=" + Pay_fee +
                        ", Get_fee=" + Get_fee +
                        ", Charge_fee=" + Charge_fee +
                        ", Discount_fee=" + Discount_fee +
                        ", Promotion_fee=" + Promotion_fee +
                        ", Currency='" + Currency + '\'' +
                        ", Remark=" + Remark +
                        ", AppId=" + AppId +
                        ", ThirdMchId='" + ThirdMchId + '\'' +
                        ", MchName='" + MchName + '\'' +
                        ", Extra='" + Extra + '\'' +
                        ", OpenId=" + OpenId +
                        ", TradeNo=" + TradeNo +
                        ", OrderType=" + OrderType +
                        ", PayParams=" + PayParams +
                        ", OrderDetails=" + OrderDetails +
                        ", Result=" + Result +
                        ", Refund_fee=" + Refund_fee +
                        ", TradeType='" + TradeType + '\'' +
                        ", MchVerion='" + MchVerion + '\'' +
                        ", SN='" + SN + '\'' +
                        ", TransferAble=" + TransferAble +
                        ", IsTransferSuccess=" + IsTransferSuccess +
                        ", TransferId=" + TransferId +
                        ", NotifyUrl=" + NotifyUrl +
                        ", ParentId=" + ParentId +
                        ", DropCheckStr=" + DropCheckStr +
                        ", IsSetDropCheck=" + IsSetDropCheck +
                        ", IsDropCheck=" + IsDropCheck +
                        ", IsBad=" + IsBad +
                        ", CheckOutCount=" + CheckOutCount +
                        ", IsEnd=" + IsEnd +
                        ", Details='" + Details + '\'' +
                        ", WxTmpMsgNotifyToUserOpenIds='" + WxTmpMsgNotifyToUserOpenIds + '\'' +
                        ", Total_Cost=" + Total_Cost +
                        ", activity_id=" + activity_id +
                        ", key_id=" + key_id +
                        ", activity_config=" + activity_config +
                        ", BaseUserId='" + BaseUserId + '\'' +
                        ", CreateOn='" + CreateOn + '\'' +
                        ", UpdateOn='" + UpdateOn + '\'' +
                        ", CreateBy=" + CreateBy +
                        ", UpdateBy=" + UpdateBy +
                        ", IsDelete=" + IsDelete +
                        ", Id='" + Id + '\'' +
                        '}';
            }
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "url='" + url + '\'' +
                    ", order=" + order +
                    '}';
        }
    }
}
