package com.ruike.alisurface.bean;

/**
 * Author: voodoo
 * CreateDate: 2020-04-16 016 下午 03:21
 * Description: 设置界面的设置项对象，方便动态修改设置项
 */
public class SettingPageItemBean {

    private int flag; // 标志位 点击的时候判断的标志位
    private int imgResId; // 展示的资源图片
    private String settingName; // 设置的名称

    public SettingPageItemBean() {
    }

    public SettingPageItemBean(int flag, int imgResId, String settingName) {
        this.flag = flag;
        this.imgResId = imgResId;
        this.settingName = settingName;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getImgResId() {
        return imgResId;
    }

    public void setImgResId(int imgResId) {
        this.imgResId = imgResId;
    }

    public String getSettingName() {
        return settingName;
    }

    public void setSettingName(String settingName) {
        this.settingName = settingName;
    }
}
