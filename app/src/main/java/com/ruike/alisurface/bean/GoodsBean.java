package com.ruike.alisurface.bean;

import com.voodoo.lib_databases.annotation.Id;
import com.voodoo.lib_databases.annotation.Property;
import com.voodoo.lib_databases.annotation.Table;

import java.io.Serializable;

/**
 * Author: voodoo
 * CreateDate: 2020-03-26 026 下午 12:45
 * Description: 商品货道详情类
 */
@Table(name = "tb_goods")
public class GoodsBean implements Serializable {

    /**
     * mchid : 6369292310701272
     * index : 1
     * productid : 016d79e8-c97b-4a05-b64b-e4129ec42e7b
     * productthumbnail : null
     * productimg : http://120.78.219.34:8018/UploadFile/product/a4589b35-e801-4415-b6fd-0a90d68a3c2aSlotPic (25).png
     * count : 0
     * maxcount : 5
     * productname : 小明同学橙
     * layer : 0
     * cab : 0
     * col : 0
     * price : 0.01
     * slottypeid : 21001
     * slotstateid : 22001
     * slottype : null
     * slotstate : null
     * categoryid : null
     */
    @Id
    private int _id;
    private String mchid; // 机器id
    @Property(column = "aisle_number")
    private int index; // 货道编号
    private String productid; // 商品id
    private String productthumbnail;
    private String productimg; // 图片地址
    private int count; // 当前库存
    private int maxcount; // 最大容量
    private String productname; // 商品名称
    private int layer; // 层序号
    private int cab; // 货柜序号
    private int col; // 槽序号
    private double price; // 商品价格
    private int slottypeid;
    private int slotstateid;
    private String slottype;
    private String slotstate;
    private String categoryid;

    public String getMchid() {
        return mchid;
    }

    public void setMchid(String mchid) {
        this.mchid = mchid;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getProductthumbnail() {
        return productthumbnail;
    }

    public void setProductthumbnail(String productthumbnail) {
        this.productthumbnail = productthumbnail;
    }

    public String getProductimg() {
        return productimg;
    }

    public void setProductimg(String productimg) {
        this.productimg = productimg;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getMaxcount() {
        return maxcount;
    }

    public void setMaxcount(int maxcount) {
        this.maxcount = maxcount;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public int getLayer() {
        return layer;
    }

    public void setLayer(int layer) {
        this.layer = layer;
    }

    public int getCab() {
        return cab;
    }

    public void setCab(int cab) {
        this.cab = cab;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getSlottypeid() {
        return slottypeid;
    }

    public void setSlottypeid(int slottypeid) {
        this.slottypeid = slottypeid;
    }

    public int getSlotstateid() {
        return slotstateid;
    }

    public void setSlotstateid(int slotstateid) {
        this.slotstateid = slotstateid;
    }

    public String getSlottype() {
        return slottype;
    }

    public void setSlottype(String slottype) {
        this.slottype = slottype;
    }

    public String getSlotstate() {
        return slotstate;
    }

    public void setSlotstate(String slotstate) {
        this.slotstate = slotstate;
    }

    public String getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(String categoryid) {
        this.categoryid = categoryid;
    }

    @Override
    public String toString() {
        return "GoodsBean{" +
                "_id=" + _id +
                ", mchid='" + mchid + '\'' +
                ", index=" + index +
                ", productid='" + productid + '\'' +
                ", productthumbnail='" + productthumbnail + '\'' +
                ", productimg='" + productimg + '\'' +
                ", count=" + count +
                ", maxcount=" + maxcount +
                ", productname='" + productname + '\'' +
                ", layer=" + layer +
                ", cab=" + cab +
                ", col=" + col +
                ", price=" + price +
                ", slottypeid=" + slottypeid +
                ", slotstateid=" + slotstateid +
                ", slottype='" + slottype + '\'' +
                ", slotstate='" + slotstate + '\'' +
                ", categoryid='" + categoryid + '\'' +
                '}';
    }
}
