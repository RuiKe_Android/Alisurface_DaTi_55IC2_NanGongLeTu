package com.ruike.alisurface.bean;

import java.util.List;

/**
 * Author: voodoo
 * CreateDate: 2020/07/03 003 04:43 下午
 * Description: 卡支付按照货道创建的需要上传的订单对象
 */
public class CardCreateOrderBySlotBean {

    /**
     * mchid : 123456
     * card_no : 123456
     * card_pwd : 123456
     * details : [{"slotIndex":1,"quantity":2},{"slotIndex":5,"quantity":2},{"slotIndex":3,"quantity":2}]
     */
    private String mchid; // 机器ID
    private String card_no; // 卡号
    private String card_pwd; // 卡密码
    private List<DetailsBean> details; // 购物详情

    public String getMchid() {
        return mchid;
    }

    public void setMchid(String mchid) {
        this.mchid = mchid;
    }

    public String getCard_no() {
        return card_no;
    }

    public void setCard_no(String card_no) {
        this.card_no = card_no;
    }

    public String getCard_pwd() {
        return card_pwd;
    }

    public void setCard_pwd(String card_pwd) {
        this.card_pwd = card_pwd;
    }

    public List<DetailsBean> getDetails() {
        return details;
    }

    public void setDetails(List<DetailsBean> details) {
        this.details = details;
    }

    public static class DetailsBean {
        /**
         * slotIndex : 1
         * quantity : 2
         */

        private int slotIndex; // 货道
        private int quantity; // 数量

        public int getSlotIndex() {
            return slotIndex;
        }

        public void setSlotIndex(int slotIndex) {
            this.slotIndex = slotIndex;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        @Override
        public String toString() {
            return "DetailsBean{" +
                    "slotIndex=" + slotIndex +
                    ", quantity=" + quantity +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "CardCreateOrderBySlotBean{" +
                "mchid='" + mchid + '\'' +
                ", card_no='" + card_no + '\'' +
                ", card_pwd='" + card_pwd + '\'' +
                ", details=" + details +
                '}';
    }
}
