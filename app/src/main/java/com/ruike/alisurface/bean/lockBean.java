package com.ruike.alisurface.bean;

import com.voodoo.lib_databases.annotation.Id;
import com.voodoo.lib_databases.annotation.Table;

@Table(name = "lockBean")
public class lockBean {
    public lockBean( ) {

    }

    @Override
    public String toString() {
        return "lockBean{" +
                "msgid='" + msgid + '\'' +
                ", boxno=" + boxno +
                ", id=" + id +
                '}';
    }

    public String getMsgid() {
        return msgid;
    }

    public void setMsgid(String msgid) {
        this.msgid = msgid;
    }

    public int getBoxno() {
        return boxno;
    }

    public void setBoxno(int boxno) {
        this.boxno = boxno;
    }

    String  msgid;
    int boxno;

    @Id(column = "id")
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
