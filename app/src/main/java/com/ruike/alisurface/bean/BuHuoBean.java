package com.ruike.alisurface.bean;

import java.util.List;



public class BuHuoBean {

    /**
     * mchid : 100
     * replenishmenter_list : [{"slot_id":"100-10","slotindex":10,"count":11},{"slot_id":"100-2","slotindex":2,"count":11}]
     */
    private String mchid;
    private List<ReplenishmenterListBean> replenishmenter_list;

    public String getMchid() {
        return mchid;
    }

    public void setMchid(String mchid) {
        this.mchid = mchid;
    }

    public List<ReplenishmenterListBean> getReplenishmenter_list() {
        return replenishmenter_list;
    }

    public void setReplenishmenter_list(List<ReplenishmenterListBean> replenishmenter_list) {
        this.replenishmenter_list = replenishmenter_list;
    }

    public static class ReplenishmenterListBean {
        /**
         * slot_id : 100-10
         * slotindex : 10
         * count : 11
         */

        private String slot_id;
        private int slotindex;
        private int count;

        public String getSlot_id() {
            return slot_id;
        }

        public void setSlot_id(String slot_id) {
            this.slot_id = slot_id;
        }

        public int getSlotindex() {
            return slotindex;
        }

        public void setSlotindex(int slotindex) {
            this.slotindex = slotindex;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }
    }
}
