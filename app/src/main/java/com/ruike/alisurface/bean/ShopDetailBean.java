package com.ruike.alisurface.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 出货商品类
 */
public class ShopDetailBean implements Serializable {


    private String OrderId;
    private double Total_fee;
    private double Pay_fee;
    private double Discount_fee;
    private double Promotion_fee;
    List<OrderDetailsBean> detailOrderList;

    @Override
    public String toString() {
        return "ShopDetailBean{" +
                "OrderId='" + OrderId + '\'' +
                ", Total_fee=" + Total_fee +
                ", Pay_fee=" + Pay_fee +
                ", Discount_fee=" + Discount_fee +
                ", Promotion_fee=" + Promotion_fee +
                ", detailOrderList=" + detailOrderList +
                '}';
    }

    public double getPromotion_fee() {
        return Promotion_fee;
    }

    public void setPromotion_fee(double promotion_fee) {
        Promotion_fee = promotion_fee;
    }

    public double getTotal_fee() {
        return Total_fee;
    }

    public void setTotal_fee(double total_fee) {
        Total_fee = total_fee;
    }

    public double getPay_fee() {
        return Pay_fee;
    }

    public void setPay_fee(double pay_fee) {
        Pay_fee = pay_fee;
    }

    public double getDiscount_fee() {
        return Discount_fee;
    }

    public void setDiscount_fee(double discount_fee) {
        Discount_fee = discount_fee;
    }

    public ShopDetailBean(String orderId) {
        OrderId = orderId;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }


    public List<OrderDetailsBean> getDetailOrderList() {
        return detailOrderList;
    }

    public void setDetailOrderList(List<OrderDetailsBean> detailOrderList) {
        this.detailOrderList = detailOrderList;
    }

    public static class OrderDetailsBean implements Serializable {
        @Override
        public String toString() {
            return "OrderDetailsBean{" +
                    "ProductName='" + ProductName + '\'' +
                    ", ProductImg='" + ProductImg + '\'' +
                    ", Price=" + Price +
                    ", Quantity=" + Quantity +
                    ", ProductId='" + ProductId + '\'' +
                    ", SlotIndex=" + SlotIndex +
                    ", OrderId='" + OrderId + '\'' +
                    ", OutQuantity=" + OutQuantity +
                    ", SlotTypeId=" + SlotTypeId +
                    ", IsUpdate=" + IsUpdate +
                    ", Id='" + Id + '\'' +
                    ", CreateOn='" + CreateOn + '\'' +
                    ", UpdateOn='" + UpdateOn + '\'' +
                    ", CreateBy=" + CreateBy +
                    ", UpdateBy=" + UpdateBy +
                    ", IsDelete=" + IsDelete +
                    ", Verion=" + Verion +
                    '}';
        }

        /**
         * ProductName : 图片测试1
         * ProductImg : http://120.78.219.34:8018/UploadFile/product/8dc54969-b939-49e8-b2af-a5b57105c366.png
         * Price : 0.01
         * Quantity : 1
         * ProductId : 6fd9b8eb-6946-4507-aaaf-10cd7174c18b
         * SlotIndex : 1
         * OrderId : 2019021310100801782400
         * OutQuantity : null
         * SlotTypeId : 21003
         * IsUpdate : false
         * Id : 0b89ce20-34c7-44b6-8a39-b08cee2325de
         * CreateOn : 2019-02-13 10:10:08
         * UpdateOn : 2019-02-13 10:10:08
         * CreateBy : null
         * UpdateBy : null
         * IsDelete : false
         * Verion : null
         */

        private String ProductName;
        private String ProductImg;
        private double Price;
        private int Quantity;
        private String ProductId;
        private int SlotIndex;
        private String OrderId;
        private Object OutQuantity;
        private int SlotTypeId = 21001;
        private boolean IsUpdate;
        private String Id;
        private String CreateOn;
        private String UpdateOn;
        private Object CreateBy;
        private Object UpdateBy;
        private boolean IsDelete;
        private Object Verion;

        public String getProductName() {
            return ProductName;
        }

        public void setProductName(String ProductName) {
            this.ProductName = ProductName;
        }

        public String getProductImg() {
            return ProductImg;
        }

        public void setProductImg(String ProductImg) {
            this.ProductImg = ProductImg;
        }

        public double getPrice() {
            return Price;
        }

        public void setPrice(double Price) {
            this.Price = Price;
        }

        public int getQuantity() {
            return Quantity;
        }

        public void setQuantity(int Quantity) {
            this.Quantity = Quantity;
        }

        public String getProductId() {
            return ProductId;
        }

        public void setProductId(String ProductId) {
            this.ProductId = ProductId;
        }

        public int getSlotIndex() {
            return SlotIndex;
        }

        public void setSlotIndex(int SlotIndex) {
            this.SlotIndex = SlotIndex;
        }

        public String getOrderId() {
            return OrderId;
        }

        public void setOrderId(String OrderId) {
            this.OrderId = OrderId;
        }

        public Object getOutQuantity() {
            return OutQuantity;
        }

        public void setOutQuantity(Object OutQuantity) {
            this.OutQuantity = OutQuantity;
        }

        public int getSlotTypeId() {
            return SlotTypeId;
        }

        public void setSlotTypeId(int SlotTypeId) {
            this.SlotTypeId = SlotTypeId;
        }

        public boolean isIsUpdate() {
            return IsUpdate;
        }

        public void setIsUpdate(boolean IsUpdate) {
            this.IsUpdate = IsUpdate;
        }

        public String getId() {
            return Id;
        }

        public void setId(String Id) {
            this.Id = Id;
        }

        public String getCreateOn() {
            return CreateOn;
        }

        public void setCreateOn(String CreateOn) {
            this.CreateOn = CreateOn;
        }

        public String getUpdateOn() {
            return UpdateOn;
        }

        public void setUpdateOn(String UpdateOn) {
            this.UpdateOn = UpdateOn;
        }

        public Object getCreateBy() {
            return CreateBy;
        }

        public void setCreateBy(Object CreateBy) {
            this.CreateBy = CreateBy;
        }

        public Object getUpdateBy() {
            return UpdateBy;
        }

        public void setUpdateBy(Object UpdateBy) {
            this.UpdateBy = UpdateBy;
        }

        public boolean isIsDelete() {
            return IsDelete;
        }

        public void setIsDelete(boolean IsDelete) {
            this.IsDelete = IsDelete;
        }

        public Object getVerion() {
            return Verion;
        }

        public void setVerion(Object Verion) {
            this.Verion = Verion;
        }
    }

}
