package com.ruike.alisurface.bean;

/**
 * Author: voodoo
 * CreateDate: 2020-05-18 018上午 09:45
 * Description: 卡信息
 */
public class CardInfoBean {


    /**
     * data : {"card_no":"2708525529","expiration_time":"2025-07-03 00:00:00","now_balance":1500.01}
     * code : 0
     * msg : success
     * sendData : 2020-07-03 14:52:59
     */

    private DataBean data;
    private int code;
    private String msg;
    private String sendData;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSendData() {
        return sendData;
    }

    public void setSendData(String sendData) {
        this.sendData = sendData;
    }

    public static class DataBean {
        /**
         * card_no : 2708525529
         * expiration_time : 2025-07-03 00:00:00
         * now_balance : 1500.01
         */

        private String card_no;
        private String expiration_time;
        private double now_balance;

        public String getCard_no() {
            return card_no;
        }

        public void setCard_no(String card_no) {
            this.card_no = card_no;
        }

        public String getExpiration_time() {
            return expiration_time;
        }

        public void setExpiration_time(String expiration_time) {
            this.expiration_time = expiration_time;
        }

        public double getNow_balance() {
            return now_balance;
        }

        public void setNow_balance(double now_balance) {
            this.now_balance = now_balance;
        }
    }
}
