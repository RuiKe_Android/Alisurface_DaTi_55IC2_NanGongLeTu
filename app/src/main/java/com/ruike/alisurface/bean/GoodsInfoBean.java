package com.ruike.alisurface.bean;

/**
 * 商品详情界面回传对象
 * Created by admin on 2019/11/27.
 */

public class GoodsInfoBean {

    /**
     * msg : Success
     * code : 0
     * Des : 农夫果园-高浓度果蔬汁饮料，与家人分享健康
     * <p>
     * 农夫果园是中、高浓度的果蔬汁饮料，采用安全放心的UHT瞬间杀菌技术，保证果汁的安全性。
     * <p>
     * 农夫果园16年后首度推新，新系列果汁含量升级到50%。 桃子全新口味添加桃原浆，尽享清甜，营养美味。
     * <p>
     * 农夫山泉相信，好果汁是种出来的。农夫果园甄选六种营 养果蔬，来自世界各地果蔬优质产区:以色列，土耳其， 智利，西班牙，新疆等，精心的搭配只为提供美味享受。
     * <p>
     * <p>
     * TemplateContent : {"Tid":"1166513483488038912","CateName":"饮料","Column":[{"ColumnName":"项目","ColumnID":"Column_0"},{"ColumnName":"每份","ColumnID":"Column_1"},{"ColumnName":"营养素参考值","ColumnID":"Column_2"}],"Sort":0}
     * StructureContent : [{"Column_0":"能量","Column_1":"156千焦（KJ）","Column_2":"2%"},{"Column_0":"蛋白质","Column_1":"0 g","Column_2":"0%"},{"Column_2":"0%","Column_1":"0 g","Column_0":"脂肪"},{"Column_0":"碳水化合物","Column_1":"8.0 g","Column_2":"9%"},{"Column_0":"钙","Column_1":"29mg","Column_2":"1%"}]
     */

    private String msg;
    private int code;
    private String Des;
    private String TemplateContent;
    private String StructureContent;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDes() {
        return Des;
    }

    public void setDes(String Des) {
        this.Des = Des;
    }

    public String getTemplateContent() {
        return TemplateContent;
    }

    public void setTemplateContent(String TemplateContent) {
        this.TemplateContent = TemplateContent;
    }

    public String getStructureContent() {
        return StructureContent;
    }

    public void setStructureContent(String StructureContent) {
        this.StructureContent = StructureContent;
    }

    public class TemplateContentColumnBean {
        //    {"ColumnName":"项目","ColumnID":"Column_0"}
        private String ColumnName;
        private String ColumnID;

        public String getColumnName() {
            return ColumnName;
        }

        public void setColumnName(String columnName) {
            ColumnName = columnName;
        }

        public String getColumnID() {
            return ColumnID;
        }

        public void setColumnID(String columnID) {
            ColumnID = columnID;
        }
    }
}
