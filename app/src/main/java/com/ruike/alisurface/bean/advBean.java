package com.ruike.alisurface.bean;


import com.voodoo.lib_databases.annotation.Id;
import com.voodoo.lib_databases.annotation.Table;

/**
 * Created by czb on 2019-12-25.
 */
@Table(name = "advBean")
public class advBean {

    @Id(column = "id")
    private int id;

    String url;

    String types;
    int sort;

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }



}
