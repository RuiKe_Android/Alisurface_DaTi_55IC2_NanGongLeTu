package com.ruike.alisurface.bean;

import java.util.List;

/**
 * Author: voodoo
 * CreateDate: 2020/07/03 003 04:43 下午
 * Description: 卡支付按照商品创建的需要上传的订单对象
 */
public class CardCreateOrderByProductBean {

    /**
     * mchid : 123456
     * card_no : 123456
     * card_pwd : 123456
     * details : [{"productid":"123456","quantity":12,"slotIndexs":[1,2,3]}]
     */

    private String mchid;
    private String card_no;
    private String card_pwd;
    private List<DetailsBean> details;

    public String getMchid() {
        return mchid;
    }

    public void setMchid(String mchid) {
        this.mchid = mchid;
    }

    public String getCard_no() {
        return card_no;
    }

    public void setCard_no(String card_no) {
        this.card_no = card_no;
    }

    public String getCard_pwd() {
        return card_pwd;
    }

    public void setCard_pwd(String card_pwd) {
        this.card_pwd = card_pwd;
    }

    public List<DetailsBean> getDetails() {
        return details;
    }

    public void setDetails(List<DetailsBean> details) {
        this.details = details;
    }

    public static class DetailsBean {
        /**
         * productid : 123456
         * quantity : 12
         * slotIndexs : [1,2,3]
         */

        private String productid;
        private int quantity;
        private List<Integer> slotIndexs;

        public String getProductid() {
            return productid;
        }

        public void setProductid(String productid) {
            this.productid = productid;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public List<Integer> getSlotIndexs() {
            return slotIndexs;
        }

        public void setSlotIndexs(List<Integer> slotIndexs) {
            this.slotIndexs = slotIndexs;
        }

        @Override
        public String toString() {
            return "DetailsBean{" +
                    "productid='" + productid + '\'' +
                    ", quantity=" + quantity +
                    ", slotIndexs=" + slotIndexs +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "CardCreateOrderByProductBean{" +
                "mchid='" + mchid + '\'' +
                ", card_no='" + card_no + '\'' +
                ", card_pwd='" + card_pwd + '\'' +
                ", details=" + details +
                '}';
    }
}
