package com.ruike.alisurface.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2018-09-07.
 */
public class OrderrBean implements Serializable {

    /**
     * msg : SUCCESS
     * code : 0
     * data : {"url":"/App/Order/Pay?orderno=2018090716070000015769","order":{"OrderNo":"2018090716070000015769","CustomerId":null,"MchId":"20180710014474","Fee_typeId":0,"M_NO":null,"O_state":0,"Pay_time":null,"Total_fee":0.1,"Pay_fee":0.1,"Get_fee":0.099,"Charge_fee":0,"Discount_fee":0,"Currency":"CNY","Remark":"[锁骨]  ","AppId":null,"Extra":null,"OpenId":null,"TradeNo":null,"OrderType":"普通订单","OrderDetails":[{"ProductName":"锁骨","ProductImg":"http://120.78.219.34:8018/UploadFile/0810.png","Price":0.1,"Quantity":1,"Product_Id":"5d87a263-9a28-49ad-8765-a4d34be6ec36","SlotIndex":1,"OrderId":"2018090716070000015769","Id":"6e41f8e5-86b3-4914-b810-07f96e36b843","CreateOn":"2018-09-07 16:07:00","UpdateOn":"2018-09-07 16:07:00","CreateBy":null,"UpdateBy":null,"IsDelete":false}],"BaseUserId":null,"Id":"2018090716070000015769","CreateOn":"2018-09-07 16:07:00","UpdateOn":"2018-09-07 16:07:00","CreateBy":null,"UpdateBy":null,"IsDelete":false}}
     */

    private String msg;
    private int code;
    private DataBean data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * url : /App/Order/Pay?orderno=2018090716070000015769
         * order : {"OrderNo":"2018090716070000015769","CustomerId":null,"MchId":"20180710014474","Fee_typeId":0,"M_NO":null,"O_state":0,"Pay_time":null,"Total_fee":0.1,"Pay_fee":0.1,"Get_fee":0.099,"Charge_fee":0,"Discount_fee":0,"Currency":"CNY","Remark":"[锁骨]  ","AppId":null,"Extra":null,"OpenId":null,"TradeNo":null,"OrderType":"普通订单","OrderDetails":[{"ProductName":"锁骨","ProductImg":"http://120.78.219.34:8018/UploadFile/0810.png","Price":0.1,"Quantity":1,"Product_Id":"5d87a263-9a28-49ad-8765-a4d34be6ec36","SlotIndex":1,"OrderId":"2018090716070000015769","Id":"6e41f8e5-86b3-4914-b810-07f96e36b843","CreateOn":"2018-09-07 16:07:00","UpdateOn":"2018-09-07 16:07:00","CreateBy":null,"UpdateBy":null,"IsDelete":false}],"BaseUserId":null,"Id":"2018090716070000015769","CreateOn":"2018-09-07 16:07:00","UpdateOn":"2018-09-07 16:07:00","CreateBy":null,"UpdateBy":null,"IsDelete":false}
         */

        private String url;
        private OrderBean order;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public OrderBean getOrder() {
            return order;
        }

        public void setOrder(OrderBean order) {
            this.order = order;
        }

        public static class OrderBean {
            public double getPromotion_fee() {
                return Promotion_fee;
            }

            public void setPromotion_fee(Double promotion_fee) {
                Promotion_fee = promotion_fee;
            }

            /**
             * OrderNo : 2018090716070000015769
             * CustomerId : null
             * MchId : 20180710014474
             * Fee_typeId : 0
             * M_NO : null
             * O_state : 0
             * Pay_time : null
             * Total_fee : 0.1
             * Pay_fee : 0.1
             * Get_fee : 0.099
             * Charge_fee : 0
             * Discount_fee : 0
             * Currency : CNY
             * Remark : [锁骨]
             * AppId : null
             * Extra : null
             * OpenId : null
             * TradeNo : null
             * OrderType : 普通订单
             * OrderDetails : [{"ProductName":"锁骨","ProductImg":"http://120.78.219.34:8018/UploadFile/0810.png","Price":0.1,"Quantity":1,"Product_Id":"5d87a263-9a28-49ad-8765-a4d34be6ec36","SlotIndex":1,"OrderId":"2018090716070000015769","Id":"6e41f8e5-86b3-4914-b810-07f96e36b843","CreateOn":"2018-09-07 16:07:00","UpdateOn":"2018-09-07 16:07:00","CreateBy":null,"UpdateBy":null,"IsDelete":false}]
             * BaseUserId : null
             * Id : 2018090716070000015769
             * CreateOn : 2018-09-07 16:07:00
             * UpdateOn : 2018-09-07 16:07:00
             * CreateBy : null
             * UpdateBy : null
             * IsDelete : false
             */

            private double Promotion_fee;


            private String activity_id;

            public String getActivity_id() {
                return activity_id;
            }

            public void setActivity_id(String activity_id) {
                this.activity_id = activity_id;
            }

            public String getActivity_config() {
                return activity_config;
            }

            public void setActivity_config(String activity_config) {
                this.activity_config = activity_config;
            }

            private String activity_config;
            private String OrderNo;
            private Object CustomerId;
            private String MchId;
            private int Fee_typeId;
            private Object M_NO;
            private int O_state;
            private Object Pay_time;
            private double Total_fee;//总金额
            private double Pay_fee;//应付金额
            private double Get_fee;
            private int Charge_fee;
            private int Discount_fee;//优惠金额，判断是否有优惠，如果有则显示出来
            private String Currency;
            private String Remark;
            private Object AppId;
            private Object Extra;
            private Object OpenId;
            private Object TradeNo;
            private String OrderType;
            private Object BaseUserId;
            private String Id;
            private String CreateOn;
            private String UpdateOn;
            private Object CreateBy;
            private Object UpdateBy;
            private boolean IsDelete;
            private List<ShopDetailBean.OrderDetailsBean> OrderDetails;

            public String getOrderNo() {
                return OrderNo;
            }

            public void setOrderNo(String OrderNo) {
                this.OrderNo = OrderNo;
            }

            public Object getCustomerId() {
                return CustomerId;
            }

            public void setCustomerId(Object CustomerId) {
                this.CustomerId = CustomerId;
            }

            public String getMchId() {
                return MchId;
            }

            public void setMchId(String MchId) {
                this.MchId = MchId;
            }

            public int getFee_typeId() {
                return Fee_typeId;
            }

            public void setFee_typeId(int Fee_typeId) {
                this.Fee_typeId = Fee_typeId;
            }

            public Object getM_NO() {
                return M_NO;
            }

            public void setM_NO(Object M_NO) {
                this.M_NO = M_NO;
            }

            public int getO_state() {
                return O_state;
            }

            public void setO_state(int O_state) {
                this.O_state = O_state;
            }

            public Object getPay_time() {
                return Pay_time;
            }

            public void setPay_time(Object Pay_time) {
                this.Pay_time = Pay_time;
            }

            public double getTotal_fee() {
                return Total_fee;
            }

            public void setTotal_fee(double Total_fee) {
                this.Total_fee = Total_fee;
            }

            public double getPay_fee() {
                return Pay_fee;
            }

            public void setPay_fee(double Pay_fee) {
                this.Pay_fee = Pay_fee;
            }

            public double getGet_fee() {
                return Get_fee;
            }

            public void setGet_fee(double Get_fee) {
                this.Get_fee = Get_fee;
            }

            public int getCharge_fee() {
                return Charge_fee;
            }

            public void setCharge_fee(int Charge_fee) {
                this.Charge_fee = Charge_fee;
            }

            public int getDiscount_fee() {
                return Discount_fee;
            }

            public void setDiscount_fee(int Discount_fee) {
                this.Discount_fee = Discount_fee;
            }

            public String getCurrency() {
                return Currency;
            }

            public void setCurrency(String Currency) {
                this.Currency = Currency;
            }

            public String getRemark() {
                return Remark;
            }

            public void setRemark(String Remark) {
                this.Remark = Remark;
            }

            public Object getAppId() {
                return AppId;
            }

            public void setAppId(Object AppId) {
                this.AppId = AppId;
            }

            public Object getExtra() {
                return Extra;
            }

            public void setExtra(Object Extra) {
                this.Extra = Extra;
            }

            public Object getOpenId() {
                return OpenId;
            }

            public void setOpenId(Object OpenId) {
                this.OpenId = OpenId;
            }

            public Object getTradeNo() {
                return TradeNo;
            }

            public void setTradeNo(Object TradeNo) {
                this.TradeNo = TradeNo;
            }

            public String getOrderType() {
                return OrderType;
            }

            public void setOrderType(String OrderType) {
                this.OrderType = OrderType;
            }

            public Object getBaseUserId() {
                return BaseUserId;
            }

            public void setBaseUserId(Object BaseUserId) {
                this.BaseUserId = BaseUserId;
            }

            public String getId() {
                return Id;
            }

            public void setId(String Id) {
                this.Id = Id;
            }

            public String getCreateOn() {
                return CreateOn;
            }

            public void setCreateOn(String CreateOn) {
                this.CreateOn = CreateOn;
            }

            public String getUpdateOn() {
                return UpdateOn;
            }

            public void setUpdateOn(String UpdateOn) {
                this.UpdateOn = UpdateOn;
            }

            public Object getCreateBy() {
                return CreateBy;
            }

            public void setCreateBy(Object CreateBy) {
                this.CreateBy = CreateBy;
            }

            public Object getUpdateBy() {
                return UpdateBy;
            }

            public void setUpdateBy(Object UpdateBy) {
                this.UpdateBy = UpdateBy;
            }

            public boolean isIsDelete() {
                return IsDelete;
            }

            public void setIsDelete(boolean IsDelete) {
                this.IsDelete = IsDelete;
            }

            public List<ShopDetailBean.OrderDetailsBean> getOrderDetails() {
                return OrderDetails;
            }

            public void setOrderDetails(List<ShopDetailBean.OrderDetailsBean> OrderDetails) {
                this.OrderDetails = OrderDetails;
            }


        }
    }
}
