package com.ruike.alisurface.bean;

import com.voodoo.lib_databases.annotation.Id;
import com.voodoo.lib_databases.annotation.OneToMany;
import com.voodoo.lib_databases.annotation.Table;

import java.io.Serializable;
import java.util.List;

@Table(name = "CarGoodsBeans")
public class CarGoodsBeans implements Serializable {
    /**
     * productname : 可乐测试
     * productimg : http://120.78.219.34:8018/UploadFile/product/b0fdf20f-f21b-46d5-a81a-b5e0ea967999.jpg
     * productid : 86475b88-c6a5-4870-b44b-805edc188f89
     * price : 0.01
     * count : 1
     * slotIndexs : [1]
     */
    @Id(column = "id")
    private int id;

    private String productname;
    private String productimg;
    private String productid;
    private double price;
    private int count;

    private String slotIndexs;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getProductimg() {
        return productimg;
    }

    public void setProductimg(String productimg) {
        this.productimg = productimg;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getSlotIndexs() {
        return slotIndexs;
    }

    public void setSlotIndexs(String slotIndexs) {
        this.slotIndexs = slotIndexs;
    }

    @Override
    public String toString() {
        return "CarGoodsBeans{" +
                "id=" + id +
                ", productname='" + productname + '\'' +
                ", productimg='" + productimg + '\'' +
                ", productid='" + productid + '\'' +
                ", price=" + price +
                ", count=" + count +
                ", slotIndexs='" + slotIndexs + '\'' +
                '}';
    }
}
