package com.ruike.alisurface.manager;

public interface PageDecorationLastJudge {
    /**
     * 是否是最后一行
     *
     * @param position 数据下标
     * @return
     */
    boolean isLastRow(int position);

    /**
     * 是否是最后一列
     *
     * @param position 数据下标
     * @return
     */
    boolean isLastColumn(int position);

    /**
     * 是否是最后一页
     *
     * @param position 数据下标
     * @return
     */
    boolean isPageLast(int position);
}