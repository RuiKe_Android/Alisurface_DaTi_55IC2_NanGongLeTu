package com.ruike.alisurface.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.voodoo.lib_frame.base.BaseFragment;

/**
 * Author: voodoo
 * CreateDate: 2020-03-26 026 上午 10:39
 * Description: 所有Activity的基类
 */
public abstract class BaseFgt extends BaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

}
