package com.ruike.alisurface.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import com.ruike.alisurface.BuildConfig;
import com.voodoo.lib_utils.L;

/**
 * Author: voodoo
 * CreateDate: 2020/04/30 030 09:15 上午
 * Description: 系统开机广播接收
 */
public class DeviceRestartReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // 开机广播
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            L.i("机器开机启动完成，即将打开 " + BuildConfig.APPLICATION_ID + " 版本号：" + BuildConfig.VERSION_CODE);

            L.i("延时时间 5 秒启动 " + BuildConfig.APPLICATION_ID);
            SystemClock.sleep(5 * 1000);

            String PackageName = context.getPackageName();
            Intent LaunchIntent = context.getPackageManager().getLaunchIntentForPackage(PackageName);
            LaunchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(LaunchIntent);
        }
    }

}
