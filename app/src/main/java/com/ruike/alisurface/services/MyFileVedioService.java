package com.ruike.alisurface.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Environment;

import com.ruike.alisurface.MyApplication;
import com.ruike.alisurface.Serials.MsgEventBus;
import com.ruike.alisurface.bean.advBean;
import com.ruike.alisurface.utils.MYUtiles;
import com.voodoo.lib_frame.base.BaseView;
import com.voodoo.lib_frame.httpTools.DownloadUtil;
import com.voodoo.lib_utils.FileUtils;
import com.voodoo.lib_utils.L;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class MyFileVedioService extends IntentService implements BaseView {

    public MyFileVedioService() {
        super("MyFileVedioService");

    }

    @Override
    public void onCreate() {
        super.onCreate();

    }


    @Override
    protected void onHandleIntent(Intent intent) {
        strpicList.clear();
        urllist.clear();
        Count = 0;
        try {
            showVedio();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    List<String> strpicList = new ArrayList<>();
    int Count = 0;
    List<String> urllist = new ArrayList<>();
    String filepath = Environment.getExternalStorageDirectory()
            + File.separator + "picture" + File.separator + "Vedio";
    /**
     * 下载 广告视频
     */
    public void showVedio() {
        try {
            ArrayList<advBean> imgshop = (ArrayList<advBean>) MyApplication.finalDbUtils.findAllByWhere(advBean.class, " types = '1'");
            //生成当前的数据
            L.i("-----getMCSolt---imgshop---" + imgshop.size());
            if (imgshop != null && imgshop.size() > 0) {

                List<String> list = new ArrayList<>();
                for (int intI = 0; intI < imgshop.size(); intI++) {
                    String p1 = imgshop.get(intI).getUrl();
                    if (!p1.equals("") && p1 != null && !p1.equals("null")) {
                        if (!list.contains(p1)) {
                            list.add(p1);
                            String Name = p1.substring(p1.lastIndexOf("/")).substring(1);
                            strpicList.add(Name);

                        }
                    }
                }
                //判断文件夹中是否有该文件
                for (int i = 0; i < strpicList.size(); i++) {

                    File myFile = new File(filepath, strpicList.get(i));

                    if (!myFile.exists()) {
                        L.i("-----downvdeioFile---" + list.get(i));
                        Count++;
                        downvdeioFile(list.get(i), 1 + "");
                        MYUtiles.fileScan(this, filepath);
                    } else {
                        EventBus.getDefault().post(new MsgEventBus("vedio", "视频下载成功"));
                        Count--;
                        if (Count < 0) {
                            Count = 0;
                        }
                        if (Count == 0) {
                            delFile(strpicList);
                        }
                        MYUtiles.fileScan(this, filepath);

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    String uploadUrl1;

    //下载ship
    public void downvdeioFile(final String strNameList, String type) {
        try {
            L.i("-----downvdeioFile---下载ship");
            uploadUrl1 = MYUtiles.toUtf8String(strNameList);
            urllist.add(uploadUrl1);
//                String s1 = uploadUrl.replaceAll(" +", "%20");
            String Name = uploadUrl1.substring(uploadUrl1.lastIndexOf("/")).substring(1);

            DownloadUtil.getInstance().download(uploadUrl1, filepath, Name, this);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void delFile(List<String> strpicList) {
        List<File> strlist = new ArrayList<>();
        strlist.clear();
        File[] folderFileList = FileUtils.getFolderFileList(filepath);
        L.i("tjf--delect--" + strpicList.toString());
        for (File file : folderFileList) {
            if (!strpicList.contains(file.getName())) {
                strlist.add(file);
            }
        }
        L.i("tjf--delect2222-" + strlist.toString());
        for (File file : strlist) {
            L.i(file.getName() + "-----删除文件---");
            if (file.exists()) {
                file.delete();
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    @Override
    public void onStarted(String loadingStr) {

    }

    @Override
    public void onLoading(long total, long current) {

    }

    @Override
    public void onComplete() {

    }

    @Override
    public void onErrorTip(String errorStr) {

    }

    @Override
    public void onSuccess(String requestUrl, String requestJsonStr) {

    }

    @Override
    public void onFailure(String requestUrl, String requestMsg) {

    }

    @Override
    public void onError(String requestUrl, String errorMsg) {

    }

    @Override
    public void onException(Exception exception) {

    }

    @Override
    public void onDownloadSuccess(String requestUrl, File file) {

        if (urllist.contains(requestUrl)) {
            Count--;
            L.i(Count + "----视频下载成功 ----" + file.getAbsolutePath());
            if (Count == 0) {
                delFile(strpicList);
            }
            EventBus.getDefault().post(new MsgEventBus("vedio", "视频下载成功"));
        }
    }

    @Override
    public void onDownloading(String requestUrl, int progress) {


    }

    @Override
    public void onDownloadFailed(String requestUrl, Exception e) {

        L.i("---视频下载失败-Exception-requestUrl--" + requestUrl
                + "======" + e.toString());

    }
}