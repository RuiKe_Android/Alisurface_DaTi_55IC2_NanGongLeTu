package com.ruike.alisurface.services;

import android.app.IntentService;
import android.content.Intent;

import com.alipay.iot.sdk.APIManager;
import com.alipay.iot.sdk.InitFinishCallback;
import com.voodoo.lib_utils.L;

public class MyIntentService extends IntentService {

    public MyIntentService() {
        super("MyApkService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    /**
     * 刷脸支付itosdk植入
     */
    private void InitSdk() {
        try {
            APIManager.getInstance().initialize(getApplicationContext(), "2088821660782062", new InitFinishCallback() {
                @Override
                public void initFinished(boolean b) {
                    L.i(   "刷脸支付 验证是否注册 " + b);
                }
            });
        } catch (APIManager.APIInitException e) {
            L.e( "刷脸支付 APIInitException " + e);
        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            L.i(  "支付宝后台注册开始植入检测");
            InitSdk();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}