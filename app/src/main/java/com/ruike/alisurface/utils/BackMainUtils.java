package com.ruike.alisurface.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.ruike.alisurface.ui.mainPage.MainActivity;
import com.ruike.alisurface.ui.mainPage.MainTypeActivity;
import com.ruike.alisurface.ui.mainPage.ShowQrCodeActivity;
import com.ruike.alisurface.ui.setting.StopSellingActivity;
import com.voodoo.lib_utils.L;
import com.voodoo.lib_utils.ShareUtils;

public class BackMainUtils {

    public static void setActivity(Activity context) {
        Intent intent;
        boolean stop_sp = ShareUtils.getInstance().getBoolean(ShareKey.IS_STOP_SELLING, false); // 停止售卖
        if (stop_sp) {
            intent = new Intent(context, StopSellingActivity.class);
        } else {
            intent = new Intent(context, ShowQrCodeActivity.class);
            if (ShareUtils.getInstance().getInt(ShareKey.IS_SHOP_TYPE, 1) == 1) {
                intent = new Intent(context, ShowQrCodeActivity.class);
            }
        }
        context.startActivity(intent);
        context.overridePendingTransition(com.voodoo.lib_frame.R.anim.page_intent_in_anim, com.voodoo.lib_frame.R.anim.page_intent_out_anim);

    }

    public static Intent setActivity(Context context) {
        Intent intent = null;
        intent = new Intent(context, ShowQrCodeActivity.class);
        if (ShareUtils.getInstance().getInt(ShareKey.IS_SHOP_TYPE, 1) == 1) {
            intent = new Intent(context, ShowQrCodeActivity.class);
        }
        return intent;
    }
}
