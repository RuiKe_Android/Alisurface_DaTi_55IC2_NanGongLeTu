package com.ruike.alisurface.utils;

import android.content.Context;
import android.widget.ImageView;


import com.youth.banner.loader.ImageLoader;

/**
 * Created by admin on 2019/3/28.
 */

public class BannerImageLoader extends ImageLoader {
    @Override
    public void displayImage(Context context, Object path, ImageView imageView) {
        com.voodoo.lib_utils.imageLoader.ImageLoader.loadImage(context, path.toString(), imageView);
    }
}
