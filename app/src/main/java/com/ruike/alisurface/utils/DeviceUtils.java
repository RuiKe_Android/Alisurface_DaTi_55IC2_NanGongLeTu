package com.ruike.alisurface.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.telephony.TelephonyManager;

import com.ruike.alisurface.BuildConfig;
import com.ruike.alisurface.Constant;
import com.ruike.alisurface.MyApplication;

import java.lang.reflect.Method;

import static android.content.Context.TELEPHONY_SERVICE;

/**
 * Author: voodoo
 * CreateDate: 2020-03-26 026 上午 10:58
 * Description: 设备相关工具类
 */
public class DeviceUtils {

    /**
     * 获取机器SN
     *
     * @return 机器sn
     */
    public static String getDeviceSn() {
        String serial = null;
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class);
            serial = (String) get.invoke(c, "ro.serialno");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BuildConfig.DEBUG ? "801001408000b4f5869" : serial;
    }

    @SuppressLint("MissingPermission")
    public static String getSimSerialNumber() {
        TelephonyManager telMgr = (TelephonyManager)MyApplication.getAppContext().getSystemService(TELEPHONY_SERVICE);

        return telMgr.getSimSerialNumber();
    }

}
