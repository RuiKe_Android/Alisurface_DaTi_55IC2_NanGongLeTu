package com.ruike.alisurface.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Author: voodoo
 * CreateDate: 2020-03-27 027 下午 04:03
 * Description: 时间有关工具类
 */
public class TimeUtils {

    /**
     * 将时间戳转换为时间字符串
     * <p>time格式为yyyy-MM-dd HH:mm:ss</p>
     *
     * @param millis 时间戳
     * @return 默认格式的时间字符串
     */
    public static String millis2String(long millis) {
        return millis2String(millis, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 将时间戳转换为时间字符串
     *
     * @param millis  时间戳
     * @param pattern 指定时间格式
     * @return 指定格式的时间字符串
     */
    public static String millis2String(long millis, String pattern) {
        return new SimpleDateFormat(pattern, Locale.getDefault()).format(new Date(millis));
    }

    /**
     * 将时间字符串转为时间戳
     * <p>time格式为yyyy-MM-dd HH:mm:ss</p>
     *
     * @param time 时间字符串
     * @return 毫秒时间戳
     */
    public static long string2Millis(String time) {
        return string2Millis(time, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 将时间字符串转为时间戳
     *
     * @param time    时间字符串
     * @param pattern 时间格式
     * @return 毫秒时间戳
     */
    public static long string2Millis(String time, String pattern) {
        try {
            return new SimpleDateFormat(pattern, Locale.getDefault()).parse(time).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return -1;
    }

}
