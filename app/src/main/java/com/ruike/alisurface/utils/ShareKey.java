package com.ruike.alisurface.utils;

public class ShareKey {

    // 广告类型
    public static final String ADV_TYPE = "adv_tpe"; // 0：轮播图 1：视频 2：无广告
    public static final String IMG_ADV_TIME_DELAY = "advTimeDelay"; // 图片广告轮播等待时间
    public static final String IS_SHOP_TYPE = "is_shop_type"; // 1 货道展示 2  商品类型展示

    public static final String VERSION_NUM = "Version_num";
    public static final String MCH_ID = "mchId"; // 当前机器id

    public static final String PURCHASE_COUNT = "purchaseCount"; // 购买数量
    public static final String IS_OFFLINE_RESTART_APP = "isOfflineRestartApp"; // 断网重启app
    public static final String IS_OFFLINE_RESTART_APPCOUNT = "isOfflineRestartApp_count"; // 断网重启默认次数
    public static final String IS_SHOW_GOODS_INFO = "isShowGoodsInfo"; // 商品详情展示
    public static final String IS_STOP_SELLING = "isStopSelling"; // 停止售卖
    public static final String IS_OPEN_ALIPAY_PAY = "isOpenAlipayPay"; // 是否启用 支付宝刷脸支付

    public static final String LOCK_MSGID = "lock_msgid"; //  mqtt下发的msgid
    //串口回调的类型值
    public static final String Type_Lockopen = "电控锁门指令开锁";
    public static final String Type_Lockclose = "电控锁门指令关锁";
    public static final String Type_Chshopzl = "出货完成指令";
    public static final String Type_Chshopzl_OLD = "旧板子出货完成指令";
    public static final String Type_toGoshopzl = "购买前的通信指令";
    public static final String Type_Steal = "反馈偷盗指令";
    public static final String Type_sendPlankVersion = "sendPlankVersion";//驱动板版本
    public static final String Type_sendWKPlankVersion = "sendWKPlankVersion";//温控板版本
    public static final String SERVICE_TEL = "service_tel";//后台服务电话
    public static final String IC2_SLOT_NUM = "IC2_SLOT_NUM";//IC2 货道号数量
    public static final String UID = "uid";
    //语言
    public static final String LANGUAGE = "language";

    public static final String ACTIVITY_ID = "ACTIVITY_ID";//优惠活动ID
    public static final String BASEUSERID = "BaseUserId";


    // 答题机的Share Key
    public static final String QUESTION_UID = "question_uid"; // 答题人的id
    public static final String STRATEGY_ID = "strategy_id"; // 策略id
}
