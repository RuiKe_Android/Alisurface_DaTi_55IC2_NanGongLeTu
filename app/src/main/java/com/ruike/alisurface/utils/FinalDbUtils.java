package com.ruike.alisurface.utils;

import android.content.Context;

import com.voodoo.lib_databases.FinalDb;
import com.voodoo.lib_databases.sqlite.SqlBuilder;

import java.util.List;

/**
 * Author: voodoo
 * CreateDate: 2020-03-30 030 下午 09:39
 * Description: 数据库工具类
 */
public class FinalDbUtils {

    static FinalDbUtils finalDbUtils;
    FinalDb finalDb;
    String databasesName = "databases.db";

    public static FinalDbUtils getInstance(Context context) {
        if (finalDbUtils == null) {
            synchronized (FinalDbUtils.class) {
                if (finalDbUtils == null) {
                    finalDbUtils = new FinalDbUtils(context);
                }
            }
        }
        return finalDbUtils;
    }

    public FinalDbUtils(Context context) {
        finalDb = FinalDb.create(context, databasesName);
    }

    /**
     * 将对象保存至数据表
     *
     * @param object 保存的数据对象
     * @return 保存结果 true：保存成功 false：保存失败
     */
    public boolean save(Object object) {
        if (object == null) {
            return false;
        }
        try {
            finalDb.save(object);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 将对象列表保存至数据表
     *
     * @param objects 保存的数据对象列表
     * @return 保存结果 true：保存成功 false：保存失败
     */
    public void saveList(List<?> objects) {
        for (Object object : objects) {
            save(object);
        }
    }

    /**
     * 删除数据库中数据
     *
     * @param object 要删除的数据
     * @return 删除结果 true：删除成功 false：删除失败
     */
    public boolean delete(Object object) {
        if (object == null) {
            return false;
        }
        try {
            finalDb.delete(object);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 根据条件删除数据
     *
     * @param clazz    Class
     * @param strWhere 条件为空的时候 将会删除所有的数据
     */
    public void deleteByWhere(Class<?> clazz, String strWhere) {
        finalDb.deleteByWhere(clazz, strWhere);
    }


    /**
     * 删除数据库中所有数据（删除表）
     *
     * @param clazz 要删除的表
     * @return 删除结果 true：删除成功 false：删除失败
     */
    public boolean deleteAll(Class clazz) {
        if (clazz == null) {
            return false;
        }
        try {
            finalDb.deleteAll(clazz);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 修改数据库对象
     *
     * @param object 修改后的数据，主键不可被修改
     * @return 修改结果 true：修改成功 false：修改失败
     */
    public boolean update(Object object) {
        if (object == null) {
            return false;
        }
        try {
            finalDb.update(object);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    /**
     * 按照条件查询数据库中的数据集合
     *
     * @param clazz 要查询的表
     * @return 查询出来的列表
     */
    public <T> List<T> findAll(Class<T> clazz) {
        if (clazz == null) {
            return null;
        }
        try {
            return finalDb.findAll(clazz);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 按照条件查询数据库中的数据集合
     *
     * @param clazz    要查询的表
     * @param whereStr 查询条件，例：id < 100
     * @return 查询出来的列表
     */
    public <T> List<T> findAllByWhere(Class<T> clazz, String whereStr) {
        if (clazz == null) {
            return null;
        }
        try {
            return finalDb.findAllByWhere(clazz, whereStr);
        } catch (Exception e) {
            return null;
        }
    }

}
