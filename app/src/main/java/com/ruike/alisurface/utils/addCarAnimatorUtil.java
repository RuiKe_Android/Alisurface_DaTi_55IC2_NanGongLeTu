package com.ruike.alisurface.utils;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ruike.alisurface.MyApplication;
import com.voodoo.lib_utils.L;

/**
 * 商品添加到购物车的动画工具类
 */
public class addCarAnimatorUtil {


    ValueAnimator valueAnimator;
    AnimatorSet animatorSetPeople;
    public void addGoodToCarAnimator(ImageView imageView, FrameLayout mRootRl, ImageView mCarImageView) {

        float[] mCurrentPosition = new float[2];
        final ImageView view = new ImageView(MyApplication.getAppContext());
        view.setImageDrawable(imageView.getDrawable());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(100, 100);
        mRootRl.addView(view, layoutParams);
        //二、计算动画开始/结束点的坐标的准备工作
        //得到父布局的起始点坐标（用于辅助计算动画开始/结束时的点的坐标）
        int[] parentLoc = new int[2];
        mRootRl.getLocationInWindow(parentLoc);

        //得到商品图片的坐标（用于计算动画开始的坐标）
        int[] startLoc = new int[2];
        imageView.getLocationInWindow(startLoc);

        //得到购物车图片的坐标(用于计算动画结束后的坐标)
        int[] endLoc = new int[2];
        mCarImageView.getLocationInWindow(endLoc);

        float startX = startLoc[0] - parentLoc[0] + imageView.getWidth() / 2;
        float startY = startLoc[1] - parentLoc[1] + imageView.getHeight() / 2;

        //商品掉落后的终点坐标：购物车起始点-父布局起始点+购物车图片的1/5
        float toX = endLoc[0] - parentLoc[0] + mCarImageView.getWidth() / 5;
        float toY = endLoc[1] - parentLoc[1];

        //开始绘制贝塞尔曲线
        Path path = new Path();
        path.moveTo(startX, startY);
        //使用二次萨贝尔曲线：注意第一个起始坐标越大，贝塞尔曲线的横向距离就会越大，一般按照下面的式子取即可
        path.quadTo((startX + toX) / 2, startY, toX, toY);
        PathMeasure mPathMeasure = new PathMeasure(path, false);

        //属性动画
        valueAnimator = ValueAnimator.ofFloat(0, mPathMeasure.getLength());
        valueAnimator.setDuration(500);
        valueAnimator.setInterpolator(new LinearInterpolator());

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (float) animation.getAnimatedValue();
                mPathMeasure.getPosTan(value, mCurrentPosition, null);
                view.setTranslationX(mCurrentPosition[0]);
                view.setTranslationY(mCurrentPosition[1]);
            }
        });

        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {

                CarAnimationCanal();
                // 把移动的图片imageview从父布局里移除
                mRootRl.removeView(view);
//                //shopImg 开始一个放大动画
                animatorSetPeople     = new AnimatorSet();  //多个动画 动画集
                animatorSetPeople.setDuration(500);
                animatorSetPeople.setInterpolator(new AccelerateDecelerateInterpolator());
                ObjectAnimator scaleX = ObjectAnimator.ofFloat(mCarImageView, "scaleX", 1, 0.7f, 1.1f, 0.9f, 1); // 从原始状态放大2倍再回到原始状态
                ObjectAnimator scaleY = ObjectAnimator.ofFloat(mCarImageView, "scaleY", 1, 0.7f, 1.1f, 0.9f, 1);
//        animatorSetPeople.play(translationX).before(scaleX).before(scaleY);
                animatorSetPeople.playTogether(scaleX, scaleY);
                animatorSetPeople.start();

                if (onItemonAnimationEnd != null) {
                    onItemonAnimationEnd.onAnimationEnd();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        valueAnimator.start();

    }

    public void CarAnimationCanal() {
        if (valueAnimator != null) {
            L.i("animator_Car====onAnimationEnd" );
            valueAnimator.cancel();
            valueAnimator=null;
        }
    }

    public OnItemonAnimationEnd onItemonAnimationEnd;


    public void setOnItemonAnimationEnd(OnItemonAnimationEnd onItemonAnimationEnd) {
        this.onItemonAnimationEnd = onItemonAnimationEnd;
    }

    /**
     * 商品列表项点击监听
     */
    public interface OnItemonAnimationEnd {
        void onAnimationEnd();

    }

}
