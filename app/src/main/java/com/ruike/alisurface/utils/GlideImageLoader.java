package com.ruike.alisurface.utils;

import android.content.Context;
import android.widget.ImageView;


import com.voodoo.lib_utils.imageLoader.ImageLoader;

/**
 * Created by admin on 2019/3/28.
 */

public class GlideImageLoader extends ImageLoader {

    public void displayImage(Context context, Object path, ImageView imageView) {
        ImageLoader.loadImage(context, path == null || path.toString().isEmpty() ? "" : path.toString(), imageView);
    }
}
