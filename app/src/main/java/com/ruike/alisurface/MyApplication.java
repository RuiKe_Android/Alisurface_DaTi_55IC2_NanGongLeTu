package com.ruike.alisurface;

import android.content.Context;
import android.os.Build;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.multidex.MultiDex;

import com.github.moduth.blockcanary.BlockCanary;
import com.ruike.alisurface.Serials.ICCardUtils;
import com.ruike.alisurface.Serials.SerialPortInstructUtils;
import com.ruike.alisurface.Serials.outGoods.ShengJiangJiUtils;
import com.ruike.alisurface.Serials.Ttys3Utils;
import com.ruike.alisurface.Serials.Ttys1Utils;
import com.ruike.alisurface.Serials.Ttys2Utils;
import com.ruike.alisurface.utils.CrashHandler;
import com.ruike.alisurface.utils.FinalDbUtils;
import com.ruike.alisurface.utils.Languages.LanguageUtil;
import com.ruike.alisurface.utils.ShareKey;
import com.ruike.lib_serialport.utils.SerialPortTtys1CallBack;
import com.ruike.lib_serialport.utils.SerialPortTtys2CallBack;
import com.ruike.lib_serialport.utils.SerialPortTtys3CallBack;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.voodoo.lib_frame.FrameApplication;
import com.voodoo.lib_utils.L;
import com.voodoo.lib_utils.ShareUtils;

import static com.ruike.alisurface.Constant.isOpenCardOperation;

/**
 * Author: voodoo
 * CreateDate: 2020-03-25 025 下午 02:55
 * Description:
 */
public class MyApplication extends FrameApplication {

    private static Context context;
    public static FinalDbUtils finalDbUtils;
    public static boolean card_init = false; // 读卡器是否初始化成功，每次进来都需要初始化一下
    private static RefWatcher sRefWatcher;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate() {
        super.onCreate();
        CrashHandler crashHandler = CrashHandler.getInstance();
        crashHandler.init(getApplicationContext());
        context = FrameApplication.getFrameContext();
        finalDbUtils = FinalDbUtils.getInstance(this);
        ShareUtils.init(this);
        /**
         * 对于7.0以下，需要在Application创建的时候进行语言切换
         */
        String language = ShareUtils.getInstance().getString(ShareKey.LANGUAGE, "");
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            LanguageUtil.changeAppLanguage(this, language);
        }
        if (isOpenCardOperation){
            openSerialPort1(); // 打开串口1
        }
//        openSerialPort2(); // 打开串口2
        openSerialPort3(); // 打开串口3

        // 在主进程初始化调用哈
        BlockCanary.install(this, new AppBlockCanaryContext()).start();
        sRefWatcher = LeakCanary.install(this);

    }

    public static RefWatcher getRefWatcher() {
        return sRefWatcher;
    }


    /**
     * 打开串口1
     */
    private void openSerialPort1() {
        SerialPortTtys1CallBack.setCallBack(Ttys1Utils.getInstance());
        Ttys1Utils.getInstance().OpenPort();
        SystemClock.sleep(500);
        ICCardUtils.initListener(); // 初始化读卡器的串口回调监听
        ICCardUtils.readWriteMode(false); // 设置被动读写模式
//        if (!card_init) {
//            ICCardUtils.initCard(); // 初始化读卡器
//        }
    }

    /**
     * 打开串口2
     */
    private void openSerialPort2() {
        SerialPortTtys2CallBack.setCallBack(Ttys2Utils.getInstance());
        Ttys2Utils.getInstance().OpenPort();
    }

    /**
     * 打开串口3
     */
    private void openSerialPort3() {
        L.i("打开串口");
        SerialPortTtys3CallBack.setCallBack(Ttys3Utils.getInstance());
        Ttys3Utils.getInstance().OpenPort();
        if (Constant.machineType == 2) { // 机器类型为升降机的时候进行初始化等操作
            SystemClock.sleep(500);
            ShengJiangJiUtils.initListener(); // 初始化升降机串口的回调监听
            // 开机通知底盘
            Ttys3Utils.getInstance().SendPort(SerialPortInstructUtils.startCommunication());
            SystemClock.sleep(500);
            // 设置电流保护阈值
            Ttys3Utils.getInstance().SendPort(SerialPortInstructUtils.sjjSetProtectCurrent(50, 50, 50, 50, 50, 50));
        }
    }

    /**
     * 获取全局Context
     */
    public static Context getAppContext() {
        return context;
    }

}
