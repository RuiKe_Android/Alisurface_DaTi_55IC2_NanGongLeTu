package com.ruike.alisurface.Serials;

import com.ruike.lib_serialport.callback.SerialCallBack;
import com.ruike.lib_serialport.utils.SerialPortTtys2Util;
import com.voodoo.lib_utils.L;

/**
 * 串口2 的底层通信类
 */
public class Ttys2Utils implements SerialCallBack {

    private static Ttys2Utils ttys2Utils;

    public static Ttys2Utils getInstance() {
        if (ttys2Utils == null) {
            synchronized (Ttys2Utils.class) {
                if (ttys2Utils == null) {
                    ttys2Utils = new Ttys2Utils();
                }
            }
        }
        return ttys2Utils;
    }

    public void OpenPort() {
        SerialPortTtys2Util.open();
    }

    /**
     * 发送十六进制的字符串
     */
    public void SendPort(String strmsg) {
        L.i("发送数据：" + strmsg);
        SerialPortTtys2Util.sendHex(strmsg);
    }

    @Override
    public void onSerialPortOpenListener(boolean isOpenSuccess, String portName, String msg) {

    }

    @Override
    public void onSerialPortData(String serialPortData, byte[] resultBytes) {

    }
}
