package com.ruike.alisurface.Serials;

import android.widget.Toast;

import com.ruike.alisurface.MyApplication;
import com.ruike.alisurface.R;
import com.voodoo.lib_frame.tip.ToastTip;
import com.voodoo.lib_utils.ByteUtils;
import com.voodoo.lib_utils.L;
import com.voodoo.lib_utils.ShareUtils;

import static com.ruike.alisurface.utils.ShareKey.IC2_SLOT_NUM;

/**
 * Author: voodoo
 * CreateDate: 2020/05/08 008 11:59 上午
 * Description: 串口指令拼接工具类
 * <p>
 * 开启通讯	startCommunication()
 * 直流电机操作指令	sjjControlMotor(电机通道号, 开关(是否转动), 速度, 步数, 是否正转)
 * 设置电流保护阈值	sjjSetProtectCurrent(通道1, 通道2, 通道3, 通道4, 通道5, 通道6)
 * 驱动板LED灯带操作	quDongLed(电路板地址, 灯光模式, 灯光通道, 是否开灯, 呼吸灯最大亮度, 呼吸灯呼吸时间)
 * 转动驱动电机出货	quDongOutGoods(货道号, 货道类型)
 * 打开电控门锁	quDongOpenElectricDoor(货柜编号, 电磁锁通电时间, 电磁锁编号)
 * 查询驱动程序版本号和驱动板批次	quDongCheckVersion(主副柜编号)
 * 查询温控程序版本号和驱动板批次	quDonfCheckWKVersion(主副柜编号)
 */
public class SerialPortInstructUtils {

    /**
     * 通知底层开始通讯
     * <p>
     * 发送指令之前首先发送此命令 01 02 2C 2F
     */
    public static String startCommunication() {
        byte[] strSerialComSend = new byte[4];
        strSerialComSend[0] = 0x01;
        strSerialComSend[1] = 0x02;
        strSerialComSend[2] = 0x2C;
        strSerialComSend[3] = 0x2F;
        L.i("拼接字符串：" + ByteUtils.byteArrToHex(strSerialComSend));
        return ByteUtils.byteArrToHex(strSerialComSend);
    }

    // ============================================================================================
    // ==                  👇👇👇👇             升降机RS485通讯          👇👇👇👇
    // ==            https://www.showdoc.cc/344763240666988?page_id=1983452567917701
    // ============================================================================================

    /**
     * 查询限位开关
     */
    public static String selectSwitch() {
        byte[] strSerialComSend = new byte[4];
        strSerialComSend[0] = 0x01;
        strSerialComSend[1] = 0x02;
        strSerialComSend[2] = 0x3D;
        strSerialComSend[3] = 0x40;
        return ByteUtils.byteArrToHex(strSerialComSend);
    }

    /**
     * 通知升降机电路板开始买货
     * 必须在货道出货前发此条指令
     */
    public static String startShoping() {
        byte[] strSerialComSend = new byte[4];
        strSerialComSend[0] = 0x01;
        strSerialComSend[1] = 0x02;
        strSerialComSend[2] = 0x2E;
        strSerialComSend[3] = 0x31;
        return ByteUtils.byteArrToHex(strSerialComSend);
    }

    /**
     * 操作直流电机
     * 数据格式：升降机电路板地址+数据长度+操作命令数据+电机通道号+开关状态+运行速度（0-90）
     * +运行步数高八位+运行步数低八位+转动方向+前面数据和低八位。
     * <p>
     * 例：01 08 02 00 01 28 01 E0 00 15    速度：40，正转，转10圈
     * <p>
     * 数据方向：安卓到驱动板
     * （1）升降机电路板地址 485通讯地址
     * （2）数据长度 除第一位与此数据长度位之外的数据长度
     * （3）控制电机转动指令标识
     * （4）电机通道号：0为通道1,1为通道2,2为通道3,3为通道4,4为通道5,5为通道6
     * （5）开关状态：0-关，1-开
     * （6）运行速度：0-90
     * （7）运行步数高八位
     * （8）运行速度低八位
     * （9）电机运转方向：0-正转，1-反转
     * （A）除该位外剩余数据和低八位
     *
     * @param motor     电机通道号
     * @param isTurn    开关(是否转动)
     * @param speed     速度
     * @param stepNum   步数
     * @param isForward 是否正转
     */
    public static String sjjControlMotor(int motor, boolean isTurn, int speed, int stepNum, boolean isForward) {
        byte[] strSerialComSend = new byte[10];
        byte strSerialChkSum_Int16 = 0;

        // 485通讯地址
        strSerialComSend[0] = 0x01;

        // 数据长度
        strSerialComSend[1] = 0x08;

        // 控制电机转动指令标识
        strSerialComSend[2] = 0x02;

        // 电机通道号 0为通道1,1为通道2,2为通道3,3为通道4,4为通道5,5为通道6
        switch (motor) {
            case 1:
                strSerialComSend[3] = 0x00;
                break;
            case 2:
                strSerialComSend[3] = 0x01;
                break;
            case 3:
                strSerialComSend[3] = 0x02;
                break;
            case 4:
                strSerialComSend[3] = 0x03;
                break;
            case 5:
                strSerialComSend[3] = 0x04;
                break;
            case 6:
                strSerialComSend[3] = 0x05;
                break;
        }

        // 开关状态 0-关，1-开
        strSerialComSend[4] = (byte) (isTurn ? 1 : 0);

        // 转动速度
        strSerialComSend[5] = (byte) speed;

        // 步数的高八位和步数的低八位
        strSerialComSend[6] = (byte) (stepNum >> 8);
        strSerialComSend[7] = (byte) (stepNum >> 0);

        // 转动方向 0-正转，1-反转
        strSerialComSend[8] = (byte) (isForward ? 0 : 1);

        for (byte tempByte : strSerialComSend) {
            strSerialChkSum_Int16 += (tempByte & 0xff);
        }
        strSerialComSend[9] = strSerialChkSum_Int16;

        return ByteUtils.byteArrToHex(strSerialComSend, strSerialComSend.length);
    }

    /**
     * 设置电机保护阈值
     *
     * @param channel1 通道1电流
     * @param channel2 通道2电流
     * @param channel3 通道3电流
     * @param channel4 通道4电流
     * @param channel5 通道5电流
     * @param channel6 通道6电流
     */
    public static String sjjSetProtectCurrent(int channel1, int channel2, int channel3, int channel4, int channel5, int channel6) {
        byte[] strSerialComSend = new byte[16];
        byte strSerialChkSum_Int16 = 0;

        channel1 = dianjizh(channel1);
        channel2 = dianjizh(channel2);
        channel3 = dianjizh(channel3);
        channel4 = dianjizh(channel4);
        channel5 = dianjizh(channel5);
        channel6 = dianjizh(channel6);

        // 485通讯地址
        strSerialComSend[0] = 0x01;

        // 数据长度
        strSerialComSend[1] = 0x0E;

        // 指令标识
        strSerialComSend[2] = 0x1D;

        // 第1通道电机电流值高八位
        strSerialComSend[3] = (byte) (channel1 >> 8 & 0xFF);

        // 第1通道电机电流值低八位
        strSerialComSend[4] = (byte) (channel1 & 0xFF);

        // 第2通道电机电流值高八位
        strSerialComSend[5] = (byte) (channel2 >> 8 & 0xFF);

        // 第2通道电机电流值低八位
        strSerialComSend[6] = (byte) (channel2 & 0xFF);

        // 第3通道电机电流值高八位
        strSerialComSend[7] = (byte) (channel3 >> 8 & 0xFF);

        // 第3通道电机电流值低八位
        strSerialComSend[8] = (byte) (channel3 & 0xFF);

        // 第4通道电机电流值高八位
        strSerialComSend[9] = (byte) (channel4 >> 8 & 0xFF);

        // 第4通道电机电流值低八位
        strSerialComSend[10] = (byte) (channel4 & 0xFF);

        // 第5通道电机电流值高八位
        strSerialComSend[11] = (byte) (channel5 >> 8 & 0xFF);

        // 第5通道电机电流值低八位
        strSerialComSend[12] = (byte) (channel5 & 0xFF);

        // 第6通道电机电流值高八位
        strSerialComSend[13] = (byte) (channel6 >> 8 & 0xFF);

        // 第6通道电机电流值低八位
        strSerialComSend[14] = (byte) (channel6 & 0xFF);

        for (byte tempByte : strSerialComSend) {
            strSerialChkSum_Int16 += (tempByte & 0xff);
        }
        strSerialComSend[15] = strSerialChkSum_Int16;

        return ByteUtils.byteArrToHex(strSerialComSend, strSerialComSend.length);
    }

    private static int dianjizh(float dj) {
        int num = (int) ((0.25 * dj * 10 / 3.3) * 4095);
        return num;
    }

    // ============================================================================================
    // ==                  👇👇👇👇             驱动板RS485通讯          👇👇👇👇
    // ==            https://www.showdoc.cc/293549481506172?page_id=1677427733030350
    // ============================================================================================

    /**
     * 驱动板普通LED灯带
     * <p>
     * 数据格式：升降机电路板地址+数据长度+操作命令数据+LED灯工作模式+LED通道号+
     * 对应通道号LED开关状态（只在开关模式下有效）+LED灯最大亮度（只在呼吸模式下有效，0-30）
     * +呼吸灯呼吸时间（只在呼吸模式下有效，0-30）+LED使能状态+前面数据和低八位。
     * <p>
     * 注：
     * （1）升降机电路板地址为485通讯地址
     * （2）数据长度为除第一位与此数据长度位之外的数据长度
     * （3）操作命令数据表示该指令的控制对象。
     * （4）LED工作模式：0-开关，1-呼吸
     * （5）LED通道号：0为LED1，1为LED2
     * （6）对应通道号LED开关状态（只在开关模式下有效）：0-关，1-开
     * （7）呼吸灯最大亮度（只在呼吸模式下有效）：取值1-30
     * （8）呼吸灯呼吸时间（只在呼吸模式下有效）：取值1-30
     * （9）LED使能状态：0-失能，1-使能
     * （10）校验位：前面数据和低八位
     * <p>
     *
     * @param pcbAddress 电路板地址 10 11 12
     * @param ledMode    灯光模式 0：开关模式  1：呼吸模式
     * @param chanl      灯光通道 0 1 2
     * @param isOpen     对应通道号LED开关状态（只在开关模式下有效）：0-关，1-开
     * @param largLight  呼吸灯最大亮度（只在呼吸模式下有效）：取值1-30
     * @param largtime   呼吸灯呼吸时间（只在呼吸模式下有效）：取值1-30
     */
    public static String quDongLed(int pcbAddress, int ledMode, int chanl, boolean isOpen, int largLight, int largtime) {
        byte[] strSerialComSend = new byte[10];
        byte strSerialChkSum_Int16 = 0;

        // 电路板地址
        switch (pcbAddress) {
            case 10:
                strSerialComSend[0] = 0x10;
                break;
            case 11:
                strSerialComSend[0] = 0x11;
                break;
            case 12:
                strSerialComSend[0] = 0x12;
                break;
        }

        // 数据长度
        strSerialComSend[1] = 0x08;

        // 开启LED灯指令标识
        strSerialComSend[2] = 0x05;

        // 工作模式
        strSerialComSend[3] = (byte) ledMode;

        // 灯光通道 0x00 0x01 0x02
        strSerialComSend[4] = (byte) chanl;

        // 对应通道号LED开关状态（只在开关模式下有效） 0-关，1-开
        strSerialComSend[5] = (byte) (isOpen ? 1 : 0);

        // 呼吸最大亮度（只在呼吸模式下有效）取值1-30
        strSerialComSend[6] = (byte) 0x1E;

        // 呼吸灯呼吸时间（只在呼吸模式下有效）取值1-30
        strSerialComSend[7] = (byte) largtime;

        // LED使能状态 0-失能，1-使能
        strSerialComSend[8] = 0x01;

        // 低八位校验和
        for (byte tempByte : strSerialComSend) {
            strSerialChkSum_Int16 += (tempByte & 0xff);
        }
        strSerialComSend[9] = strSerialChkSum_Int16;

        return ByteUtils.byteArrToHex(strSerialComSend, strSerialComSend.length);
    }

    /**
     * 驱动板出货
     * <p>
     * 数据协议格式：驱动板地址 + 数据长度 + 命令数据 + 总出货货道数量 +
     * 1货道号 + 1出货数量 + 1货道类型 +
     * 2货道号 + 2货道出货数量 + 2货道类型 +
     * 3货道号 + 3货道出货数量 + 3货道类型 +
     * 4货道号 + 4货道出货数量 + 4货道类型 +
     * 5货道号 + 5货道出货数量 + 5货道类型 +
     * 前面数据和低八位。
     * <p>
     * 示例数据：10 06 00 01 2E 01 00 46
     * 示例数据：10 09 00 02 01 01 00 02 02 00 21 暂时不支持多货道出货方法需修改本地方法
     * <p>
     * 注：
     * （1）驱动板地址为485通讯地址，主柜为0x10，副柜1位0x11，副柜2为0x12，以此类推。
     * （2）数据长度为第一位与此数据长度位之外的数据长度。
     * （3）出货指令标识
     * （4）总出货数量 为一次性选择的出货货道总数，最大值为5，也就是说一次性最多可以买五种商品。1表示一次出一种货，此时数据格式：
     * 驱动板地址+数据长度+命令数据+总出货货道数量+1货道号+1货道类型+前面数据和低八位。
     * （5）货道号
     * （6）当前货道号出货数量
     * （7）货道类型：0-弹簧货道，1-电磁锁货道，2-履带货道
     * 5、6、7 循环，多货道出货，最大为5
     *
     * @param index     货道号
     * @param indexType 货道类型 0-弹簧货道，1-电磁锁货道，2-履带
     */
    public static String quDongOutGoods(int index, int indexType) {
        int res = 0;
        byte[] strSerialComSend = new byte[8];
        byte strSerialChkSum_Int16 = 0;
        // 驱动板地址，如果第一块就是0x10，第二块就是0x11，依次类推
        int Hdh_num = ShareUtils.getInstance().getInt(IC2_SLOT_NUM, 60);
        L.i("Hdh_num  货道柜数===" + Hdh_num);
        if (index < Hdh_num) {
            res = index;
            strSerialComSend[0] = 0x10;
        } else if (index >= Hdh_num && index < Hdh_num * 2) {
            strSerialComSend[0] = 0x11;
            res = index % Hdh_num;
        } else if (index >= Hdh_num * 2 && index < Hdh_num * 3) {
            strSerialComSend[0] = 0x12;
            res = index % Hdh_num;
        } else if (index >= Hdh_num * 3 && index < Hdh_num * 4) {
            strSerialComSend[0] = 0x13;
            res = index % Hdh_num;
        } else if (index >= Hdh_num * 4 && index < Hdh_num * 5) {
            strSerialComSend[0] = 0x14;
            res = index % Hdh_num;
        } else if (index >= Hdh_num * 5 && index < Hdh_num * 6) {
            strSerialComSend[0] = 0x15;
            res = index % Hdh_num;
        } else if (index >= Hdh_num * 6 && index < Hdh_num * 7) {
            strSerialComSend[0] = 0x16;
            res = index % Hdh_num;
        } else if (index >= Hdh_num * 7 && index < Hdh_num * 8) {
            strSerialComSend[0] = 0x17;
            res = index % Hdh_num;
        } else if (index >= Hdh_num * 8 && index < Hdh_num * 9) {
            strSerialComSend[0] = 0x18;
            res = index % Hdh_num;
        } else if (index >= Hdh_num * 9 && index < Hdh_num * 10) {
            strSerialComSend[0] = 0x19;
            res = index % Hdh_num;
        } else if (index >= Hdh_num * 10) {
            L.e("连接的板子已经超出了范围...");
            String errorStr = MyApplication.getAppContext().getString(R.string.circuitBoardQuantityExceedsStr);
            ToastTip.makeText(MyApplication.getAppContext(), errorStr, Toast.LENGTH_LONG);
            return "";
        }

        strSerialComSend[1] = 0x06;

        strSerialComSend[2] = 0x00;

        // 商品种类
        strSerialComSend[3] = 0x01;

        // 货道编号
        strSerialComSend[4] = (byte) res;

        // 货道数量
        strSerialComSend[5] = 0x01;

        // 货道类型 0-弹簧货道，1-电磁锁货道，2-蛇形货道
        strSerialComSend[6] = (byte) indexType;
        L.i("货道类型 === " + (indexType == 0 ? "弹簧货道" : indexType == 1 ? "电磁锁货道" : indexType == 2 ? "履带货道" : "未知货道类型"));

        // 低八位校验和
        for (byte tempByte : strSerialComSend) {
            strSerialChkSum_Int16 += (tempByte & 0xff);
        }
        // 校验位
        strSerialComSend[7] = (byte) (strSerialChkSum_Int16 >> 0);

        return ByteUtils.byteArrToHex(strSerialComSend, strSerialComSend.length);

    }

    /**
     * 驱动板开启电控锁
     *
     * @param boxno    货柜编号 0是主柜 现在只能是0到4，1是第一辅柜0x11，2是第二辅柜0x12 ...
     * @param ElecNum  电磁锁编号 0表示1通道，1表示2通道
     * @param ElecOnTi 电磁锁通电时间 取值3-50，对应0.3s-5s
     * @return
     */
    public static String quDongOpenElectricDoor(int boxno, int ElecOnTi, int ElecNum) {
        byte[] strSerialComSend = new byte[6];
        byte strSerialChkSum_Int16 = 0;

        // 485设备地址 主柜为0x10，副柜1位0x11，副柜2为0x12 ...
        switch (boxno) {
            case 0:
                strSerialComSend[0] = 0x10;
                break;
            case 1:
                strSerialComSend[0] = 0x11;
                break;
            case 2:
                strSerialComSend[0] = 0x12;
                break;
            case 3:
                strSerialComSend[0] = 0x13;
                break;
            case 4:
                strSerialComSend[0] = 0x14;
                break;
        }

        // 数据长度
        strSerialComSend[1] = 0x04;

        // 开启电控锁指令标识
        strSerialComSend[2] = 0x03;

        // 00表示通道1锁 01表示通道2锁
        strSerialComSend[3] = (byte) ElecNum;

        // 电控锁开启时间
        strSerialComSend[4] = (byte) ElecOnTi;

        // 低八位校验和
        for (byte tempByte : strSerialComSend) {
            strSerialChkSum_Int16 += (tempByte & 0xff);
        }
        strSerialComSend[5] = (byte) (strSerialChkSum_Int16 >> 0);

        return ByteUtils.byteArrToHex(strSerialComSend, strSerialComSend.length);
    }

    /**
     * 查询驱动程序版本号和驱动板批次
     *
     * @param zfg 主副柜
     */
    public static String quDongCheckVersion(int zfg) {

        byte[] strSerialComSend = new byte[5];
        byte strSerialChkSum_Int16 = 0;

        switch (zfg) {
            case 10:
                strSerialComSend[0] = 0x10;
                break;
            case 11:
                strSerialComSend[0] = 0x11;
                break;
            case 12:
                strSerialComSend[0] = 0x12;
                break;
            case 13:
                strSerialComSend[0] = 0x13;
                break;
        }

        strSerialComSend[1] = 0x03;

        strSerialComSend[2] = 0x1C;

        strSerialComSend[3] = (byte) (zfg - 10);

        for (byte tempByte : strSerialComSend) {
            strSerialChkSum_Int16 += (tempByte & 0xff);
        }
        strSerialComSend[4] = strSerialChkSum_Int16;

        return ByteUtils.byteArrToHex(strSerialComSend, strSerialComSend.length);
    }

    /**
     * 查询温控程序版本号和驱动板批次
     * 23 03 03 06 2F
     *
     * @param zfg
     */
    public static String quDonfCheckWKVersion(int zfg) {

        byte[] strSerialComSend = new byte[5];
        byte strSerialChkSum_Int16 = 0;

        strSerialComSend[0] = 0x23;

        strSerialComSend[1] = 0x03;

        strSerialComSend[2] = 0x03;

        strSerialComSend[3] = (byte) zfg;
        for (byte tempByte : strSerialComSend) {
            strSerialChkSum_Int16 += (tempByte & 0xff);
        }
        strSerialComSend[4] = strSerialChkSum_Int16;
        return ByteUtils.byteArrToHex(strSerialComSend, strSerialComSend.length);
    }

    /**
     * 查询升降机驱动板电机保护电流
     */
    public static String queryCurrent() {
        byte[] strSerialComSend = new byte[4];
        strSerialComSend[0] = 0x01;
        strSerialComSend[1] = 0x02;
        strSerialComSend[2] = 0x51;
        strSerialComSend[3] = 0x54;
        return ByteUtils.byteArrToHex(strSerialComSend);
    }


}
