package com.ruike.alisurface.Serials;

import android.os.Handler;
import android.os.Looper;

import com.ruike.alisurface.utils.SerialsVerify;
import com.voodoo.lib_utils.ByteUtils;
import com.voodoo.lib_utils.L;

import java.math.BigInteger;
import java.util.Timer;
import java.util.TimerTask;

public class ICCardUtils {

    private static Timer timer;
    private static TimerTask task;
    private static String cardNumber;
    private static boolean isXunKa = false;

    public static void initListener() {
        Ttys1Utils.getInstance().setOnSerialPortResultDataListener(new Ttys1Utils.OnSerialPortResultDataListener() {
            @Override
            public void onDataReceived(String resultDataStr, byte[] resultBytes) {
                Handler mainHandler = new Handler(Looper.getMainLooper());
                mainHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mySerialPortResultData(resultDataStr, resultBytes);
                    }
                });
            }
        });
    }

    private static void mySerialPortResultData(String serialPortData, byte[] resultBytes) {

        L.i("接收刷卡数据: ", serialPortData);

        if (resultBytes[0] == (byte) 0xAA && resultBytes[1] == (byte) 0xBB && resultBytes.length > 9) {

            // CMD 寻卡返回
            if (resultBytes[6] == 0x01 && resultBytes[7] == 0x02) {
                if (resultBytes[2] == 0x08) { // 有卡 AA BB 08 00 00 00 01 02 00 04 00 07 ----- 无卡 AA BB 06 00 00 00 01 02 14 17
                    isXunKa = false;
                    stopXunKa(); // 停止寻卡
                    fangChongTu(); // 查询卡号
                }
            }
            // CMD 获取卡号返回
            if (resultBytes[6] == 0x02 && resultBytes[7] == 0x02) {
                if (resultBytes.length < 14) {
                    errorFengMing();
                    L.e("获取卡号返回值长度异常。" + serialPortData);
                    return;
                }
                StringBuffer cardNumberStringBuffer = new StringBuffer();
                cardNumberStringBuffer.append(ByteUtils.byteToHex(resultBytes[12]))
                        .append(ByteUtils.byteToHex(resultBytes[11]))
                        .append(ByteUtils.byteToHex(resultBytes[10]))
                        .append(ByteUtils.byteToHex(resultBytes[9]));
                cardNumber = String.valueOf(new BigInteger(cardNumberStringBuffer.toString(), 16));
                L.i("获取的卡号为：" + cardNumber);
                if (onmICCardListener != null) {
                    onmICCardListener.onICCardNumberListener(resultBytes, cardNumber);
                }
                oneKeyReadCard(25);
            }
            // CMD 一键读卡
            if (resultBytes[6] == 0x08 && resultBytes[7] == 0x06) {
                if (resultBytes.length < 26) {
                    errorFengMing();
                    L.e("获取卡密码返回值长度异常，无法解析。" + serialPortData);
                    return;
                }
                L.i("获取卡密码返回，蜂鸣 1 秒");
                fengMing(1); // 蜂鸣1秒，提示已经获取到所有信息，无论密码有无都已经全部获取完毕，剩下的就是本地数据的解析
                String[] resultStrings = serialPortData.split(" ");
                StringBuffer pwdStringBuffer = new StringBuffer();
                pwdStringBuffer.append(resultStrings[9])
                        .append(resultStrings[10])
                        .append(resultStrings[11])
                        .append(resultStrings[12])
                        .append(resultStrings[13])
                        .append(resultStrings[14])
                        .append(resultStrings[15])
                        .append(resultStrings[16])
                        .append(resultStrings[17])
                        .append(resultStrings[18])
                        .append(resultStrings[19])
                        .append(resultStrings[20])
                        .append(resultStrings[21])
                        .append(resultStrings[22])
                        .append(resultStrings[23])
                        .append(resultStrings[24]);
                L.i("读到密码数据：" + pwdStringBuffer);
                if (onmICCardListener != null) {
                    onmICCardListener.onICCardPwdListener(resultBytes, pwdStringBuffer.toString());
                }
                if (onICCardGetNumberPwdListener != null) {
                    L.i("卡号：" + cardNumber + "\t密码：" + pwdStringBuffer.toString());
                    onICCardGetNumberPwdListener.onICCardNumberPwdGet(cardNumber, pwdStringBuffer.toString());
                }
            }
        } else {
            L.e("返回数据头信息异常：" + serialPortData);
            errorFengMing();
        }
    }


    public static OnICCardListener onmICCardListener;
    public static OnICCardGetNumberPwdListener onICCardGetNumberPwdListener;

    public static void setOnICCardListener(OnICCardListener onICCardListener) {
        onmICCardListener = onICCardListener;
    }

    public static void setOnICCardGetNumberPwdListener(OnICCardGetNumberPwdListener onICCardGetNumberPwdListener) {
        ICCardUtils.onICCardGetNumberPwdListener = onICCardGetNumberPwdListener;
    }

    // 初始化结果接口回调
    public interface OnICCardListener {
        /**
         * 刷卡卡号回调
         *
         * @param resultBytes 回传的byte数组
         * @param cardNumber  十进制的卡号数据
         */
        void onICCardNumberListener(byte[] resultBytes, String cardNumber);

        /**
         * 获取卡密码
         *
         * @param resultBytes
         * @param cardPwd
         */
        void onICCardPwdListener(byte[] resultBytes, String cardPwd);
    }

    // 初始化结果接口回调
    public interface OnICCardGetNumberPwdListener {
        /**
         * 刷卡卡号回调
         *
         * @param cardNumber 十进制的卡号数据
         * @param cardPwd    获取到的卡密码
         */
        void onICCardNumberPwdGet(String cardNumber, String cardPwd);
    }

    public static void errorFengMing() {
        fengMing(1);
        fengMing(3);
    }

    // ==================================== M系列读卡器通讯协议 ====================================

    /**
     * 寻卡
     * <p>
     * 发送：AA BB 06 00 00 00 01 02 52 51
     * 返回：AA BB 08 00 00 00 01 02 00 04 00 07 ----  04 00 为卡片型号
     * <p>
     */
    public static void xunKa() {
        isXunKa = true;
        byte[] strSerialComSend = new byte[10];
        strSerialComSend[0] = (byte) 0xAA;
        strSerialComSend[1] = (byte) 0xBB;
        strSerialComSend[2] = 0x06;
        strSerialComSend[3] = 0x00;
        strSerialComSend[4] = 0x00;
        strSerialComSend[5] = 0x00;
        strSerialComSend[6] = 0x01;
        strSerialComSend[7] = 0x02;
        strSerialComSend[8] = 0x52;
        strSerialComSend[9] = (byte) Integer.parseInt(SerialsVerify.getBCC(strSerialComSend, 4), 16);

        // 开启计时器，进行循环发送寻卡指令
        if (timer == null) {
            timer = new Timer();
        }
        if (task != null) {
            task.cancel();
        }
        task = new TimerTask() {
            @Override
            public void run() {
                if (isXunKa) {
                    L.i("发送寻卡指令：" + ByteUtils.byteArrToHex(strSerialComSend));
                    Ttys1Utils.getInstance().SendPort(ByteUtils.byteArrToHex(strSerialComSend));
                }
            }
        };
        timer.schedule(task, 0, 1000);
    }

    /**
     * 停止寻卡 实际上就是将计时器取消掉
     */
    public static void stopXunKa() {
        L.i("停止寻卡，取消计时器。");
        isXunKa = false;
        if (timer != null) {
            timer.cancel();
            timer.purge();
            timer = null;
        }
        if (task != null) {
            task.cancel();
            task = null;
        }
    }

    /**
     * 防冲突(获取卡号)
     * <p>
     * 发送：AA BB 06 00 00 00 02 02 04 04
     * 返回：AA BB 0A 00 00 00 02 02 00 'EE 23 0B 6D' AB ----- EE 23 0B 6D：为卡片卡号，在选卡命令中要选中此卡号
     * <p>
     */
    public static void fangChongTu() {
        byte[] strSerialComSend = new byte[10];
        strSerialComSend[0] = (byte) 0xAA;
        strSerialComSend[1] = (byte) 0xBB;
        strSerialComSend[2] = 0x06;
        strSerialComSend[3] = 0x00;
        strSerialComSend[4] = 0x00;
        strSerialComSend[5] = 0x00;
        strSerialComSend[6] = 0x02;
        strSerialComSend[7] = 0x02;
        strSerialComSend[8] = 0x04;
        strSerialComSend[9] = (byte) Integer.parseInt(SerialsVerify.getBCC(strSerialComSend, 4), 16);
        Ttys1Utils.getInstance().SendPort(ByteUtils.byteArrToHex(strSerialComSend));
    }

    /**
     * 选卡
     * <p>
     * 发送：AA BB 09 00 00 00 03 02 EE 23 0B 6D AA 00
     * 返回：AA BB 07 00 00 00 03 02 00 08 09
     * <p>
     */
    public static void xuanKa() {
        byte[] strSerialComSend = new byte[14];
        strSerialComSend[0] = (byte) 0xAA;
        strSerialComSend[1] = (byte) 0xBB;
        strSerialComSend[2] = 0x09;
        strSerialComSend[3] = 0x00;
        strSerialComSend[4] = 0x00;
        strSerialComSend[5] = 0x00;
        strSerialComSend[6] = 0x03;
        strSerialComSend[7] = 0x02;
        strSerialComSend[8] = (byte) 0xEE;
        strSerialComSend[9] = 0x23;
        strSerialComSend[10] = 0x0B;
        strSerialComSend[11] = 0x6D;
        strSerialComSend[12] = (byte) 0xAA;
        strSerialComSend[13] = (byte) Integer.parseInt(SerialsVerify.getBCC(strSerialComSend, 4), 16);
        Ttys1Utils.getInstance().SendPort(ByteUtils.byteArrToHex(strSerialComSend));
    }

    /**
     * 认证
     * <p>
     * 发送：AA BB 0D 00 00 00 07 02 60 00 FF FF FF FF FF FF 65
     * 返回：AA BB 06 00 00 00 07 02 00 05
     * <p>
     */
    public static void renZheng() {
        byte[] strSerialComSend = new byte[17];
        strSerialComSend[0] = (byte) 0xAA;
        strSerialComSend[1] = (byte) 0xBB;
        strSerialComSend[2] = 0x0D;
        strSerialComSend[3] = 0x00;
        strSerialComSend[4] = 0x00;
        strSerialComSend[5] = 0x00;
        strSerialComSend[6] = 0x07;
        strSerialComSend[7] = 0x02;
        strSerialComSend[8] = 0x60;
        strSerialComSend[9] = 0x00;
        strSerialComSend[10] = (byte) 0xFF;
        strSerialComSend[11] = (byte) 0xFF;
        strSerialComSend[12] = (byte) 0xFF;
        strSerialComSend[13] = (byte) 0xFF;
        strSerialComSend[14] = (byte) 0xFF;
        strSerialComSend[15] = (byte) 0xFF;
        strSerialComSend[16] = (byte) Integer.parseInt(SerialsVerify.getBCC(strSerialComSend, 4), 16);
        Ttys1Utils.getInstance().SendPort(ByteUtils.byteArrToHex(strSerialComSend));
    }

    /**
     * 读卡
     * <p>
     * 发送：AA BB 06 00 00 00 08 02 00 0A
     * 返回：AA BB 16 00 00 00 08 02 00 EE 23 0B 6D AB 88 04 00 46 49 66 17 59 10 26 08 9F
     * <p>
     *
     * @param blockumber 块号
     */
    public static void duKa(int blockumber) {
        byte[] strSerialComSend = new byte[10];
        strSerialComSend[0] = (byte) 0xAA;
        strSerialComSend[1] = (byte) 0xBB;
        strSerialComSend[2] = 0x06;
        strSerialComSend[3] = 0x00;
        strSerialComSend[4] = 0x00;
        strSerialComSend[5] = 0x00;
        strSerialComSend[6] = 0x08;
        strSerialComSend[7] = 0x02;
        strSerialComSend[8] = (byte) blockumber;
        strSerialComSend[9] = (byte) Integer.parseInt(SerialsVerify.getBCC(strSerialComSend, 4), 16);
        Ttys1Utils.getInstance().SendPort(ByteUtils.byteArrToHex(strSerialComSend));
    }

    /**
     * 写卡
     * <p>
     * 发送：AA BB 16 00 00 00 09 02 01 11 11 11 11 11 11 11 11 11 11 11 11 11 11 11 11 0A
     * 返回：AA BB 06 00 00 00 09 02 00 0B
     * <p>
     *
     * @param blockumber 块号
     * @param datas      写入的数据 长度不得小于16字节
     */
    public static void xieKa(int blockumber, byte... datas) {
        if (datas.length < 16) {
            return;
        }
        byte[] strSerialComSend = new byte[26];
        strSerialComSend[0] = (byte) 0xAA;
        strSerialComSend[1] = (byte) 0xBB;
        strSerialComSend[2] = 0x16;
        strSerialComSend[3] = 0x00;
        strSerialComSend[4] = 0x00;
        strSerialComSend[5] = 0x00;
        strSerialComSend[6] = 0x09;
        strSerialComSend[7] = 0x02;
        strSerialComSend[8] = (byte) blockumber;
        strSerialComSend[9] = datas[0];
        strSerialComSend[10] = datas[1];
        strSerialComSend[11] = datas[2];
        strSerialComSend[12] = datas[3];
        strSerialComSend[13] = datas[4];
        strSerialComSend[14] = datas[5];
        strSerialComSend[15] = datas[6];
        strSerialComSend[16] = datas[7];
        strSerialComSend[17] = datas[8];
        strSerialComSend[18] = datas[9];
        strSerialComSend[19] = datas[10];
        strSerialComSend[20] = datas[11];
        strSerialComSend[21] = datas[12];
        strSerialComSend[22] = datas[13];
        strSerialComSend[23] = datas[14];
        strSerialComSend[24] = datas[15];
        strSerialComSend[25] = (byte) Integer.parseInt(SerialsVerify.getBCC(strSerialComSend, 4), 16);
        Ttys1Utils.getInstance().SendPort(ByteUtils.byteArrToHex(strSerialComSend));
    }

    /**
     * 休眠
     * <p>
     * 发送：AA BB 05 00 00 00 04 02 06  （在IDE状态下,读或写完卡操作后使用）
     * 返回：AA BB 06 00 00 00 04 02 00 06
     * <p>
     */
    public static void xiuMn() {
        byte[] strSerialComSend = new byte[9];
        strSerialComSend[0] = (byte) 0xAA;
        strSerialComSend[1] = (byte) 0xBB;
        strSerialComSend[2] = 0x05;
        strSerialComSend[3] = 0x00;
        strSerialComSend[4] = 0x00;
        strSerialComSend[5] = 0x00;
        strSerialComSend[6] = 0x04;
        strSerialComSend[7] = 0x02;
        strSerialComSend[8] = (byte) Integer.parseInt(SerialsVerify.getBCC(strSerialComSend, 4), 16);
        Ttys1Utils.getInstance().SendPort(ByteUtils.byteArrToHex(strSerialComSend));
    }

    /**
     * 读硬件版本号
     * <p>
     * 发送：AA BB 05 00 00 00 04 01 05
     * 返回：AA BB 17 00 00 00 04 01 00 4D 32 2D 45 44 2D 54 59 2D 30 30 20 56 31 2E 33 00 01
     * <p>
     */
    public static void getVersion() {
        byte[] strSerialComSend = new byte[9];
        strSerialComSend[0] = (byte) 0xAA;
        strSerialComSend[1] = (byte) 0xBB;
        strSerialComSend[2] = 0x05;
        strSerialComSend[3] = 0x00;
        strSerialComSend[4] = 0x00;
        strSerialComSend[5] = 0x00;
        strSerialComSend[6] = 0x04;
        strSerialComSend[7] = 0x01;
        strSerialComSend[8] = (byte) Integer.parseInt(SerialsVerify.getBCC(strSerialComSend, 4), 16);
        Ttys1Utils.getInstance().SendPort(ByteUtils.byteArrToHex(strSerialComSend));
    }

    /**
     * 蜂鸣鸣响
     * <p>
     * 发送：AA BB 06 00 00 00 06 01 '01' 06 (单引号中01为蜂鸣时间，1----10范围)
     * 返回：AA BB 06 00 00 00 06 01 00 07
     * <p>
     */
    public static void fengMing(int time) {
        byte[] strSerialComSend = new byte[10];
        strSerialComSend[0] = (byte) 0xAA;
        strSerialComSend[1] = (byte) 0xBB;
        strSerialComSend[2] = 0x06;
        strSerialComSend[3] = 0x00;
        strSerialComSend[4] = 0x00;
        strSerialComSend[5] = 0x00;
        strSerialComSend[6] = 0x06;
        strSerialComSend[7] = 0x01;
        strSerialComSend[8] = (byte) (time < 1 ? 1 : time > 10 ? 10 : time);
        strSerialComSend[9] = (byte) Integer.parseInt(SerialsVerify.getBCC(strSerialComSend, 4), 16);
        Ttys1Utils.getInstance().SendPort(ByteUtils.byteArrToHex(strSerialComSend));
    }

    /**
     * 初始化值操作
     * <p>
     * 发送：AA BB 0A 00 00 00 0A 02 01 09 00 00 00 00
     * 返回：AA BB 06 00 00 00 0A 02 00 08
     * <p>
     */
    public static void init() {
        byte[] strSerialComSend = new byte[14];
        strSerialComSend[0] = (byte) 0xAA;
        strSerialComSend[1] = (byte) 0xBB;
        strSerialComSend[2] = 0x0A;
        strSerialComSend[3] = 0x00;
        strSerialComSend[4] = 0x00;
        strSerialComSend[5] = 0x00;
        strSerialComSend[6] = 0x0A;
        strSerialComSend[7] = 0x02;
        strSerialComSend[8] = 0x01;
        strSerialComSend[9] = 0x09;
        strSerialComSend[10] = 0x00;
        strSerialComSend[11] = 0x00;
        strSerialComSend[12] = 0x00;
        strSerialComSend[13] = (byte) Integer.parseInt(SerialsVerify.getBCC(strSerialComSend, 4), 16);
        Ttys1Utils.getInstance().SendPort(ByteUtils.byteArrToHex(strSerialComSend));
    }

    /**
     * 一键读卡操作
     * <p>
     * 发送：AA BB 0D 00 00 00 08 06 60 '01' 'FF FF FF FF FF FF' 6F ----- 01为块号；FF FF FF FF FF FF为密码
     * 返回：AA BB 16 00 00 00 08 06 00 11 22 33 44 55 66 77 88 99 00 AA 00 BB CC DD EE FF 0E ----- 备注：数据有AA的，后面会多发一个00
     * <p>
     *
     * @param blockNumber 块号
     */
    public static void oneKeyReadCard(int blockNumber) {
        byte[] strSerialComSend = new byte[17];
        strSerialComSend[0] = (byte) 0xAA;
        strSerialComSend[1] = (byte) 0xBB;
        strSerialComSend[2] = 0x0D;
        strSerialComSend[3] = 0x00;
        strSerialComSend[4] = 0x00;
        strSerialComSend[5] = 0x00;
        strSerialComSend[6] = 0x08;
        strSerialComSend[7] = 0x06;
        strSerialComSend[8] = 0x60;
        strSerialComSend[9] = (byte) blockNumber;
        strSerialComSend[10] = (byte) 0xFF;
        strSerialComSend[11] = (byte) 0xFF;
        strSerialComSend[12] = (byte) 0xFF;
        strSerialComSend[13] = (byte) 0xFF;
        strSerialComSend[14] = (byte) 0xFF;
        strSerialComSend[15] = (byte) 0xFF;
        strSerialComSend[16] = (byte) Integer.parseInt(SerialsVerify.getBCC(strSerialComSend, 4), 16);
        Ttys1Utils.getInstance().SendPort(ByteUtils.byteArrToHex(strSerialComSend));
    }

    /**
     * 一键写卡操作
     * <p>
     * 发送：AA BB 1D 00 00 00 09 06 60 '01' 'FF FF FF FF FF FF' '11 22 33 44 55 66 77 88 99 00 AA 00 BB CC DD EE FF' 6E ----- 01为块号；FF FF FF FF FF FF为密码； 16字节数据
     * 返回：AA BB 06 00 00 00 09 06 00 0F
     * <p>
     *
     * @param blockNumber 块号
     */
    public static void oneKeyWriteCard(int blockNumber, byte... datas) {
        if (datas.length < 16) {
            return;
        }
        byte[] strSerialComSend = new byte[17];
        strSerialComSend[0] = (byte) 0xAA;
        strSerialComSend[1] = (byte) 0xBB;
        strSerialComSend[2] = 0x0D;
        strSerialComSend[3] = 0x00;
        strSerialComSend[4] = 0x00;
        strSerialComSend[5] = 0x00;
        strSerialComSend[6] = 0x09;
        strSerialComSend[7] = 0x06;
        strSerialComSend[8] = 0x60;
        strSerialComSend[9] = (byte) blockNumber;
        strSerialComSend[10] = (byte) 0xFF;
        strSerialComSend[11] = (byte) 0xFF;
        strSerialComSend[12] = (byte) 0xFF;
        strSerialComSend[13] = (byte) 0xFF;
        strSerialComSend[14] = (byte) 0xFF;
        strSerialComSend[15] = (byte) 0xFF;
        strSerialComSend[15] = datas[0];
        strSerialComSend[15] = datas[1];
        strSerialComSend[15] = datas[2];
        strSerialComSend[15] = datas[3];
        strSerialComSend[15] = datas[4];
        strSerialComSend[15] = datas[5];
        strSerialComSend[15] = datas[6];
        strSerialComSend[15] = datas[7];
        strSerialComSend[15] = datas[8];
        strSerialComSend[15] = datas[9];
        strSerialComSend[15] = datas[10];
        strSerialComSend[15] = datas[11];
        strSerialComSend[15] = datas[12];
        strSerialComSend[15] = datas[13];
        strSerialComSend[15] = datas[14];
        strSerialComSend[15] = datas[15];
        strSerialComSend[16] = (byte) Integer.parseInt(SerialsVerify.getBCC(strSerialComSend, 4), 16);
        Ttys1Utils.getInstance().SendPort(ByteUtils.byteArrToHex(strSerialComSend));
    }

    /**
     * 读写模式
     * <p>
     * 发送：AA BB 06 00 00 00 01 06 '01' 06 ----- 01进入读写模块（被动读写卡，由指令控制）  01  04
     * 返回：AA BB 06 00 00 00 01 06 00 07
     * <p>
     */
    public static void readWriteMode(boolean isZhuDong) {
        byte[] strSerialComSend = new byte[10];
        strSerialComSend[0] = (byte) 0xAA;
        strSerialComSend[1] = (byte) 0xBB;
        strSerialComSend[2] = 0x06;
        strSerialComSend[3] = 0x00;
        strSerialComSend[4] = 0x00;
        strSerialComSend[5] = 0x00;
        strSerialComSend[6] = 0x01;
        strSerialComSend[7] = 0x06;
        strSerialComSend[8] = (byte) (isZhuDong ? 4 : 1);
        strSerialComSend[9] = (byte) Integer.parseInt(SerialsVerify.getBCC(strSerialComSend, 4), 16);
        Ttys1Utils.getInstance().SendPort(ByteUtils.byteArrToHex(strSerialComSend));
    }

    /**
     * 只读模式一
     * 发送：AA BB 06 00 00 00 01 06 02 05
     * 返回：AA BB 06 00 00 00 01 06 00 07
     * 备注：02进入只读模块, 输出格式10位十进制物理UID（正向卡号）；
     */
    public static void initCard() {
        byte[] strSerialComSend = new byte[10];
        strSerialComSend[0] = (byte) 0xAA;
        strSerialComSend[1] = (byte) 0xBB;
        strSerialComSend[2] = 0x06;
        strSerialComSend[3] = 0x00;
        strSerialComSend[4] = 0x00;
        strSerialComSend[5] = 0x00;
        strSerialComSend[6] = 0x01;
        strSerialComSend[7] = 0x06;
        strSerialComSend[8] = 0x02;
        strSerialComSend[9] = (byte) Integer.parseInt(SerialsVerify.getBCC(strSerialComSend, 4), 16);
        Ttys1Utils.getInstance().SendPort(ByteUtils.byteArrToHex(strSerialComSend));
    }

    /**
     * 继电器
     * 发送：AA BB 06 00 00 00 02 07 '01' 04 ----- 01，为开继电器；00，为开继电器
     * 返回：AA BB 06 00 00 00 02 07 00 05
     * 备注：02进入只读模块, 输出格式10位十进制物理UID（正向卡号）；
     */
    public static void initCard(boolean isOpen) {
        byte[] strSerialComSend = new byte[10];
        strSerialComSend[0] = (byte) 0xAA;
        strSerialComSend[1] = (byte) 0xBB;
        strSerialComSend[2] = 0x06;
        strSerialComSend[3] = 0x00;
        strSerialComSend[4] = 0x00;
        strSerialComSend[5] = 0x00;
        strSerialComSend[6] = 0x02;
        strSerialComSend[7] = 0x07;
        strSerialComSend[8] = (byte) (isOpen ? 1 : 0);
        strSerialComSend[9] = (byte) Integer.parseInt(SerialsVerify.getBCC(strSerialComSend, 4), 16);
        Ttys1Utils.getInstance().SendPort(ByteUtils.byteArrToHex(strSerialComSend));
    }


}
