package com.ruike.alisurface.Serials;

import com.ruike.lib_serialport.callback.SerialCallBack;
import com.ruike.lib_serialport.utils.SerialPortTtys1Util;
import com.voodoo.lib_utils.L;

/**
 * 串口1 的底层通信类
 */
public class Ttys1Utils implements SerialCallBack {

    private static Ttys1Utils ttys1Utils;

    public static Ttys1Utils getInstance() {
        if (ttys1Utils == null) {
            synchronized (Ttys1Utils.class) {
                if (ttys1Utils == null) {
                    ttys1Utils = new Ttys1Utils();
                }
            }
        }
        return ttys1Utils;

    }

    public void OpenPort() {
        SerialPortTtys1Util.open();
    }

    /**
     * 发送十六进制的字符串
     */
    public void SendPort(String strmsg) {
        SerialPortTtys1Util.sendHex(strmsg);
    }

    /**
     * 打开串口监听
     *
     * @param isOpenSuccess 是否打开成功
     * @param portName      串口地址名称
     * @param msg           回传的消息
     */
    @Override
    public void onSerialPortOpenListener(boolean isOpenSuccess, String portName, String msg) {
    }

    @Override
    public void onSerialPortData(String resultDataStr, byte[] resultBytes) {
        if (onSerialPortResultDataListener != null) {
            onSerialPortResultDataListener.onDataReceived(resultDataStr, resultBytes);
        }
    }

    public OnSerialPortResultDataListener onSerialPortResultDataListener;

    public void setOnSerialPortResultDataListener(OnSerialPortResultDataListener onSerialPortResultDataListener) {
        this.onSerialPortResultDataListener = onSerialPortResultDataListener;
    }

    interface OnSerialPortResultDataListener {
        void onDataReceived(String resultDataStr, byte[] resultBytes);
    }

}
