package com.ruike.alisurface.Serials;

import com.ruike.alisurface.mqtt.MqttOperation;
import com.voodoo.lib_utils.GsonUtils;
import com.voodoo.lib_utils.L;
import com.voodoo.lib_utils.ShareUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 转动出货信息返回值解析上传类
 */
public class ShopInfoUtils {

    static StringBuffer dls = new StringBuffer();
    static StringBuffer RT = new StringBuffer();
    static String Is_O;

    /**
     * Topic:rkt/mch/checkoutnotify/
     * 报文 {
     * "oid": "2018112114100000022524",
     * "Is_O": 2,
     * "dl": "001010|002010",
     * "RT": "1919",
     * "MV": "130a09130713",
     * "T": "130a0b130a0b",
     * "Fir": "130a09130713",
     * "Sec": "130a09130713",
     * "Thi": "130a09130713"
     * }
     *
     * @param oid
     * @param
     */


    public static void sendCheckoutnotify(String oid) {
        HashMap<String, String> map = BzVersion();
        map.put("oid", oid);
        map.put("Is_O", Is_O);
        if (dllist.size() > 1) {
            for (int i = 0; i < dllist.size(); i++) {
                slotBean sb = dllist.get(i);
                if (i > 0) {
                    if (dllist.size() - 1 == i) {
                        dls.append(sb.getSlot()).append(sb.getCount()).append(sb.getZdcount()).append(sb.getTypes());
                        break;
                    }
                }
                dls.append(sb.getSlot()).append(sb.getCount()).append(sb.getZdcount()).append(sb.getTypes()).append("|");
            }
        } else {
            slotBean sb = dllist.get(0);
            dls.append(sb.getSlot()).append(sb.getCount()).append(sb.getZdcount()).append(sb.getTypes());
        }
        map.put("dl", dls.toString());
        map.put("RT", RT.toString());
        map.put("model", "IC2");
        String data = GsonUtils.MapToJson(map);
        L.i("上传解析好的出货信息", data);
        MqttOperation.publishMessage("rkt/mch/checkoutnotify/", data);
        dls.delete(0, dls.length());
        RT.delete(0, RT.length());
        dllist.clear();
    }

    /**
     * 温控板子 版本信息
     *
     * @return
     */
    public static HashMap<String, String> BzVersion() {
        HashMap<String, String> map = new HashMap<>();
        map.put("MV", ShareUtils.getInstance().getString("MV", ""));
        map.put("T", ShareUtils.getInstance().getString("T", ""));
        map.put("Fir", ShareUtils.getInstance().getString("Fir", ""));
        map.put("Sec", ShareUtils.getInstance().getString("Sec", ""));
        map.put("Thi", ShareUtils.getInstance().getString("Thi", ""));
        return map;
    }

    /**
     * 10 0E 02
     * 04
     * 01
     * 00
     * 00
     * 01
     * 13 0B 1D 13 0C 05 1A 9F
     * 第4位	掉货检测是否在线，3-在线，4-不在线	十六进制
     * 01	第5位	出货货到数量	十六进制
     * 00	第6位	货道号	十六进制
     * 00	第7位	掉货检测检测数量	十六进制
     * 02	第8位	电机转动次数
     * 19	第15位	第一个电机转动时间
     */


    public static List<slotBean> dllist = new ArrayList<>();

    public static void analyzeSlot(String date, int ponstion, int Count, int slottype) {
        String[] data_code = date.split(" ");
        slotBean slotBean = new slotBean();
        Is_O = String.valueOf(Integer.parseInt(data_code[3], 16));
        StringBuffer dl = new StringBuffer();
        int chnum = Integer.parseInt(data_code[4], 16);
        int index = Integer.parseInt(data_code[5], 16) + 1;
        if (index < 10) {
            dl.append("00").append(index + "");
        } else if (10 <= index && index < 100) {
            dl.append("0").append(index + "");
        } else if (index >= 100) {
            dl.append(index + "");
        }
        int dhnum = Integer.parseInt(data_code[6], 16);
        int djzdcs = Integer.parseInt(data_code[7], 16);//电机转动次数
        String zdtime = data_code[14];
        RT.append(zdtime);
        if (dllist.size() > 0) {
            L.i("dllist-2--", dl, dllist.get(dllist.size() - 1).getSlot());
            if (dl.toString().equals(dllist.get(dllist.size() - 1).getSlot())) {
                L.i("dllist-3--");
                slotBean.setSlot(dl.toString());
                slotBean.setCount(dhnum);
                slotBean.setZdcount(dllist.get(dllist.size() - 1).getZdcount() + 1);
                slotBean.setTypes(0);
                slotBean.setTimes(zdtime);
                dllist.set(dllist.size() - 1, slotBean);
                return;
            }
        }
        slotBean.setSlot(dl.toString());
        slotBean.setCount(dhnum);
        slotBean.setZdcount(djzdcs);
        slotBean.setTypes(0);
        slotBean.setTimes(zdtime);
        dllist.add(slotBean);

    }

    public static class slotBean implements Serializable {

//        xxx x x x每一项出货请求包含6位 其中前三位表示货道编号
//        第四位表示货道检测数量 第五位表示电机转动数量 最后一位表示货道类型，
//        每条数据之间用竖线隔开
//        （0,弹簧货道 1,电磁锁 2,履带 如果为1时，不判断弹掉货检测无报修提示）


        String slot;
        int count;
        int zdcount;
        int types;
        String times;

        public String getTimes() {
            return times;
        }

        public void setTimes(String times) {
            this.times = times;
        }

        public String getSlot() {
            return slot;
        }

        public void setSlot(String slot) {
            this.slot = slot;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public int getZdcount() {
            return zdcount;
        }

        public void setZdcount(int zdcount) {
            this.zdcount = zdcount;
        }

        public int getTypes() {
            return types;
        }

        public void setTypes(int types) {
            this.types = types;
        }
    }
}
