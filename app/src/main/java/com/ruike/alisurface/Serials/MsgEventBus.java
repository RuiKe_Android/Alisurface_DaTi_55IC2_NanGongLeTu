package com.ruike.alisurface.Serials;

public class MsgEventBus {
    public   String type;
    public   String message;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MsgEventBus(String type, String message) {
        this.type = type;
        this.message = message;
    }

    @Override
    public String toString() {
        return "MsgEventBus{" +
                "type='" + type + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}