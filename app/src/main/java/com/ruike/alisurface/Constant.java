package com.ruike.alisurface;

import android.os.Environment;

import com.ruike.alisurface.utils.DeviceUtils;

/**
 * Author: voodoo
 * CreateDate: 2020-03-26 026 上午 10:46
 * Description: 项目常量类
 */
public class Constant {

    //==============================================================================================
    //    驱动板RS485通信协议：https://www.showdoc.cc/293549481506172?page_id=1677427733030350
    //    升降机RS485通信协议：https://www.showdoc.cc/344763240666988?page_id=1983452567917701
    //==============================================================================================

    public static boolean showShop = false;//是否出货中

    public static boolean isSzwk = false;//是否温控

    public static boolean isNoTtitle = false;//是否关闭title  logo
    public static final int machineType = 1; // 机器类型 1、IC2 2、升降机
    public static final String APP_TYPE = "20231024NG"; // 南宫答题

    public static final boolean isOpenCardOperation = false; // 是否开启刷卡的操作，就是界面上展不展示充卡刷卡支付按钮
    public static final String MAIN_PACKAGE_NAME_FILE_PATH = Environment.getExternalStorageDirectory() + "/com.ruike.system.folder/mainPackageShare.txt"; // 主程序包名的存储文件路径

    // 广告类型
    public static int ADV_TYPE_IMAGE = 0; // 图片广告
    public static int ADV_TYPE_VIDEO = 1; // 视频广告
    public static int ADV_TYPE_NONE = 2; // 无广告

    /**
     * 接口地址
     */
    public static class HttpUrl {
        public static final String url = "http://www.shouhuojiyun.com/Rknew"; // 正式接口
        public static final String url_cmd = "http://120.78.219.34:8018"; // 正式CMD指令回调接口
        public static final String QUESTION_URL = "https://rk.shouhuojiyun.com/otherApi/nangongdati/"; // 答题项目正式库

//        public static final String url = "http://c.shouhuojiyun.com"; // 测试接口
//        public static final String url_cmd = "http://47.106.175.214:8018"; // 测试CMD指令回调接口
//        public static final String QUESTION_URL = "http://robot.shouhuojiyun.com/Customer/question/"; // 答题项目测试库


        public static final String url_log = url_cmd + "/mchapi/addlog"; // 上传log接口

    }

    /**
     * Mqtt地址
     */
    public static class Mqtt {
        public static final String BASE_URL = "tcp://mqtt-cn-4590yhjpl06.mqtt.aliyuncs.com:1883";
        public static final String CLIENT_ID = "GID_MCH@@@" + DeviceUtils.getDeviceSn();
        //        public static final String INSTANCE_ID = "mqtt-cn-4590yhjpl06"; // 实例名
//        public static final String ACCESS_KEY = "LTAIXG4D0wcEhwUP"; // 阿里云AK
//        public static final String SECRET_KEY = "G95oPlxwsFbJvnDvMLLHoYtIstyjlg"; // 访问私钥
        public static final String userName = "LTAImQuW7MJwIh0D";
        public static final String passWord = "/v9GTxXfC+NnUHeJvjEe4P9cbaI=";
        // 订阅主题
        private static String MQTT_BASE_THEME = "rkt/mch/" + DeviceUtils.getDeviceSn();
        public static final String SUBSCR_ADVERTISEMENT = MQTT_BASE_THEME + "/refreshad/"; // 广告下发
        public static final String SUBSCR_LANE_UPDATA = MQTT_BASE_THEME + "/refreshslot/"; // 货道更新
        public static final String SUBSCR_OUT_GOODS = MQTT_BASE_THEME + "/outgoods/"; // 远程出货
        public static final String SUBSCR_CMD = MQTT_BASE_THEME + "/cmd/"; //远程获取日志
        public static final String SUBSCR_TEMPERATURE_CONTROL = MQTT_BASE_THEME + "/device/"; // 温度控制
        public static final String SUBSCR_OUTADV = MQTT_BASE_THEME + "/ad/"; // 售货机接收广告推送
        public static final String SUBSCR_LOCK = MQTT_BASE_THEME + "/lock/"; // 售货机电控锁  门
        public static final String BINDSUSCCESS = "rkt/mch/bindsuccess/";//售货机接收绑定重启软件
        public static final String SERVICE_TEL = "rkt/mch/service_tel/";//更新机主电话
        public static final String SUBSCR_GET_OPENID = "rkt/p2p/GID_MCH@@@" + DeviceUtils.getDeviceSn() + "/"; // 登录获取openId
        // Action
    }

    /**
     * 默认（初始化时候）的数据
     */
    public static class DefaultData {
        public static final int PURCHASE_COUNT = 1; // 默认可购买数量
    }

}
