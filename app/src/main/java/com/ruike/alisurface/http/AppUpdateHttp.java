package com.ruike.alisurface.http;

import com.ruike.alisurface.BuildConfig;
import com.ruike.alisurface.Constant;
import com.ruike.alisurface.utils.ShareKey;
import com.voodoo.lib_frame.base.BaseView;
import com.voodoo.lib_frame.httpTools.ApiTools;
import com.voodoo.lib_frame.httpTools.RequestParams;
import com.voodoo.lib_utils.ShareUtils;

/**
 * Author: voodoo
 * CreateDate: 2020-03-26 026 下午 01:59
 * Description:
 */
public class AppUpdateHttp {

    private static String url = Constant.HttpUrl.url_cmd + "/AppUpdate/";

    public static void tellServiceAppVersionInfo(BaseView apiListener) {
        ApiTools apiTools = new ApiTools();
        RequestParams params = new RequestParams();
        params.put("mchid", ShareUtils.getInstance().getString(ShareKey.MCH_ID));
        params.put("AppProject_Id", Constant.APP_TYPE);
        params.put("app_Version", BuildConfig.VERSION_NAME);
        params.put("packageName", BuildConfig.APPLICATION_ID);
        apiTools.postRequest(url + "TellService_AppVersion_Info", params, apiListener);
    }

}
