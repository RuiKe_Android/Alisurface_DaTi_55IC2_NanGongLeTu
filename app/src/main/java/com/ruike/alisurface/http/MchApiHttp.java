package com.ruike.alisurface.http;

import com.ruike.alisurface.Constant;
import com.ruike.alisurface.bean.BuHuoBean;
import com.ruike.alisurface.bean.CardCreateOrderByProductBean;
import com.ruike.alisurface.bean.CardCreateOrderBySlotBean;
import com.ruike.alisurface.utils.DeviceUtils;
import com.ruike.alisurface.utils.ShareKey;
import com.voodoo.lib_frame.base.BaseView;
import com.voodoo.lib_frame.httpTools.ApiTools;
import com.voodoo.lib_frame.httpTools.RequestParams;
import com.voodoo.lib_utils.GsonUtils;
import com.voodoo.lib_utils.L;
import com.voodoo.lib_utils.ShareUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: voodoo
 * CreateDate: 2020-03-26 026 下午 01:33
 * Description: MchApi请求
 */
public class MchApiHttp {

    private static String url = Constant.HttpUrl.url + "/MchApi/";
    private static String url_lower = Constant.HttpUrl.url + "/mchapi/";
    // 卡充值
    private static String createRechargeOrderUrl = Constant.HttpUrl.url + "/app/Order/Create_Recharge_Order";
    // 卡支付按照商品创建订单
    private static String cardCreateOrderByProductUrl = "http://120.78.219.34:9018/otherApi/card/Rk_Card_CreateOrder_ByProduct";
    // 卡支付按照货道创建订单
    private static String cardCreateOrderBySlotUrl = "http://120.78.219.34:9018/otherApi/card/Rk_Card_CreateOrder_BySlot";
    // 卡支付订单完结
    private static String rkCardEndOrderUrl = "http://120.78.219.34:9018/otherApi/card/Rk_Card_EndOrder";

    private static ApiTools apiTools = new ApiTools();
    //上传网络信号
    private static String submit_Signal_Intensity = "http://120.78.219.34:8018/MchApi/submit_signal_intensity";// mchid=100&value=-70

    /**
     * 上传网络信号
     *
     * @param apiListener 回调
     */
    public static void getSignal_Intensity(BaseView apiListener, int Signal) {
        RequestParams params = new RequestParams();
        params.put("mchid", ShareUtils.getInstance().getString(ShareKey.MCH_ID));
        params.put("value", Signal + "");
        apiTools.getRequest(submit_Signal_Intensity, params, apiListener);
    }

    /**
     * 获取程序版本
     */
    public static void getAppVersion(BaseView apiListener) {
        apiTools.getRequest(url + "getAppVersion", new RequestParams(), apiListener);
    }

    /**
     * 获取机器信息
     *
     * @param apiListener 回调
     */
    public static void getMchBySn(BaseView apiListener) {
        RequestParams params = new RequestParams();
        params.put("SN", DeviceUtils.getDeviceSn());
        apiTools.getRequest(url_lower + "geMchBySn", params, apiListener);
    }

    /**
     * 登录
     *
     * @param userNameStr 用户名
     * @param pwdStr      密码
     * @param apiListener 回调
     */
    public static void loginByMch(String userNameStr, String pwdStr, BaseView apiListener) {
        RequestParams params = new RequestParams();
        params.put("UserName", userNameStr);
        params.put("Pwd", pwdStr);
        params.put("sn", DeviceUtils.getDeviceSn());
        apiTools.getRequest(url + "LoginByMch", params, apiListener);
    }

    /**
     * 获取商品列表数据
     *
     * @param apiListener 回调
     */
    public static void getSlots(BaseView apiListener) {

        RequestParams params = new RequestParams();
        params.put("mchid", ShareUtils.getInstance().getString(ShareKey.MCH_ID));
        apiTools.getRequest(url_lower + "getSlots", params, apiListener);
    }

    /**
     * 按类型获取商品列表数据
     *
     * @param apiListener 回调
     */
    public static void getSlotsType(BaseView apiListener) {

        RequestParams params = new RequestParams();
        params.put("mchid", ShareUtils.getInstance().getString(ShareKey.MCH_ID));
        apiTools.getRequest(url_lower + "getProduct", params, apiListener);
    }

    /**
     * 获取商品详情数据
     *
     * @param apiListener 回调
     */
    public static void getGoodsinfo(BaseView apiListener, String productId) {

        RequestParams params = new RequestParams();
        params.put("productId", productId);
        apiTools.getRequest(url_lower + "Get_Productinfo", params, apiListener);
    }

    /**
     * 获取商品支付二维码
     *
     * @param apiListener 回调
     */
    public static void postGoodsQRCode(BaseView apiListener, HashMap<String, Object> map) {
        RequestParams params = new RequestParams();
        params.put(map);
        apiTools.postRequest(Constant.HttpUrl.url + "/app/order/create", params, apiListener);
    }

    /**
     * /app/order/CreateForProduct
     * 获取商品类型支付二维码
     *
     * @param apiListener 回调
     */
    public static void postGoodsTypeQRCode(BaseView apiListener, HashMap<String, Object> map) {
        RequestParams params = new RequestParams();
        params.put(map);
        apiTools.postRequest(Constant.HttpUrl.url + "/app/order/CreateForProduct", params, apiListener);
    }

    /**
     * 获取商品支付信息
     *
     * @param apiListener 回调
     */
    public static void getPayResult(BaseView apiListener, String OrderId) {
        RequestParams params = new RequestParams();
        params.put("OrderId", OrderId);
        apiTools.getRequest(Constant.HttpUrl.url + "/App/Order/getPayResult", params, apiListener);
    }


    /**
     * // 刷脸生成支付单号
     * "/App/SmilePay/CreareSmilePayOrder";
     */
    public static void postCreareSmilePayOrder(BaseView apiListener, HashMap<String, Object> map) {
        RequestParams params = new RequestParams();
        params.put(map);
        apiTools.postRequest(Constant.HttpUrl.url + "/App/SmilePay/CreareSmilePayOrder", params, apiListener);

    }


    /**
     * 将metaInfo发送给商户服务端，由商户服务端发起刷脸初始化OpenAPI的调用
     * "/SmilePay/Initialize?str=";
     */

    public static void getInitialize(BaseView apiListener, String map) {
        RequestParams params = new RequestParams();
        params.put("str", map);
        apiTools.getRequest(Constant.HttpUrl.url + "/SmilePay/Initialize", params, apiListener);
    }


    /**
     * 开始支付 刷脸
     * /App/SmilePay/StartSmilePay
     */

    public static void postStartSmilePay(BaseView apiListener, HashMap<String, Object> map) {
        RequestParams params = new RequestParams();
        params.put(map);
        apiTools.postRequest(Constant.HttpUrl.url + "/App/SmilePay/StartSmilePay", params, apiListener);
    }


    /**
     * 确认出货完成
     * /app/order/OrderIsEnd
     */

    public static void getOrderIsEnd(BaseView apiListener, HashMap<String, Object> map) {
        RequestParams params = new RequestParams();
        params.put(map);
        apiTools.getRequest(Constant.HttpUrl.url + "/app/order/OrderIsEnd", params, apiListener);
    }

    public static void cancal(String url) {
        apiTools.CallCancal(url_lower + url);
    }


    /**
     * 请求后台开启电控锁
     *
     * @param apiListener 回调
     */
    public static void getDoorlock(BaseView apiListener, String boxno) {
        String url = Constant.HttpUrl.url_cmd + "/MchAdminApi/send_open_lock";
        RequestParams params = new RequestParams();
        params.put("uid", ShareUtils.getInstance().getString(ShareKey.UID, ""));
        params.put("mchid", ShareUtils.getInstance().getString(ShareKey.MCH_ID));
        params.put("boxno", boxno);
        apiTools.getRequest(url, params, apiListener);
    }


    /**
     * 请求后台登录
     *
     * @param apiListener 回调
     */
    public static void getLogin(BaseView apiListener, String name, String password) {
        String uri = url + "/LoginByMch";
        RequestParams params = new RequestParams();
        params.put("sn", DeviceUtils.getDeviceSn());
        params.put("UserName", name);
        params.put("Pwd", password);
        apiTools.getRequest(uri, params, apiListener);
    }


    /**
     * 更改货道库存
     */
    public static void getStartReplenishment(BaseView apiListener, BuHuoBean listBeans) {
        String uri = url + "StartReplenishment";
        String json = GsonUtils.model2Json(listBeans);
        apiTools.postJsonRequest(uri, json, apiListener);
    }

    /**
     * http://www.shouhuojiyun.com/rknew/MchApi/GetActivity?mchid=1073
     * 获取优惠活动
     */
    public static void getActivityType(BaseView apiListener) {
        String uri = url + "GetActivity";
        RequestParams params = new RequestParams();
        params.put("mchid", ShareUtils.getInstance().getString(ShareKey.MCH_ID));
        apiTools.getRequest(uri, params, apiListener);
    }

    // ====================================== 刷卡的这一堆接口 ======================================

    /**
     * 卡充值
     *
     * @param recharge_price 充值金额
     * @param card_no        待充值卡号
     * @param apiListener    回调
     */
    public static void createRechargeOrder(String recharge_price, String card_no, BaseView apiListener) {
        JSONObject params = new JSONObject();
        try {
            params.put("mchId", ShareUtils.getInstance().getString(ShareKey.MCH_ID));
            params.put("recharge_price", recharge_price);
            params.put("card_no", card_no);
            apiTools.postJsonRequest(createRechargeOrderUrl, params.toString(), apiListener);
        } catch (JSONException e) {
            L.e("MchApiHttp - createRechargeOrder() Error:" + e.toString());
        }
    }

    /**
     * 刷卡按照商品下单
     *
     * @param cardCreateOrderByProductBean 创建的订单对象
     * @param apiListener                  回调
     */
    public static void cardCreateOrderByProduct(CardCreateOrderByProductBean cardCreateOrderByProductBean, BaseView apiListener) {
        if (cardCreateOrderByProductBean != null) {
            cardCreateOrderByProductBean.setMchid(ShareUtils.getInstance().getString(ShareKey.MCH_ID));
            apiTools.postJsonRequest(cardCreateOrderByProductUrl, GsonUtils.model2Json(cardCreateOrderByProductBean), apiListener);
        } else {
            L.e("创建的订单为空....");
        }
    }

    /**
     * 刷卡按照货道下单
     *
     * @param cardCreateOrderBySlotBean 创建的订单对象
     * @param apiListener               回调
     */
    public static void cardCreateOrderBySlot(CardCreateOrderBySlotBean cardCreateOrderBySlotBean, BaseView apiListener) {
        if (cardCreateOrderBySlotBean != null) {
            cardCreateOrderBySlotBean.setMchid(ShareUtils.getInstance().getString(ShareKey.MCH_ID));
            apiTools.postJsonRequest(cardCreateOrderBySlotUrl, GsonUtils.model2Json(cardCreateOrderBySlotBean), apiListener);
        } else {
            L.e("创建的订单为空....");
        }
    }

    /**
     * 刷卡订单完结
     *
     * @param order_id    订单号
     * @param isEnd       是否出货完成
     * @param remark      备注
     * @param apiListener 回调
     */
    public static void rkCardEndOrder(String order_id, boolean isEnd, String remark, BaseView apiListener) {
        RequestParams params = new RequestParams();
        params.put("order_id", order_id);
        params.put("IsEnd", isEnd);
        params.put("remark", remark);
        apiTools.getRequest(rkCardEndOrderUrl, params, apiListener);
    }

}
