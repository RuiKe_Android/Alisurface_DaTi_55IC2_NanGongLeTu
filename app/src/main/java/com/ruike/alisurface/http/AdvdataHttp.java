package com.ruike.alisurface.http;


import com.ruike.alisurface.BuildConfig;
import com.ruike.alisurface.Constant;
import com.ruike.alisurface.utils.ShareKey;
import com.voodoo.lib_frame.base.BaseView;
import com.voodoo.lib_frame.httpTools.ApiTools;
import com.voodoo.lib_frame.httpTools.RequestParams;
import com.voodoo.lib_utils.ShareUtils;

import static com.ruike.alisurface.Constant.APP_TYPE;

/**
 * Created by czb on 2019-12-28.
 */

public class AdvdataHttp {


    private static String url = Constant.HttpUrl.url + "/MchApi/";
    private static String url_lower = Constant.HttpUrl.url + "/mchapi/";
    private static String url_cmd = Constant.HttpUrl.url_cmd;
    private static ApiTools apiTools = new ApiTools();

    /**
     * 安卓告知服务器广告更换完成
     */
    public static void getConfirm_ApplyAd(String ad_id, BaseView baseView) {
        String id1 = ShareUtils.getInstance().getString(ShareKey.MCH_ID);
        String Confirm_ApplyAd = url_cmd + "/A_AdApi/Confirm_ApplyAd";
        RequestParams params = new RequestParams();
        params.put("Mchid", id1);
        params.put("ad_id", ad_id);
        apiTools.postRequest(Confirm_ApplyAd, params, baseView);

    }


    /**
     * 告知服务器主程序版本信息
     */
    public static void TellService_AppVersion_Info(BaseView baseView) {
        String id1 = ShareUtils.getInstance().getString(ShareKey.MCH_ID);
        RequestParams params = new RequestParams();
        params.put("mchid", id1);
        params.put("AppProject_Id", APP_TYPE);
        params.put("app_Version", BuildConfig.VERSION_NAME);
        params.put("packageName", BuildConfig.APPLICATION_ID);

        String TellService_AppVersion_Info = url_cmd + "/AppUpdate/TellService_AppVersion_Info";

        apiTools.postRequest(TellService_AppVersion_Info, params, baseView);

    }

}
