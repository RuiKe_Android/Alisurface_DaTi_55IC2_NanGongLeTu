package com.ruike.alisurface.http;

import com.ruike.alisurface.Constant;
import com.ruike.alisurface.utils.ShareKey;
import com.voodoo.lib_frame.base.BaseView;
import com.voodoo.lib_frame.httpTools.ApiTools;
import com.voodoo.lib_frame.httpTools.RequestParams;
import com.voodoo.lib_utils.ShareUtils;

/**
 * Author: voodoo
 * CreateDate: 2020-03-26 026 下午 01:59
 * Description: 卡的网络请求类
 */
public class CardHttp {

    private static String url = Constant.HttpUrl.url + "/App/Card/";

    /**
     * 查询卡信息 http://120.78.219.34:9018/otherApi/card/get_card_details
     * 原始查询卡信息 http://www.shouhuojiyun.com/Rknew/App/Card/QueryCardNo?CardNo=卡号
     *
     * @param cardNo 卡号
     */
    public static void getCardDetails(String cardNo, BaseView apiListener) {
        ApiTools apiTools = new ApiTools();
        RequestParams params = new RequestParams();
        params.put("mchid", ShareUtils.getInstance().getString(ShareKey.MCH_ID));
        params.put("card_no", cardNo);
        apiTools.getRequest("http://120.78.219.34:9018/otherApi/card/get_card_details", params, apiListener);
    }

    /**
     * 充值 http://www.shouhuojiyun.com/Rknew/App/Card/Recharge?cardno=卡号&SN=Sn
     *
     * @param cardNo 卡号
     * @param sn     SN号
     */
    public static void recharge(String cardNo, String sn, BaseView apiListener) {
        ApiTools apiTools = new ApiTools();
        RequestParams params = new RequestParams();
        params.put("cardNo", cardNo);
        params.put("SN", sn);
        apiTools.getRequest(url + "Recharge", params, apiListener);
    }

    /**
     * 创建订单
     *
     * @param mchId        机器id
     * @param cardNo       卡号
     * @param feeTypeId    费用类型 感觉是扣款类型，104写死的
     * @param payTime      支付时间
     * @param totalFee     总支付金额
     * @param payFee       实际支付金额
     * @param orderDetails 订单详情，JSONObject类型的String
     * @param apiListener  回调
     */
    public static void createOrder(String mchId, String cardNo, String feeTypeId, String payTime, double totalFee, double payFee, String orderDetails, BaseView apiListener) {
        ApiTools apiTools = new ApiTools();
        RequestParams params = new RequestParams();
        params.put("mchId", mchId);
        params.put("CardNo", cardNo);
        params.put("Fee_typeId", feeTypeId);
        params.put("Pay_time", payTime);
        params.put("Total_fee", totalFee);
        params.put("Pay_fee", payFee);
        params.put("OrderDetails", orderDetails);
        apiTools.getRequest(url + "CreateOrder", params, apiListener);
    }

}
