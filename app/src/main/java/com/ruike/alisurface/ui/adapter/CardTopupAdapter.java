package com.ruike.alisurface.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ruike.alisurface.R;

import java.util.List;

public class CardTopupAdapter extends RecyclerView.Adapter<CardTopupAdapter.ViewHolder> {

    Context context;
    List<Float> list;
    private ViewHolder lastViewHolder = null;

    public CardTopupAdapter(Context context, List<Float> list) {
        this.context = context;
        this.list = list;
        this.list.add(-100F);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_top_up_price, viewGroup, false);
        return new ViewHolder(view);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        float price = list.get(i);

        if (price < 0) {
            viewHolder.priceTv.setVisibility(View.GONE);
            viewHolder.yuanTv.setVisibility(View.GONE);
            viewHolder.priceEt.setVisibility(View.VISIBLE);
            viewHolder.priceEt.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        viewHolder.bgRlayout.performClick();
                        String inputPrice = viewHolder.priceEt.getText().toString().trim();
                        if (inputPrice.isEmpty()) {
                            inputPrice = "0";
                        }
                        if (onPriceSelectListener != null) {
                            onPriceSelectListener.onSelectPrice(Integer.parseInt(inputPrice));
                        }
                    }
                    return false;
                }
            });
        } else {
            viewHolder.priceTv.setVisibility(View.VISIBLE);
            viewHolder.yuanTv.setVisibility(View.VISIBLE);
            viewHolder.priceEt.setVisibility(View.GONE);
            viewHolder.priceTv.setText(String.valueOf(price));
        }

        viewHolder.bgRlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeSelectStatus(viewHolder, i);
                if (price < 0) {
                    viewHolder.priceEt.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            String trim = viewHolder.priceEt.getText().toString().trim();
                            if (trim.isEmpty()) {
                                trim = "0";
                            }
                            if (onPriceSelectListener != null) {
                                onPriceSelectListener.onSelectPrice(Integer.parseInt(trim));
                            }
                        }
                    });
                } else {
                    if (onPriceSelectListener != null) {
                        onPriceSelectListener.onSelectPrice(price);
                    }
                }
            }
        });

    }

    /**
     * 修改选中状态
     *
     * @param viewHolder
     */
    private void changeSelectStatus(ViewHolder viewHolder, int i) {
        int textColor;
        int textHintColor = context.getResources().getColor(R.color.colorDefultHintText);
        if (lastViewHolder != null) {
            textColor = context.getResources().getColor(R.color.colorDefultText);
            lastViewHolder.bgRlayout.setSelected(false);
            lastViewHolder.priceTv.setTextColor(textColor); // 价格的TextView
            lastViewHolder.yuanTv.setTextColor(textColor); // 元的TextView
            lastViewHolder.priceEt.setHintTextColor(textHintColor); // 输入框的Hint文字颜色
            lastViewHolder.priceEt.setTextColor(textColor);  // 输入框输入的文字颜色
            lastViewHolder.priceEt.setText("");  // 输入框输入的文字颜色
        }
        textColor = context.getResources().getColor(R.color.colorDefultSelectText);
        viewHolder.bgRlayout.setSelected(true);
        viewHolder.priceTv.setTextColor(textColor);
        viewHolder.yuanTv.setTextColor(textColor);
        viewHolder.priceEt.setHintTextColor(textHintColor);
        viewHolder.priceEt.setTextColor(textColor);
        lastViewHolder = viewHolder;
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout bgRlayout;
        TextView priceTv;
        EditText priceEt;
        TextView yuanTv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            bgRlayout = itemView.findViewById(R.id.itemCardTopUp_bg_rlayout);
            priceTv = itemView.findViewById(R.id.itemCardTopUp_price_tv);
            priceEt = itemView.findViewById(R.id.itemCardTopUp_price_et);
            yuanTv = itemView.findViewById(R.id.itemCardTopUp_yuan_tv);
        }
    }

    public OnPriceSelectListener onPriceSelectListener;

    public void setOnPriceSelectListener(OnPriceSelectListener onPriceSelectListener) {
        this.onPriceSelectListener = onPriceSelectListener;
    }

    public interface OnPriceSelectListener {
        void onSelectPrice(float price);
    }

}
