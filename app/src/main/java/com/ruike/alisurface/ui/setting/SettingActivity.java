package com.ruike.alisurface.ui.setting;

import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.ruike.alisurface.BuildConfig;
import com.ruike.alisurface.Constant;
import com.ruike.alisurface.R;
import com.ruike.alisurface.Serials.MsgEventBus;
import com.ruike.alisurface.base.BaseAty;
import com.ruike.alisurface.bean.SettingPageItemBean;
import com.ruike.alisurface.services.AppRunMonitoringService;
import com.ruike.alisurface.ui.adapter.SettingPageAdapter;
import com.ruike.alisurface.ui.mainPage.MainActivity;
import com.ruike.alisurface.utils.BackMainUtils;
import com.voodoo.lib_frame.manager.FActivityManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Author: voodoo
 * CreateDate: 2020-04-16 016 下午 01:14
 * Description: 设置主界面
 */
public class SettingActivity extends BaseAty implements SettingPageAdapter.OnSettingItemClickListener {

    @BindView(R.id.setting_connect_recv)
    RecyclerView settingConnectRecv;

    List<SettingPageItemBean> settingPageItemBeans;
    SettingPageAdapter adapter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_setting;
    }

    @Override
    protected void initViews() {
        initTitleBar(false);

    }

    @Override
    protected void onResume() {
        super.onResume();
        startCountdownTimer(60, null, null);

    }

    @Override
    protected void initData() {
        settingPageItemBeans = new ArrayList<>();
        settingPageItemBeans.add(new SettingPageItemBean(1, R.mipmap.icon_setting, getResources().getString(R.string.baseSettingStr)));
        settingPageItemBeans.add(new SettingPageItemBean(2, R.mipmap.icon_slot, getResources().getString(R.string.slotTestStr)));
        settingPageItemBeans.add(new SettingPageItemBean(6, R.mipmap.icon_replenishment, getResources().getString(R.string.replenishment_settings)));
        adapter = new SettingPageAdapter(this, settingPageItemBeans);
        adapter.setOnSettingItemClickListener(this);
        settingConnectRecv.setLayoutManager(new GridLayoutManager(this, 2));
        settingConnectRecv.setAdapter(adapter);
    }

    @OnClick({R.id.setting_goback_tv, R.id.setting_exitApp_tv})
    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.setting_goback_tv:
                startCountdownTimer(0, null, null);
                break;
            case R.id.setting_exitApp_tv:
                // 停止服务退出APP
                stopService(new Intent(this, AppRunMonitoringService.class));
                FActivityManager.getInstance().killAllActivityExit();
                break;
        }
    }

    @Override
    public void onSettingItemClick(SettingPageItemBean settingPageItemBean, int index) {
        switch (settingPageItemBean.getFlag()) {
            case 1: // 基础设置
                startActivity(BasicSettingActivity.class, null);
                break;
            case 2: // 货道检测
                startActivity(SlotTestActivity.class, null);
                break;
            case 4: // 开启电控锁门
                startActivity(OpenDoorActivity.class, null);
                break;
            case 5: // 开启电控锁门
                startActivity(ChangeLanguageActivity.class, null);
                break;
            case 6: // 补货设置
                startActivity(BuHuoManagActivity.class, null);
                break;
            case 8: //  测试刷脸相机
                startActivity(SettingAlFaceActivity.class, null);
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void doEventBusResult(MsgEventBus event) {

    }
}
