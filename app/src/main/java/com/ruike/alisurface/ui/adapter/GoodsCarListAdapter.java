package com.ruike.alisurface.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ruike.alisurface.R;
import com.ruike.alisurface.bean.GoodsBean;
import com.voodoo.lib_utils.AssetsUtils;
import com.voodoo.lib_utils.imageLoader.ImageLoader;

import java.util.List;

public class GoodsCarListAdapter extends RecyclerView.Adapter<GoodsCarListAdapter.ViewHolder> {

    Context context;
    List<GoodsBean> list;

    public GoodsCarListAdapter(Context context, List<GoodsBean> list) {
        this.context = context;
        this.list = list;
    }

    public void setData(List<GoodsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_goods_car_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        GoodsBean goodsBean = list.get(i);

        ImageLoader.loadImage(context, goodsBean.getProductimg(), viewHolder.goodsImageImgv);

        viewHolder.priceTv.setText(new StringBuffer().append(goodsBean.getPrice()));
        viewHolder.deleteRlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemDeleteClickListener != null) {
                    onItemDeleteClickListener.onGoodsDeleteClick(i, goodsBean);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView goodsImageImgv;
        RelativeLayout deleteRlayout;
        ImageView deleteImgv;
        TextView priceTv;

        public ViewHolder(View itemView) {
            super(itemView);

            goodsImageImgv = itemView.findViewById(R.id.itemGoodsCarList_goodsImage_imgv);
            deleteRlayout = itemView.findViewById(R.id.itemGoodsCarList_delete_rlayout);
            deleteImgv = itemView.findViewById(R.id.itemGoodsCarList_delete_imgv);
            priceTv = itemView.findViewById(R.id.itemGoodsCarList_price_tv);
        }
    }

    public OnItemDeleteClickListener onItemDeleteClickListener;

    public void setOnItemDeleteClickListener(OnItemDeleteClickListener onItemDeleteClickListener) {
        this.onItemDeleteClickListener = onItemDeleteClickListener;
    }

    /**
     * 删除购物车商品
     */
    public interface OnItemDeleteClickListener {
        void onGoodsDeleteClick(int index, GoodsBean goodsBean);
    }

}
