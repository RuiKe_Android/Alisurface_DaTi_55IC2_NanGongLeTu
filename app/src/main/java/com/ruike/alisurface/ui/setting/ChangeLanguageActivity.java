package com.ruike.alisurface.ui.setting;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.ImageView;

import com.ruike.alisurface.MyApplication;
import com.ruike.alisurface.R;
import com.ruike.alisurface.Serials.MsgEventBus;
import com.ruike.alisurface.base.BaseAty;
import com.ruike.alisurface.utils.Languages.LanguageType;
import com.ruike.alisurface.utils.Languages.LanguageUtil;
import com.ruike.alisurface.utils.ShareKey;
import com.voodoo.lib_frame.manager.FActivityManager;
import com.voodoo.lib_utils.ShareUtils;

import butterknife.BindView;

public class ChangeLanguageActivity extends BaseAty {
    @BindView(R.id.goback_imgv)
    ImageView gobackImgv;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_change_language;
    }

    @Override
    protected void initViews() {

        initTitleBar(false);

        gobackImgv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(SettingActivity.class, null);
                finish();
            }
        });
    }

    @Override
    protected void initData() {

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void onClick(View view) {
        String language = null;
        switch (view.getId()) {
            case R.id.btn_chinese:
                //切换为简体中文
                language = LanguageType.CHINESE.getLanguage();
                break;
            case R.id.btn_english:
                //切换为英语
                language = LanguageType.ENGLISH.getLanguage();
                break;
            default:
                break;

        }

        changeLanguage(language);
    }

    /**
     * 如果是7.0以下，我们需要调用changeAppLanguage方法，
     * 如果是7.0及以上系统，直接把我们想要切换的语言类型保存在SharedPreferences中即可
     * 然后重新启动MainActivity
     *
     * @param language
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void changeLanguage(String language) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            LanguageUtil.changeAppLanguage(MyApplication.getAppContext(), language);
        }
        ShareUtils.getInstance().putString(ShareKey.LANGUAGE, language);
        startActivity(SettingActivity.class, null);
        finish();
        FActivityManager.getInstance().killOtherActivity();
    }

    @Override
    public void doEventBusResult(MsgEventBus event) {

    }
}
