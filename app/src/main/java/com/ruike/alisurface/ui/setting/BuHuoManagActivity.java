package com.ruike.alisurface.ui.setting;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ruike.alisurface.R;
import com.ruike.alisurface.Serials.MsgEventBus;
import com.ruike.alisurface.base.BaseAty;
import com.ruike.alisurface.bean.BuHuoBean;
import com.ruike.alisurface.bean.GoodsBean;
import com.ruike.alisurface.http.MchApiHttp;
import com.ruike.alisurface.ui.adapter.Buhu_SlotsAdapter;
import com.ruike.alisurface.utils.ShareKey;
import com.voodoo.lib_utils.GsonUtils;
import com.voodoo.lib_utils.L;
import com.voodoo.lib_utils.ShareUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 一键补货 页面 进行商品数据的修改
 */

public class BuHuoManagActivity extends BaseAty {
    @BindView(R.id.bh_keyrepment)
    TextView bhKeyrepment;
    @BindView(R.id.bh_refresh)
    TextView bhRefresh;
    @BindView(R.id.bh_submit)
    TextView bhSubmit;
    @BindView(R.id.bhStol_listview)
    RecyclerView bhStolListview;
    @BindView(R.id.lg_back)
    ImageView lgBack;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_buhuomanag;
    }

    @Override
    protected void initViews() {

        initTitleBar(false);

        lgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        bhRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgressDialog("库存刷新中，请等待...");
                MchApiHttp.getSlots(BuHuoManagActivity.this);
            }
        });

        bhSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bhlist.size() <= 0) {
                    return;
                }
                showProgressDialog("库存提交更新中，请等待...");
                UpSlotCount();
            }
        });

        bhKeyrepment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRightTip("一键补货，请等待...,稍后选择提交");
//                MchApiHttp.getSettingAllCount(BuHuoManagActivity.this);
                if (bhlist.size() <= 0) {
                    return;
                }
                for (int i = 0; i < bhlist.size(); i++) {
                    bhlist.get(i).setCount(bhlist.get(i).getMaxcount());
                    bhlist.set(i, bhlist.get(i));
                }
                bh_slotsAdapter.setData(bhlist);
            }
        });
    }

    List<GoodsBean> bhlist = new ArrayList<>();
    Buhu_SlotsAdapter bh_slotsAdapter;

    @Override
    protected void initData() {
        showProgressDialog("请求数据中，请等待.....");
        MchApiHttp.getSlots(this);
        bhStolListview.setLayoutManager(new LinearLayoutManager(this));
        bh_slotsAdapter = new Buhu_SlotsAdapter(this, bhlist);
        bhStolListview.setAdapter(bh_slotsAdapter);
    }

    @Override
    public void onSuccess(String requestUrl, String requestJsonStr) {
        super.onSuccess(requestUrl, requestJsonStr);

        JSONObject object = null;
        try {
            object = new JSONObject(requestJsonStr);
            String data = object.optString("data");
            if (requestUrl.endsWith("getSlots")) { // 获取商品货道信息
                bhlist = GsonUtils.jsonArray2ModelList(data, GoodsBean.class);
                L.i("BHLIST===", bhlist.size());
                if (bhlist == null) {
                    return;
                }
                bh_slotsAdapter.setData(bhlist);
                return;
            } else if (requestUrl.endsWith("StartReplenishment")) {
                showRightTip("补货成功");
                return;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(String requestUrl, String errorMsg) {
        super.onError(requestUrl, errorMsg);

        if (requestUrl.endsWith("StartReplenishment")) {
            showErrorTip("补货失败");
            return;
        }
    }

    /**
     * 遍历库存更新数据，上传后台，更新库存
     */
    public void UpSlotCount() {
        BuHuoBean buHuoBean = new BuHuoBean();
        List<BuHuoBean.ReplenishmenterListBean> slotlist = new ArrayList<>();
        for (GoodsBean goodsBean : bhlist) {
            StringBuilder sb = new StringBuilder();
            BuHuoBean.ReplenishmenterListBean replenishmenterListBean = new BuHuoBean.ReplenishmenterListBean();
            replenishmenterListBean.setCount(goodsBean.getCount());
            sb.append(ShareUtils.getInstance().getString(ShareKey.MCH_ID)).append("-").append(goodsBean.getIndex());
            replenishmenterListBean.setSlot_id(sb.toString());
            replenishmenterListBean.setSlotindex(goodsBean.getIndex());
            slotlist.add(replenishmenterListBean);
        }
        buHuoBean.setMchid(ShareUtils.getInstance().getString(ShareKey.MCH_ID));
        buHuoBean.setReplenishmenter_list(slotlist);
        MchApiHttp.getStartReplenishment(BuHuoManagActivity.this, buHuoBean);
    }

    @Override
    public void doEventBusResult(MsgEventBus event) {

    }
}
