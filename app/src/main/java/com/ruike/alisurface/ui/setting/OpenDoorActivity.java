package com.ruike.alisurface.ui.setting;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.ruike.alisurface.Constant;
import com.ruike.alisurface.R;
import com.ruike.alisurface.Serials.MsgEventBus;
import com.ruike.alisurface.base.BaseAty;
import com.ruike.alisurface.bean.SettingPageItemBean;
import com.ruike.alisurface.http.MchApiHttp;
import com.ruike.alisurface.ui.adapter.SettingPageAdapter;
import com.voodoo.lib_utils.L;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class OpenDoorActivity extends BaseAty implements SettingPageAdapter.OnSettingItemClickListener {


    @BindView(R.id.opendoor_recv)
    RecyclerView opendoorRecv;
    List<SettingPageItemBean> settingPageItemBeans;
    SettingPageAdapter adapter;
    @BindView(R.id.goback_imgv)
    ImageView gobackImgv;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_open_door;
    }

    @Override
    protected void initViews() {

        initTitleBar(false);
        gobackImgv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void initData() {
        settingPageItemBeans = new ArrayList<>();
        settingPageItemBeans.add(new SettingPageItemBean(1, R.mipmap.icon_door, getResources().getString(R.string.opendoor_one)));
        settingPageItemBeans.add(new SettingPageItemBean(2, R.mipmap.icon_door, getResources().getString(R.string.opendoor_twe)));
        settingPageItemBeans.add(new SettingPageItemBean(3, R.mipmap.icon_door, getResources().getString(R.string.opendoor_three)));
        settingPageItemBeans.add(new SettingPageItemBean(4, R.mipmap.icon_door, getResources().getString(R.string.opendoor_four)));
        settingPageItemBeans.add(new SettingPageItemBean(5, R.mipmap.icon_door, getResources().getString(R.string.opendoor_five)));
        adapter = new SettingPageAdapter(this, settingPageItemBeans);
        adapter.setOnSettingItemClickListener(this);
        opendoorRecv.setLayoutManager(new GridLayoutManager(this, 2));
        opendoorRecv.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.showShop=true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        Constant.showShop=false;
    }

    @Override
    public void doEventBusResult(MsgEventBus event) {

    }

    @Override
    public void onSettingItemClick(SettingPageItemBean settingPageItemBean, int index) {
        switch (settingPageItemBean.getFlag()) {
            case 1: //开启电控锁门1

                MchApiHttp.getDoorlock(this, "0");
                showRightTip(getString(R.string.open_door));

                break;
            case 2: //开启电控锁门2

                MchApiHttp.getDoorlock(this, "1");
                showRightTip(getString(R.string.open_door));

                break;
            case 3: // 开启电控锁门3

                MchApiHttp.getDoorlock(this, "2");
                showRightTip(getString(R.string.open_door));

                break;
            case 4: // 开启电控锁门4

                MchApiHttp.getDoorlock(this, "3");
                showRightTip(getString(R.string.open_door));

                break;
            case 5: // 开启电控锁门4

                MchApiHttp.getDoorlock(this, "4");
                showRightTip(getString(R.string.open_door));

                break;
            default:
                break;

        }
    }

    @Override
    public void onSuccess(String requestUrl, String requestJsonStr) {
        super.onSuccess(requestUrl, requestJsonStr);
        if (requestUrl.endsWith("send_open_lock")) {
            L.i(" lock==  onSuccess ",requestJsonStr);
//            showRightTip(getString(R.string.open_door));
        }
    }


    @Override
    public void onError(String requestUrl, String errorMsg) {
        super.onError(requestUrl, errorMsg);
        if (requestUrl.endsWith("send_open_lock")) {

            L.i(" lock==  onError ",errorMsg);
//            showRightTip(getString(R.string.open_door_fail));

        }
    }
}
