package com.ruike.alisurface.ui.payPage;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ruike.alisurface.R;
import com.ruike.alisurface.Serials.MsgEventBus;
import com.ruike.alisurface.Serials.ShopInfoUtils;
import com.ruike.alisurface.base.BaseAty;
import com.ruike.alisurface.bean.ShopDetailBean;
import com.ruike.alisurface.ui.mainPage.MainActivity;
import com.ruike.alisurface.ui.mainPage.WebViewActivity;
import com.ruike.alisurface.utils.MyCountDownTimer;
import com.ruike.alisurface.utils.ShareKey;
import com.voodoo.lib_frame.view.LoadingView;
import com.voodoo.lib_utils.L;
import com.voodoo.lib_utils.ShareUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;

/**
 * 支付刷脸出货完成页
 */
public class PayfaceSuccessActivity extends BaseAty {

    private static final String TAG = "tjf";

    @BindView(R.id.outGoodsResult_show_imgv)
    ImageView showResultImgv;

//    @BindView(R.id.order_outGoodsResult_loadv)
//    LoadingView outGoodsResultLoadv;
//    @BindView(R.id.order_outGoodsResult_tv)
//    TextView outGoodsResultTv;
//
//    @BindView(R.id.order_money)
//    TextView orderMoneyTv;
//    @BindView(R.id.actual_money)
//    TextView actualMoneyTv;
//    @BindView(R.id.discount)
//    TextView discountTv;
//    @BindView(R.id.selectPay_countDown_tv)
//    TextView selectPayCountDownTv;
//    @BindView(R.id.order_goLottery_imgv)
//    ImageView order_goLottery_imgv;
//
//    @BindView(R.id.paysucc_content)
//    TextView paysucc_content;

    String orderid;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_payface_success;
    }

    @Override
    protected void onResume() {
        super.onResume();
        startCountdownTimer(5, null, null);
//        startCountdownTimer(10, selectPayCountDownTv, null);
    }

    List<ShopInfoUtils.slotBean> slotBeanList = new ArrayList<>();


    @Override
    protected void initViews() {
//        initTitleBar(false);

        L.i("acty_id=acty_config===", ShareUtils.getInstance().getString(ShareKey.ACTIVITY_ID, "0"));

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            boolean isSuccess = bundle.getBoolean("isSuccess");
            double order_money = bundle.getDouble("order_money");
            double actual_money = bundle.getDouble("actual_money");
            double discount = bundle.getDouble("discount");

            showResultImgv.setImageResource(isSuccess ? R.drawable.img_out_goods_right : R.drawable.img_out_goods_error);
//            new MyCountDownTimer(2000, 2000) {
//
//                @Override
//                public void onTick(long millisUntilFinished) {
//                }
//
//                @Override
//                public void onFinish() {
//                    outGoodsResultLoadv.setStatus(isSuccess ? LoadingView.STATUS_SUCCESS : LoadingView.STATUS_FAIL);
//                }
//            }.start();

//            outGoodsResultTv.setText(isSuccess ? "出货成功" : "出货失败");
//            orderMoneyTv.setText(new StringBuffer("订单金额: ￥").append(order_money));
//            actualMoneyTv.setText(new StringBuffer("消费金额: ￥").append(actual_money));
//            discountTv.setText(new StringBuffer(" ￥").append(discount));
            orderid = bundle.getString("order_id");
            Log.i(TAG, "initView-------: " + discount);
            try {
                if (bundle.getSerializable("slotList") != null) {
                    slotBeanList.addAll((Collection<? extends ShopInfoUtils.slotBean>) bundle.getSerializable("slotList"));
                    Collections.sort(slotBeanList, new Comparator<ShopInfoUtils.slotBean>() {
                        @Override
                        public int compare(ShopInfoUtils.slotBean o1, ShopInfoUtils.slotBean o2) {
                            return Integer.valueOf(o1.getSlot(), 16) - Integer.valueOf(o2.getSlot(), 16);
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            StringBuffer sbf = new StringBuffer();
            if (slotBeanList.size() > 0) {
                for (ShopInfoUtils.slotBean slotBean : slotBeanList) {
                    sbf.append(Integer.parseInt(slotBean.getSlot(), 16) + "货道  ");
                }
//                paysucc_content.setText("未出货的货道号: " + sbf.toString());
            }
        }
        if (ShareUtils.getInstance().getString(ShareKey.ACTIVITY_ID, "0").equals("1")) {
//            order_goLottery_imgv.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void initData() {
//        order_goLottery_imgv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Bundle bundle = new Bundle();
//                bundle.putString("orderid", orderid);
//                startActivity(WebViewActivity.class, bundle);
//                finish();
//            }
//        });
    }


    @Override
    public void doEventBusResult(MsgEventBus event) {

    }

}
