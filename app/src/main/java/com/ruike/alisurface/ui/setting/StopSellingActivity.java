package com.ruike.alisurface.ui.setting;

import com.ruike.alisurface.R;
import com.ruike.alisurface.Serials.MsgEventBus;
import com.ruike.alisurface.base.BaseAty;
import com.voodoo.lib_frame.manager.FActivityManager;
import com.voodoo.lib_utils.L;

/**
 * 停止售卖，反正就是展示所有最高权限停止操作的界面
 */
public class StopSellingActivity extends BaseAty {

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_stop_sell;
    }

    @Override
    protected void initViews() {
        initTitleBar(true);
    }

    @Override
    protected void initData() {
        L.i(getString(R.string.systemMaintainingStr) + "实际上就是机器不卖东西了...");
        FActivityManager.getInstance().killOtherActivity();
    }

    @Override
    public void doEventBusResult(MsgEventBus event) {

    }
}
