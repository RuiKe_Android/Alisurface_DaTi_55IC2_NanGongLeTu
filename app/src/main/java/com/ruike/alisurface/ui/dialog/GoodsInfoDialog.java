package com.ruike.alisurface.ui.dialog;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.BounceInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.ruike.alisurface.R;

import com.ruike.alisurface.bean.GoodsBean;
import com.ruike.alisurface.bean.GoodsInfoBean;
import com.ruike.alisurface.ui.adapter.GoodsInfoListAdapter;
import com.ruike.alisurface.utils.BlurBitmapUtil;
import com.ruike.alisurface.utils.DensityUtil;
import com.ruike.alisurface.utils.GlideImageLoader;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 商品详情弹窗介绍
 * Created by admin on 2019/11/29 029.
 */
public class GoodsInfoDialog {

    public static Dialog dialog;

    public static class Build {

        Context context;
        Bitmap background; // 背景图
        Bitmap blurBg; // 模糊处理之后的背景图
        int position;
        GoodsBean goodsBean;

        public OnDialogClickListener onDialogClickListener;

        private TextView infoShowTv;
        private LinearLayout composTitleLlayout;
        private RecyclerView composListRecv;
        private TextView noListTv;
        private View composTitleListLineView; // 标题和列表的分割线

        LinearLayout iceLRllayout; // 两侧冰块
        //        ImageView iceBigImgv; // 大冰块
        ImageView iceSmallImgv; // 小冰块

        public Build(Context context) {
            this.context = context;
        }

        /**
         * 设置模糊后的背景图
         *
         * @param drawable Bitmap背景图片
         */
        public Build setBackground(Bitmap drawable) {
            this.background = drawable;
            return this;
        }

        /**
         * 设置点击的商品在列表中的下标
         *
         * @param position 下标
         */
        public Build setPosition(int position) {
            this.position = position;
            return this;
        }

        /**
         * 设置商品对象，主要是商品的价格名称图片和货道信息
         *
         * @param goodsBean 商品对象
         */
        public Build setGoodsBean(GoodsBean goodsBean) {
            this.goodsBean = goodsBean;
            return this;
        }

        /**
         * 设置展示的数据
         *
         * @param goodsInfoBean 获取的数据
         */
        public void setInfoData(GoodsInfoBean goodsInfoBean) {
            String des = "";
            String templateContent = "";
            String structureContent = "";
            if (goodsInfoBean != null) {
                des = goodsInfoBean.getDes();
                templateContent = goodsInfoBean.getTemplateContent();
                structureContent = goodsInfoBean.getStructureContent();
            }
            if (des == null || des.isEmpty()) {
                infoShowTv.setGravity(Gravity.CENTER);
                infoShowTv.setTextSize(30);
                infoShowTv.setText("暂无描述信息");
            } else {
                infoShowTv.setGravity(Gravity.START);
                infoShowTv.setTextSize(24);
                infoShowTv.setText(des);
            }

            if (templateContent != null && structureContent != null && !templateContent.isEmpty() && !structureContent.isEmpty()) {
                noListTv.setVisibility(View.GONE);
                composTitleListLineView.setVisibility(View.VISIBLE);
                try {
                    // 表头列表
                    JSONObject templateContentJsonObj = new JSONObject(templateContent);
                    String cateName = templateContentJsonObj.optString("CateName");
                    if (cateName != null && !cateName.isEmpty() && cateName.equals("饮料")) {
                        iceLRllayout.setVisibility(View.VISIBLE); // 两侧冰块
//                        iceBigImgv.setVisibility(View.VISIBLE); // 大冰块
                        iceSmallImgv.setVisibility(View.VISIBLE); // 小冰块

                        // 两侧冰块动画
                        ObjectAnimator iceLRAnimator = ObjectAnimator.ofFloat(iceLRllayout, "translationY", -303f, 0f);
                        iceLRAnimator.setDuration(1500);
                        iceLRAnimator.setInterpolator(new BounceInterpolator());
                        iceLRAnimator.start();
                        // 大冰块动画
//                        ObjectAnimator iceBigAnimator = ObjectAnimator.ofFloat(iceBigImgv, "translationY", -303f, 0f);
//                        iceBigAnimator.setDuration(2100);
//                        iceBigAnimator.setInterpolator(new BounceInterpolator());
//                        iceBigAnimator.start();
                        // 小冰块动画
                        ObjectAnimator iceSmallAnimatorY = ObjectAnimator.ofFloat(iceSmallImgv, "translationY", -303f, 0f);
                        iceSmallAnimatorY.setInterpolator(new BounceInterpolator());
                        ObjectAnimator iceSmallAnimator = ObjectAnimator.ofFloat(iceSmallImgv, "rotation", 0f, 360f);
                        AnimatorSet iceSmallAnimatorSet = new AnimatorSet();
                        iceSmallAnimatorSet.setDuration(2800);
                        iceSmallAnimatorSet.playTogether(iceSmallAnimatorY, iceSmallAnimator);// 同时播放
                        iceSmallAnimatorSet.start();

                    }
                    JSONArray templateContentArray = templateContentJsonObj.optJSONArray("Column");
                    for (int i = 0; i < templateContentArray.length(); i++) {
                        String columnName = templateContentArray.getJSONObject(i).getString("ColumnName");
                        LinearLayout contentllayout = new LinearLayout(context);
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                        contentllayout.setLayoutParams(layoutParams);
                        contentllayout.setGravity(Gravity.CENTER);

                        TextView textView = new TextView(context);
                        textView.setText(columnName);
                        textView.setTextColor(Color.WHITE);
                        textView.setTextSize(DensityUtil.px2sp(context, 34f));

                        contentllayout.addView(textView);
                        composTitleLlayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        composTitleLlayout.addView(contentllayout);
                    }

                    // 内容的列表
                    JSONArray structureContentArray = new JSONArray(structureContent);
                    GoodsInfoListAdapter adapter = new GoodsInfoListAdapter(context, structureContentArray, templateContentArray);
                    composListRecv.setLayoutManager(new LinearLayoutManager(context));
                    composListRecv.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                // 空数据展示文字设置在布局设置
                composTitleListLineView.setVisibility(View.GONE);
                noListTv.setVisibility(View.VISIBLE);
            }

        }

        public void show() {
            dialog = new Dialog(context, R.style.GoodsInfoDialogStyle);
            // 模糊处理并保存
            Window window = dialog.getWindow();
            if (window != null && background != null) {
                blurBg = BlurBitmapUtil.blur(context, background);
                window.setBackgroundDrawable(new BitmapDrawable(context.getResources(), blurBg)); // 将图片设置成dialog的背景
                background.recycle();
            }
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    // 对话框取消时释放背景图bitmap
                    if (blurBg != null && blurBg.isRecycled() && background != null && background.isRecycled()) {
                        if (onDialogClickListener != null) {
                            onDialogClickListener.onDialogClose();
                        }
                        blurBg.recycle(); // 将Bitmap回收
                    }
                }
            });

            View view = LayoutInflater.from(context).inflate(R.layout.dialog_goods_info, null); // 获取Dialog对应的布局文件
            ImageView closeImgv = view.findViewById(R.id.goodsInfoDialog_close_imgv); // 关闭按钮
            TextView priceTv = view.findViewById(R.id.goodsInfoDialog_price_tv); // 价格
            TextView nameTv = view.findViewById(R.id.goodsInfoDialog_name_tv); // 名称
            TextView aisleNumberTv = view.findViewById(R.id.goodsInfoDialog_aisleNumber_tv); // 货道号
            // 详情描述展示
            infoShowTv = view.findViewById(R.id.goodsInfoDialog_infoShow_tv);
            composTitleLlayout = view.findViewById(R.id.goodsInfoDialog_composTitle_llayout);
            // 成分表
            composListRecv = view.findViewById(R.id.goodsInfoDialog_composList_recv);
            composTitleListLineView = view.findViewById(R.id.goodsInfoDialog_composTitleListLine_view);
            noListTv = view.findViewById(R.id.goodsInfoDialog_noList_tv);
            ImageView goodsImgImgv = view.findViewById(R.id.goodsInfoDialog_goodsImg_imgv); // 商品图片

            iceLRllayout = view.findViewById(R.id.goodsInfoDialog_iceLR_llayout); // 两侧冰块
//            iceBigImgv = view.findViewById(R.id.goodsInfoDialog_iceBig_imgv); // 大冰块
            iceSmallImgv = view.findViewById(R.id.goodsInfoDialog_iceSmall_imgv); // 小冰块

            LinearLayout addCarLlayout = view.findViewById(R.id.goodsInfoDialog_addCar_llayout); // 加入购物车
            closeImgv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
            });

            if (goodsBean != null) {
                priceTv.setText(new StringBuffer().append("：").append(goodsBean.getPrice()).toString()); // 设置价格
                nameTv.setText(new StringBuffer().append("：").append(goodsBean.getProductname()).toString()); // 设置商品名称
                aisleNumberTv.setText(new StringBuffer().append("：").append(goodsBean.getIndex()).toString()); // 设置商品货道
                new GlideImageLoader().displayImage(context, goodsBean.getProductimg(), goodsImgImgv); // 设置商品图片

            }

            addCarLlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dialog.isShowing()) {
                        if (onDialogClickListener != null) {
                            onDialogClickListener.onAddCar(position);
                        }
                        dialog.dismiss();
                    }
                }
            });

            dialog.setContentView(view);
            dialog.show();
        }

        public void setOnDialogClickListener(OnDialogClickListener onDialogClickListener) {
            this.onDialogClickListener = onDialogClickListener;
        }

        public interface OnDialogClickListener {
            /**
             * 关闭弹窗
             */
            void onDialogClose();

            /**
             * 添加购物车
             *
             * @param position 查看的商品在列表中的下标
             */
            void onAddCar(int position);
        }

    }


}