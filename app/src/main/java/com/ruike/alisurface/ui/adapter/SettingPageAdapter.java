package com.ruike.alisurface.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ruike.alisurface.R;
import com.ruike.alisurface.bean.SettingPageItemBean;

import java.util.List;

/**
 * Author: voodoo
 * CreateDate: 2020-04-16 016 下午 03:32
 * Description: 设置界面列表的适配器
 */
public class SettingPageAdapter extends RecyclerView.Adapter<SettingPageAdapter.ViewHolder> {

    Context context;
    List<SettingPageItemBean> list;

    public SettingPageAdapter(Context context, List<SettingPageItemBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_setting_page, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        SettingPageItemBean settingPageItemBean = list.get(i);

        viewHolder.img_imgv.setImageResource(settingPageItemBean.getImgResId());
        viewHolder.name_tv.setText(settingPageItemBean.getSettingName());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (onSettingItemClickListener != null) {
                    onSettingItemClickListener.onSettingItemClick(settingPageItemBean, i);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_imgv;
        private TextView name_tv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_imgv = itemView.findViewById(R.id.itemSettingPage_img_imgv);
            name_tv = itemView.findViewById(R.id.itemSettingPage_name_tv);
        }

    }

    private OnSettingItemClickListener onSettingItemClickListener;

    public void setOnSettingItemClickListener(OnSettingItemClickListener onSettingItemClickListener) {
        this.onSettingItemClickListener = onSettingItemClickListener;
    }

    public interface OnSettingItemClickListener {
        void onSettingItemClick(SettingPageItemBean settingPageItemBean, int index);
    }

}
