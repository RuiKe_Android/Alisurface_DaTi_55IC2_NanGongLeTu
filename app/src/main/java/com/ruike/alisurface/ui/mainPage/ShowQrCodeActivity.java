package com.ruike.alisurface.ui.mainPage;

import android.view.View;
import android.widget.ImageView;

import com.ruike.alisurface.Constant;
import com.ruike.alisurface.R;
import com.ruike.alisurface.Serials.MsgEventBus;
import com.ruike.alisurface.base.BaseAty;
import com.ruike.alisurface.project_DaTi.AnswerQuestionActivity;
import com.ruike.alisurface.utils.ShareKey;
import com.ruike.alisurface.utils.ZXingUtils;
import com.voodoo.lib_utils.L;
import com.voodoo.lib_utils.ShareUtils;

import butterknife.BindView;

public class ShowQrCodeActivity extends BaseAty {

    @BindView(R.id.showQrCode_qrcode_imgv)
    ImageView qrCodeImgv;
    @BindView(R.id.showQrCode_toLogin_view)
    View toLoginView;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_show_qrcode;
    }

    @Override
    protected void initViews() {
    }

    @Override
    protected void initData() {
        // 生成关注二维码
        qrCodeImgv.setImageBitmap(ZXingUtils.createQRImage("https://rk.shouhuojiyun.com/otherapi/letu/login.html?mchId=" + ShareUtils.getInstance().getString(ShareKey.MCH_ID), 300, 300, null));
        if (ShareUtils.getInstance().getInt(ShareKey.ADV_TYPE) == Constant.ADV_TYPE_NONE) {
            toLoginView.setOnClickListener(view -> {
                continuousClick();
            });
        } else {
            // 倒计时
            startCountdownTimer(60, null, null);
        }
    }

    @Override
    public void doEventBusResult(MsgEventBus event) {
        // 收到消息
        L.i(event.type + ":" + event.getMessage());
        L.i("\"AnswerQuestionLogin\".equals(event.type):" + ("AnswerQuestionLogin".equals(event.type)));
        if ("AnswerQuestionLogin".equals(event.type)) {
            startActivity(AnswerQuestionActivity.class, null);
            finish();
        }
    }

}