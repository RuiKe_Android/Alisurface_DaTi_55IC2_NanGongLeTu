package com.ruike.alisurface.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ruike.alisurface.R;
import com.ruike.alisurface.bean.GoodsBean;

import java.util.List;

/**
 * 货道检测适配器
 * Created by czb on 2019-12-02.
 */

public class Slot_test_Adapter extends RecyclerView.Adapter<Slot_test_Adapter.ViewHolder> {


    Context context;
    List<GoodsBean> datas;

    public Slot_test_Adapter(Context context, List<GoodsBean> datas) {

        this.context = context;
        this.datas = datas;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_solt_index, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.index_text.setText(datas.get(i).getIndex()+"");
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.onGoodsSlotClick(i);
            }
        });
    }
    public OnItemClickListenerr onItemClickListener;

    public void setOnItemClickListenerr(OnItemClickListenerr onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    /**
     * 删除购物车商品
     */
    public interface OnItemClickListenerr {
        void onGoodsSlotClick(int index);
    }
    @Override
    public int getItemCount() {
        return datas.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView index_text;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            index_text = itemView.findViewById(R.id.index_text);
        }
    }
}
