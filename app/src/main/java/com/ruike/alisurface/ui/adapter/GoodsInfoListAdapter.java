package com.ruike.alisurface.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ruike.alisurface.R;
import com.ruike.alisurface.utils.DensityUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 商品详情界面展示成分列表的适配器
 * Created by admin on 2019/11/25.
 */

public class GoodsInfoListAdapter extends RecyclerView.Adapter<GoodsInfoListAdapter.ViewHolder> {

    private Context context;
    private JSONArray templateContentArray; // 表头的集合
    private JSONArray tempstructureContentJarray; // 表信息的集合

    public GoodsInfoListAdapter(Context context, JSONArray tempstructureContentJarray, JSONArray templateContentArray) {
        this.context = context;
        this.tempstructureContentJarray = tempstructureContentJarray;
        this.templateContentArray = templateContentArray;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_goods_element, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            if (templateContentArray.length() > 0) {
                JSONObject jsonObject = tempstructureContentJarray.getJSONObject(position);

                for (int j = 0; j < templateContentArray.length(); j++) {
                    String optString = jsonObject.optString(templateContentArray.getJSONObject(j).optString("ColumnID"));

                    LinearLayout contentllayout = new LinearLayout(context);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
                    contentllayout.setLayoutParams(layoutParams);
                    contentllayout.setGravity(Gravity.CENTER);

                    TextView textView = new TextView(context);
                    textView.setText(optString);
                    textView.setTextColor(context.getResources().getColor(R.color.colorGoodsInfoDialogText));
                    textView.setTextSize(DensityUtil.px2sp(context, 34f));

                    contentllayout.addView(textView);
                    holder.contentLayout.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
                    holder.contentLayout.setGravity(Gravity.CENTER);
                    holder.contentLayout.addView(contentllayout);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (position == tempstructureContentJarray.length() - 1) {
            holder.linev.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return tempstructureContentJarray.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout contentLayout;
        View linev;

        public ViewHolder(View itemView) {
            super(itemView);
            contentLayout = itemView.findViewById(R.id.itemGoodsElement_content_llayout);
            linev = itemView.findViewById(R.id.itemGoodsElement_line_v);
        }

    }

}
