package com.ruike.alisurface.ui.adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ruike.alisurface.Constant;
import com.ruike.alisurface.R;
import com.ruike.alisurface.bean.GoodsBean;
import com.ruike.alisurface.utils.ShareKey;
import com.voodoo.lib_utils.AssetsUtils;
import com.voodoo.lib_utils.L;
import com.voodoo.lib_utils.ShareUtils;
import com.voodoo.lib_utils.imageLoader.ImageLoader;

import java.util.List;

public class GoodsListAdapter extends RecyclerView.Adapter<GoodsListAdapter.ViewHolder> {

    Context context;
    List<GoodsBean> list;

    private boolean isShowGoodsInfo = false; // 是否展示商品详情按钮和监听点击状态，默认不展示

    public GoodsListAdapter(Context context, List<GoodsBean> list) {
        this.context = context;
        this.list = list;
        isShowGoodsInfo = ShareUtils.getInstance().getBoolean(ShareKey.IS_SHOW_GOODS_INFO);
    }

    public void setData(List<GoodsBean> list) {
        isShowGoodsInfo = ShareUtils.getInstance().getBoolean(ShareKey.IS_SHOW_GOODS_INFO);
        this.list = list;
        notifyDataSetChanged();
    }

    public void setDoudong(int count, RecyclerView recyclerView) {
        View cardView = recyclerView.getChildAt(count);
        ImageView imgview = cardView.findViewById(R.id.itemGoodsList_goodsImage_imgv);
        ObjectAnimator animator = ObjectAnimator.ofFloat(imgview, "rotation",
                0, -25, 0, 20, 0, -15, 0, 10, 0, -5, 0, 5, 0);
        animator.setDuration(1000);
        //调整控件 相对位置为轴心 开始动画
        imgview.setPivotY(imgview.getHeight());
        imgview.setPivotX(imgview.getWidth() / 2);
        animator.start();
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                L.i("animator====onAnimationEnd");
                animation.cancel();
                animation = null;
            }
        });
        notifyItemChanged(count);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_goods_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        GoodsBean goodsBean = list.get(i);

        if (goodsBean.getProductimg() != null && !goodsBean.getProductimg().equals(viewHolder.goodsImageImgv.getTag(R.id.itemGoodsList_goodsImage_imgv))) {
            viewHolder.goodsImageImgv.setTag(R.id.itemGoodsList_goodsImage_imgv, goodsBean.getProductimg());
        }
        // 加载图片
        ImageLoader.loadImage(context, goodsBean.getProductimg(), viewHolder.goodsImageImgv);
        if (goodsBean.getCount() <= 0) {
            viewHolder.goodsClearImgv.setVisibility(View.VISIBLE);
        } else {
            viewHolder.goodsClearImgv.setVisibility(View.GONE);
        }
        viewHolder.goodsGoodsNameTv.setText(goodsBean.getProductname());
        viewHolder.goodsGoodsPriceTv.setText(new StringBuffer("￥ ").append(goodsBean.getPrice()));
        viewHolder.goodsGoodsIndexTv.setText(new StringBuffer("编号：").append(goodsBean.getIndex()));
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onGoodsItemClickListener != null) {
                    onGoodsItemClickListener.onGoodsClick(i, goodsBean);
                    onGoodsItemClickListener.onGoodsClick(viewHolder.goodsImageImgv, i, goodsBean);
                }
            }
        });

        if (isShowGoodsInfo) { // 展示商品详情
            viewHolder.goodsInfoRlayout.setVisibility(View.VISIBLE); // 占位隐藏商品详情按钮
            viewHolder.goodsInfoRlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemGoodsInfoClickListener != null) {
                        onItemGoodsInfoClickListener.onGoodsInfoClick(i, goodsBean);
                    }
                }
            });
            viewHolder.goodsGoodsIndexTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemGoodsInfoClickListener != null) {
                        onItemGoodsInfoClickListener.onGoodsInfoClick(i, goodsBean);
                    }
                }
            });
        } else { // 不展示商品详情的按钮
            viewHolder.goodsInfoRlayout.setVisibility(View.INVISIBLE); // 占位隐藏商品详情按钮
        }


    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView goodsImageImgv;
        ImageView goodsClearImgv;
        TextView goodsGoodsNameTv;
        TextView goodsGoodsPriceTv;
        TextView goodsGoodsIndexTv;
        RelativeLayout goodsInfoRlayout;

        public ViewHolder(View itemView) {
            super(itemView);
            goodsImageImgv = itemView.findViewById(R.id.itemGoodsList_goodsImage_imgv);
            goodsClearImgv = itemView.findViewById(R.id.itemGoodsList_clear_imgv);
            goodsGoodsNameTv = itemView.findViewById(R.id.itemGoodsList_goodsName_tv);
            goodsGoodsPriceTv = itemView.findViewById(R.id.itemGoodsList_goodsPrice_tv);
            goodsGoodsIndexTv = itemView.findViewById(R.id.itemGoodsList_goodsIndex_tv);
            goodsInfoRlayout = itemView.findViewById(R.id.itemGoodsList_goodsInfo_rlayout);
        }
    }

    public OnGoodsItemClickListener onGoodsItemClickListener;
    public OnItemGoodsInfoClickListener onItemGoodsInfoClickListener;

    public void setOnGoodsItemClickListener(OnGoodsItemClickListener onGoodsItemClickListener) {
        this.onGoodsItemClickListener = onGoodsItemClickListener;
    }

    public void setOnItemGoodsInfoClickListener(OnItemGoodsInfoClickListener onItemGoodsInfoClickListener) {
        this.onItemGoodsInfoClickListener = onItemGoodsInfoClickListener;
    }

    /**
     * 商品列表项点击监听
     */
    public interface OnGoodsItemClickListener {
        void onGoodsClick(int index, GoodsBean clickBean);

        void onGoodsClick(ImageView imageView, int index, GoodsBean clickBean);
    }

    /**
     * 显示商品详情的按钮点击监听
     */
    public interface OnItemGoodsInfoClickListener {
        void onGoodsInfoClick(int index, GoodsBean clickBean);
    }

}
