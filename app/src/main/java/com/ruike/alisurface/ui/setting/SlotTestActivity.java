package com.ruike.alisurface.ui.setting;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.ruike.alisurface.Constant;
import com.ruike.alisurface.R;
import com.ruike.alisurface.Serials.MsgEventBus;
import com.ruike.alisurface.Serials.outGoods.ShopChUtils;
import com.ruike.alisurface.base.BaseAty;
import com.ruike.alisurface.bean.GoodsBean;
import com.ruike.alisurface.http.MchApiHttp;
import com.ruike.alisurface.ui.adapter.Slot_test_Adapter;
import com.ruike.alisurface.ui.dialog.CustomDialog;
import com.ruike.alisurface.utils.MYUtiles;
import com.ruike.alisurface.utils.ThreadUtils;
import com.voodoo.lib_frame.view.AutofitTextView;
import com.voodoo.lib_utils.GsonUtils;
import com.voodoo.lib_utils.L;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import butterknife.BindView;

import static com.ruike.alisurface.utils.ShareKey.Type_Chshopzl;
import static com.ruike.alisurface.utils.ShareKey.Type_Chshopzl_OLD;

/**
 * 货道检测页面
 */
public class SlotTestActivity extends BaseAty {

    @BindView(R.id.goback_imgv)
    ImageView gobackImgv;
    @BindView(R.id.start_et)
    EditText startEt;
    @BindView(R.id.end_et)
    EditText endEt;
    @BindView(R.id.number_et)
    EditText numberEt;
    @BindView(R.id.make_sure_test)
    AutofitTextView makeSureTest;
    @BindView(R.id.Text_all)
    AutofitTextView TextAll;
    @BindView(R.id.slot_cycler)
    RecyclerView slotCycler;

    private ProgressDialog progressDialog;
    private Timer timer;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_slot_test;
    }

    @Override
    protected void initViews() {
        Constant.showShop = true;
        L.i("货道检测页==" + this.getClass().getName());
        initTitleBar(false);
    }

    @Override
    protected void initData() {
        showProgressDialog("加载数据中，请等待。。。");
        MchApiHttp.getSlots(this); // 获取商品列表数据
        init();
    }

    @Override
    public void doEventBusResult(MsgEventBus event) {

        if (event.getType().equals(Type_Chshopzl)) {
             removeProgressDialog();
            return;
        }

        if (event.getType().equals(Type_Chshopzl_OLD)) {
            removeProgressDialog();
            return;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Constant.showShop = false;
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            handler = null;
        }

        if (alertDialog != null) {
            alertDialog.dismiss();
        }
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

    }

    AlertDialog alertDialog;

    /**
     * 数据初始化
     */
    private void init() {

        gobackImgv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 10);
        slotCycler.setLayoutManager(gridLayoutManager);


        //输完测试的货道以及转动的次数，点击确定按钮
        makeSureTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    showProgressDialog("正在测试所选货道，请等待......", true);
//                    // 创建对话框构建器
//                    AlertDialog.Builder builder = new AlertDialog.Builder(SlotTestActivity.this);
//                    // 获取布局
//                    View dialogView = View.inflate(SlotTestActivity.this, R.layout.tip_dialog, null);
//                    ImageView dla_img = dialogView.findViewById(R.id.tip_img);
//                    try {
//                        ImageLoader.loadImage(SlotTestActivity.this, R.mipmap.img_loading, dla_img);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    // 创建对话框
//                    TextView title = new TextView(SlotTestActivity.this);
//                    title.setText("正在测试所选货道，请等待......");
//                    title.setGravity(Gravity.CENTER);
//                    title.setTextColor(Color.BLUE);
//                    title.setTextSize(30);
//                    title.setPadding(10, 10, 10, 10);
//                    builder.setCustomTitle(title);
//                    builder.setView(dialogView).setNegativeButton("", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                        }
//                    });
//                    alertDialog = builder.create();
//                    alertDialog.setCancelable(true);
//                    alertDialog.show();
                    TestSolt();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        //一键测试货道按钮
        TextAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TestAll();
            }
        });

    }


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    showRightTip("请输入开始货道");
                    break;
                case 1:
                    showRightTip("请输入结束货道");
                    break;
                case 2:
                    showRightTip("请输入要转动的次数");
                    break;
                case 3:
                    if (alertDialog != null) {
                        alertDialog.dismiss();
                    }
                    showRightTip("所有货道测试完毕");
                    break;
            }
        }
    };

    @Override
    public void onSuccess(String requestUrl, String requestJsonStr) {
        super.onSuccess(requestUrl, requestJsonStr);
        JSONObject object = null;
        try {
            object = new JSONObject(requestJsonStr);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String data = object.optString("data");
        if (requestUrl.endsWith("getSlots")) { // 获取商品货道信息
            L.i(requestJsonStr);
            dataBeanList.addAll(GsonUtils.jsonArray2ModelList(data, GoodsBean.class));

            SlotInfor();
            return;
        }
    }

    @Override
    public void onError(String requestUrl, String errorMsg) {
        super.onError(requestUrl, errorMsg);
        L.i(requestUrl + "===error===" + errorMsg);

    }

    List<GoodsBean> dataBeanList = new ArrayList<>();
    Slot_test_Adapter testAdapter;

    /**
     * 获取货道信息和货道类型
     */
    private void SlotInfor() {
        try {
            L.i("货道信息" + dataBeanList.size());
            if (dataBeanList.size() > 0) {
//                testAdapter = new Slot_test_Adapter(this,   dataBeanList);
                testAdapter = new Slot_test_Adapter(this, dataBeanList);
                slotCycler.setAdapter(testAdapter);
                testAdapter.notifyDataSetChanged();
            }

            testAdapter.setOnItemClickListenerr(new Slot_test_Adapter.OnItemClickListenerr() {
                @Override
                public void onGoodsSlotClick(int position) {
                    final int index = dataBeanList.get(position).getIndex();
                    showProgressDialog("正在测试第" + index + "货道，请稍后.....", true);
                    List<GoodsBean> dataBeanLists = new ArrayList<>();
                    dataBeanLists.add(dataBeanList.get(position));
                    PutGoods(dataBeanLists);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void PutGoods(List<GoodsBean> dataBeanList1) {
        try {
            //判断数据是否已经存在
            ThreadUtils.post(new Runnable() {
                @Override
                public void run() {

                    try {
                        for (int i = 0; i < dataBeanList1.size(); i++) {
                            int index = dataBeanList1.get(i).getIndex();
                            int type = dataBeanList1.get(i).getSlottypeid();
                            L.i("tjf==" + "run: 出货货道：" + index + "出货类型==" + type);
                            ShopChUtils.shopTodo(type, index);
                        }
                        if (progressDialog != null) {
                            progressDialog.dismiss();//dialog消失
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    int type;

    /**
     * 输入选择测试的货道
     */
    private void TestSolt() {
        try {
            String start = startEt.getText().toString();
            String end = endEt.getText().toString();
            String number = numberEt.getText().toString();
            if (start.equals("")) {
                handler.sendEmptyMessage(0);
                MYUtiles.HideKeyBoard(SlotTestActivity.this);
                return;
            }
            if (end.equals("")) {
                handler.sendEmptyMessage(1);
                MYUtiles.HideKeyBoard(SlotTestActivity.this);
                return;
            }
            if (number.equals("")) {
                handler.sendEmptyMessage(2);
                MYUtiles.HideKeyBoard(SlotTestActivity.this);
                return;
            }


            ThreadUtils.post(new Runnable() {
                public void run() {
                    int s = Integer.parseInt(start);
                    int e = Integer.parseInt(end) + 1;
                    int n = Integer.parseInt(number);
                    for (int i = s; i < e; i++) {
                        for (GoodsBean gdb : dataBeanList) {
                            if (gdb.getIndex() == i) {
                                type = gdb.getSlottypeid();
                                for (int j = 0; j < n; j++) {
                                    ShopChUtils.shopTodo(type, i);
                                }
                                if (i == e - 1) {
                                    handler.sendEmptyMessage(3);
                                    return;
                                }
                                break;
                            }
                        }
                    }
                }
            });
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
        }
    }

    /**
     * 一键测试所有货道
     */
    private void TestAll() {
        try {
            CustomDialog dialog = new CustomDialog.Builder(this)
                    .setTitle("信息提示！！！")
                    .setMessage("确定进行货道测试吗？")
                    .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            showProgressDialog("正在测试全部货道，请稍后.....");
                            try {
                                PutGoods(dataBeanList);
                                handler.sendEmptyMessage(3);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    })
                    .setNegativeButton("取消", null)
                    .create();
            dialog.show();
//            AlertDialog.Builder alg = new AlertDialog.Builder(SlotTestActivity.this);
//            alg.setIcon(R.drawable.icon_notice);
//            alg.setTitle("信息提示！！！")
//                    .setMessage("确定进行货道测试吗？")
//                    .setPositiveButton("确定", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, final int pos) {
//
//                            showProgressDialog("正在测试全部货道，请稍后.....", false);
//                            try {
//                                PutGoods(dataBeanList);
//                                handler.sendEmptyMessage(3);
//                            } catch (Exception ex) {
//                                ex.printStackTrace();
//                            }
//                            //hideProgressDialog();
//                        }
//                    })
//                    .setNegativeButton("取消", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//
//                        }
//                    });
//            alg.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public void showProgDialog(String str, boolean cancelable) {
//        progressDialog = new ProgressDialog(SlotTestActivity.this);
//        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        progressDialog.setTitle("信息提示！！");
//        progressDialog.setMessage(str);
//        progressDialog.setCancelable(cancelable);
//        progressDialog.setIndeterminate(false);
//        progressDialog.show();
//    }

}
