package com.ruike.alisurface.ui.mainPage;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.ruike.alisurface.BuildConfig;
import com.ruike.alisurface.Constant;
import com.ruike.alisurface.R;
import com.ruike.alisurface.Serials.MsgEventBus;
import com.ruike.alisurface.base.BaseAty;
import com.ruike.alisurface.http.MchApiHttp;
import com.ruike.alisurface.ui.setting.SettingActivity;
import com.ruike.alisurface.utils.BackMainUtils;
import com.ruike.alisurface.utils.DeviceUtils;
import com.ruike.alisurface.utils.ShareKey;
import com.tencent.rtmp.TXVodPlayer;
import com.tencent.rtmp.ui.TXCloudVideoView;
import com.voodoo.lib_utils.AssetsUtils;
import com.voodoo.lib_utils.L;
import com.voodoo.lib_utils.ShareUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Author: voodoo
 * CreateDate: 2020-03-29 029 下午 09:49
 * Description: 登录界面
 */
public class LoginActivity extends BaseAty {

    @BindView(R.id.login_video_txcvv)
    TXCloudVideoView loginVideoTxcvv;
    @BindView(R.id.login_versionName_tv)
    TextView loginVersionNameTv;
    @BindView(R.id.login_userName_et)
    EditText loginUserNameEt;
    @BindView(R.id.login_userPwd_et)
    EditText loginUserPwdEt;
    @BindView(R.id.login_sn_tv)
    TextView loginSnTv;
    private TXVodPlayer mVodPlayer;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initViews() {
        L.i("登录页==" + this.getClass().getName());
        startCountdownTimer(60, null, null);
    }

    @Override
    protected void initData() {
        loginVersionNameTv.setText(new StringBuffer(getResources().getString(R.string.versionNameStr)).append(BuildConfig.VERSION_NAME));
        loginSnTv.setText(new StringBuffer(getResources().getString(R.string.deviceSnStr)).append(DeviceUtils.getDeviceSn()));
        playVideo();
    }

    private void playVideo() {
        // 创建 player 对象
        mVodPlayer = new TXVodPlayer(this);
        //关键 player 对象与界面 view
        mVodPlayer.setPlayerView(loginVideoTxcvv);
        File file = AssetsUtils.getFile(this, "video_sunny.mp4");
        if (file != null && file.exists()) {
            mVodPlayer.startPlay(file.getAbsolutePath());
            mVodPlayer.setLoop(true);
        }
    }

    @OnClick({R.id.login_goback_imgv, R.id.login_login_btn})
    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.login_goback_imgv:
                 startCountdownTimer(0,null,null);
                break;
            case R.id.login_login_btn:
                String userNameStr = loginUserNameEt.getText().toString().trim();
                String userPwdStr = loginUserPwdEt.getText().toString().trim();
                if (userNameStr.isEmpty()) {
                    showErrorTip("请输入账号");
                    return;
                }
                if (userPwdStr.isEmpty()) {
                    showErrorTip("请输入密码");
                    return;
                }
                showProgressDialog("登录中，请等待。。。");
                MchApiHttp.loginByMch(userNameStr, userPwdStr, this);

                break;
        }
    }

    @Override
    public void onSuccess(String requestUrl, String requestJsonStr) {
        super.onSuccess(requestUrl, requestJsonStr);

        if (requestUrl.endsWith("LoginByMch")) {
            L.i(" lgoin==  onSuccess ", requestJsonStr);
            try {
                JSONObject object = new JSONObject(requestJsonStr);
                String msg = null;
                msg = object.getString("msg");
                int code = object.getInt("code");
                L.i("===msg==" + msg + "===code===" + code);
                if (code == 0) {
                    JSONObject data = object.optJSONObject("data");
                    String uid = data.optString("uid");
                    ShareUtils.getInstance().putString(ShareKey.UID, uid);
                    startActivity(SettingActivity.class, null);
                    finish();
                } else {
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onError(String requestUrl, String errorMsg) {
        super.onError(requestUrl, errorMsg);
        if (requestUrl.endsWith("LoginByMch")) {
            L.i(" lgoin==  onError ", errorMsg);
            showErrorTip(errorMsg);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // 暂停播放
        if (mVodPlayer != null) {
            mVodPlayer.pause();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        // 恢复播放
        if (mVodPlayer != null) {
            mVodPlayer.resume();
        }
    }

    @Override
    public void doEventBusResult(MsgEventBus event) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mVodPlayer.stopPlay(true); // true 代表清除最后一帧画面
        loginVideoTxcvv.onDestroy();
        loginVideoTxcvv = null;
    }
}
