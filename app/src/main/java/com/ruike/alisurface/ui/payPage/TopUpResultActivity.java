package com.ruike.alisurface.ui.payPage;

import android.os.Bundle;
import android.widget.TextView;

import com.ruike.alisurface.R;
import com.ruike.alisurface.Serials.MsgEventBus;
import com.ruike.alisurface.base.BaseAty;

import butterknife.BindView;

/**
 * Author: voodoo
 * CreateDate: 2020/07/17 017 09:57 上午
 * Description: 充值结果界面
 */
public class TopUpResultActivity extends BaseAty {

    @BindView(R.id.topupResult_countDown_tv)
    TextView topupResultCountDownTv;
    @BindView(R.id.topupResult_showMsg_tv)
    TextView topupResultShowMsgTv;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_topup_result;
    }

    @Override
    protected void initViews() {
        initTitleBar(false);
    }

    @Override
    protected void initData() {
        Bundle bundle = getIntent().getExtras();

        startCountdownTimer(20, topupResultCountDownTv, null);

        if (bundle != null) {
            String resultStr = bundle.getString("resultStr", "没有返回值");
            topupResultShowMsgTv.setText(resultStr);
        }

    }

    @Override
    public void doEventBusResult(MsgEventBus event) {

    }

}
