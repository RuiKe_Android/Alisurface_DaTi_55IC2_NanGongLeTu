package com.ruike.alisurface.ui.serialportTest;

import android.view.View;
import android.widget.EditText;

import com.ruike.alisurface.Constant;
import com.ruike.alisurface.R;
import com.ruike.alisurface.Serials.MsgEventBus;
import com.ruike.alisurface.Serials.SerialPortInstructUtils;
import com.ruike.alisurface.Serials.outGoods.ShengJiangJiUtils;
import com.ruike.alisurface.Serials.Ttys3Utils;
import com.ruike.alisurface.base.BaseAty;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Author: voodoo
 * CreateDate: 2020-03-26 026 下午 10:41
 * Description: 用于测试串口通讯
 */
public class TestSerialPortActivity extends BaseAty {

    @BindView(R.id.testSerialPort_cargoLaneNumber_et)
    EditText serialPortCargoLaneNumberEt;

    int cargoLane = 0; // 货道号

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_test_serial_port;
    }

    @Override
    protected void initViews() {
        Constant.showShop = true;
    }

    @Override
    protected void initData() {
    }

    @OnClick({R.id.testSerialPort_initShengjiangji_btn, R.id.testSerialPort_goback_llayout, R.id.testSerialPort_tpUp_btn, R.id.testSerialPort_jlmOpen_btn,
            R.id.testSerialPort_qhOpen_btn, R.id.testSerialPort_tpDown_btn, R.id.testSerialPort_jlmClose_btn,
            R.id.testSerialPort_qhClose_btn, R.id.testSerialPort_tpStop_btn, R.id.testSerialPort_jlmStop_btn,
            R.id.testSerialPort_qhStop_btn, R.id.testSerialPort_testCargoLane_btn, R.id.testSerialPort_outGoods_btn,
            R.id.testSerialPort_setProtectCurrent_btn, R.id.testSerialPort_queryCurrent_btn})
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.testSerialPort_testCargoLane_btn || view.getId() == R.id.testSerialPort_outGoods_btn) {
            String cargoLaneStr = serialPortCargoLaneNumberEt.getText().toString();
            if (cargoLaneStr.isEmpty() || cargoLaneStr.equals("0")) {
                return;
            }
            cargoLane = Integer.parseInt(cargoLaneStr);
        }
        String sendData = null;
        switch (view.getId()) {
            case R.id.testSerialPort_goback_llayout:
                finish();
                break;
            case R.id.testSerialPort_initShengjiangji_btn: // 初始化升降机
                ShengJiangJiUtils.init();
                break;
            case R.id.testSerialPort_tpUp_btn: // 托盘上升
                sendData = SerialPortInstructUtils.sjjControlMotor(1, true, 70, 500, false);
                break;
            case R.id.testSerialPort_tpDown_btn:  // 托盘下降
                sendData = SerialPortInstructUtils.sjjControlMotor(1, true, 70, 500, true);
                break;
            case R.id.testSerialPort_tpStop_btn: // 托盘停止
                sendData = SerialPortInstructUtils.sjjControlMotor(1, false, 70, 500, false);
                break;
            case R.id.testSerialPort_jlmOpen_btn: // 卷帘门开
                ShengJiangJiUtils.juanLianDoor(true, true);
//                sendData = SerialPortInstructUtils.sjjControlMotor(4, true, 60, 100, false);
                break;
            case R.id.testSerialPort_jlmClose_btn: // 卷帘门关
                ShengJiangJiUtils.juanLianDoor(false, true);
//                sendData = SerialPortInstructUtils.sjjControlMotor(4, true, 60, 100, true);
                break;
            case R.id.testSerialPort_jlmStop_btn: // 卷帘门停
                sendData = SerialPortInstructUtils.sjjControlMotor(4, false, 60, 100, false);
                break;
            case R.id.testSerialPort_qhOpen_btn: // 取货门开
                sendData = SerialPortInstructUtils.sjjControlMotor(5, true, 60, 500, true);
                break;
            case R.id.testSerialPort_qhClose_btn:  // 取货门关
                sendData = SerialPortInstructUtils.sjjControlMotor(5, true, 60, 500, false);
                break;
            case R.id.testSerialPort_qhStop_btn: // 取货门停
                sendData = SerialPortInstructUtils.sjjControlMotor(5, false, 60, 500, false);
                break;
            case R.id.testSerialPort_testCargoLane_btn: // 测试货道
                sendData = SerialPortInstructUtils.quDongOutGoods(Integer.parseInt(serialPortCargoLaneNumberEt.getText().toString()), 0);
                break;
            case R.id.testSerialPort_outGoods_btn: // 出货
                ShengJiangJiUtils.outGoods(Integer.parseInt(serialPortCargoLaneNumberEt.getText().toString()), 0);
                break;
            case R.id.testSerialPort_queryCurrent_btn: // 查询电流
                Ttys3Utils.getInstance().SendPort(SerialPortInstructUtils.queryCurrent());
                break;
            case R.id.testSerialPort_setProtectCurrent_btn: // 设置电流保护阈值
                Ttys3Utils.getInstance().SendPort(SerialPortInstructUtils.sjjSetProtectCurrent(50, 50, 50, 50, 50, 50));
                break;
        }
        if (sendData == null || sendData.isEmpty()) {
            return;
        }
        Ttys3Utils.getInstance().SendPort(sendData);
    }

    @Override
    public void doEventBusResult(MsgEventBus event) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Constant.showShop = false;
    }

}
