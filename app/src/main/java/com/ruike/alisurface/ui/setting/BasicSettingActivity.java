package com.ruike.alisurface.ui.setting;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import com.ruike.alisurface.Constant;
import com.ruike.alisurface.R;
import com.ruike.alisurface.Serials.MsgEventBus;
import com.ruike.alisurface.base.BaseAty;
import com.ruike.alisurface.utils.ShareKey;
import com.voodoo.lib_utils.ShareUtils;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Author: voodoo
 * CreateDate: 2020-04-16 016 下午 04:41
 * Description: 基础设置
 */
public class BasicSettingActivity extends BaseAty {

    @BindView(R.id.baseSetting_buyQuantity_et)
    EditText buyQuantityEt;
    @BindView(R.id.baseSetting_weichat_swch)
    Switch weichatSwch;
    @BindView(R.id.baseSetting_showAdv_swch)
    Switch showAdvSwch;
    @BindView(R.id.baseSetting_stopSell_swch)
    Switch stopSellSwch;
    @BindView(R.id.baseSetting_showGoodsInfo_swch)
    Switch showGoodsInfoSwch;
    @BindView(R.id.baseSetting_offlineRestart_swch)
    Switch offlineRestartSwch;
    @BindView(R.id.baseSetting_shop_type)
    Switch baseSettingShopType;
    @BindView(R.id.baseSetting_offlineRestart_et)
    EditText Restart_et;
    @BindView(R.id.baseSetting_offlineRestart_btn)
    Button Restart_btn;

    @BindView(R.id.baseSetting_imgAdvTime_et)
    EditText imgAdvTimeEt;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_setting_base;
    }

    @Override
    protected void initViews() {
        initTitleBar(false);
    }

    int IS_SHOP_TYPE;

    @Override
    protected void initData() {
        int purchaseCount = ShareUtils.getInstance().getInt(ShareKey.PURCHASE_COUNT, Constant.DefaultData.PURCHASE_COUNT); // 购买数量
        buyQuantityEt.setText(String.valueOf(purchaseCount));

        int isShowAdv = ShareUtils.getInstance().getInt(ShareKey.ADV_TYPE); // 展示界面广告
        showAdvSwch.setChecked(isShowAdv != Constant.ADV_TYPE_NONE); // 无广告为2，showAdvSwch状态为关

        imgAdvTimeEt.setText(String.valueOf(ShareUtils.getInstance().getLong(ShareKey.IMG_ADV_TIME_DELAY, 5000)));

        boolean isStopSelling = ShareUtils.getInstance().getBoolean(ShareKey.IS_STOP_SELLING); // 停止售卖
        stopSellSwch.setChecked(isStopSelling);
        boolean isOfflineRestartApp = ShareUtils.getInstance().getBoolean(ShareKey.IS_OFFLINE_RESTART_APP); // 断网重启app
        offlineRestartSwch.setChecked(isOfflineRestartApp);
//        boolean isShowGoodsInfo = ShareUtils.getInstance().getBoolean(ShareKey.IS_SHOW_GOODS_INFO); // 可展示商品详情
//        showGoodsInfoSwch.setChecked(isShowGoodsInfo);

//        IS_SHOP_TYPE = ShareUtils.getInstance().getInt(ShareKey.IS_SHOP_TYPE, 1); // 可商品类型展示
//        if (IS_SHOP_TYPE == 1) {
//            baseSettingShopType.setChecked(false);
//        } else {
//            baseSettingShopType.setChecked(true);
//        }
        int restCount = ShareUtils.getInstance().getInt(ShareKey.IS_OFFLINE_RESTART_APPCOUNT, 10);
        Restart_et.setText(String.valueOf(restCount));
//        boolean isOpenAlipayPay = ShareUtils.getInstance().getBoolean(ShareKey.IS_OPEN_ALIPAY_PAY, true); // 启用 支付宝刷脸支付
//        weichatSwch.setChecked(isOpenAlipayPay);
    }

    @OnClick({R.id.baseSetting_goback_imgv, R.id.baseSetting_buyQuantitySave_btn,
            R.id.baseSetting_weichat_swch, R.id.baseSetting_stopSell_swch,
            R.id.baseSetting_showGoodsInfo_swch, R.id.baseSetting_offlineRestart_swch,
            R.id.baseSetting_shop_type, R.id.baseSetting_showAdv_swch, R.id.baseSetting_offlineRestart_btn,
            R.id.baseSetting_saveImgAdvTime_btn})
    @Override
    public void onClick(View view) {
        super.onClick(view);
        // TODO: 2020/04/29 029 此处需要一个设置提交的接口，上传各个的设置状态
        switch (view.getId()) {
            case R.id.baseSetting_goback_imgv:
                finish();
                if (stopSellSwch.isChecked()) {
                    startActivity(StopSellingActivity.class, null);
                }
                break;
            case R.id.baseSetting_saveImgAdvTime_btn:
                boolean isSettingTime = true;
                String delayStr = imgAdvTimeEt.getText().toString().trim();
                if (TextUtils.isEmpty(delayStr)) {
                    isSettingTime = false;
                    delayStr = imgAdvTimeEt.getHint().toString().trim();
                }
                long delay = Long.parseLong(delayStr);
                if (delay <= 500) {
                    showErrorTip("请输入大于1000毫秒的值");
                    return;
                }
                ShareUtils.getInstance().putLong(ShareKey.IMG_ADV_TIME_DELAY, delay);
                showRightTip(isSettingTime ? getResources().getString(R.string.setSuccessStr) : "保存成功（使用的默认值5000ms）");
                break;
            case R.id.baseSetting_buyQuantitySave_btn:
                String buyQuantityStr = buyQuantityEt.getText().toString().trim();
                if (!buyQuantityStr.isEmpty()) {
                    try {
                        int buyQuantity = Integer.parseInt(buyQuantityStr);
                        if (buyQuantity > 0 && buyQuantity <= 3) {
                            ShareUtils.getInstance().putInt(ShareKey.PURCHASE_COUNT, buyQuantity); // 购买数量
                            showRightTip(getResources().getString(R.string.setSuccessStr));
                        } else { // 小于0 或者大于3
                            showErrorTip(getResources().getString(R.string.setBuyCountErrorStr));
                            buyQuantityEt.setText(buyQuantity <= 0 ? String.valueOf(1) : String.valueOf(3));
                        }
                    } catch (Exception e) {
                        showErrorTip(getResources().getString(R.string.onlyInputIntNumStr));
                    }
                } else {
                    showErrorTip(getResources().getString(R.string.pleaseInputBuyNumStr));
                }
                break;
            case R.id.baseSetting_offlineRestart_btn:
                String Restart_etyStr = Restart_et.getText().toString().trim();
                if (!Restart_etyStr.isEmpty()) {
                    try {
                        int buyQuantity = Integer.parseInt(Restart_etyStr);
                        if (buyQuantity > 0 && buyQuantity <= 100) {
                            ShareUtils.getInstance().putInt(ShareKey.IS_OFFLINE_RESTART_APPCOUNT, buyQuantity);
                            showRightTip(getResources().getString(R.string.setSuccessStr));
                        } else { // 小于0 或者大于3
                            showErrorTip(getResources().getString(R.string.setBuyCountErrorStr));
                            Restart_et.setText(buyQuantity <= 0 ? String.valueOf(1) : String.valueOf(10));
                        }
                    } catch (Exception e) {
                        showErrorTip("");
                    }
                } else {
                    showErrorTip("");
                }
                break;

            case R.id.baseSetting_showAdv_swch:
                ShareUtils.getInstance().putInt(ShareKey.ADV_TYPE, showAdvSwch.isChecked() ? Constant.ADV_TYPE_IMAGE : Constant.ADV_TYPE_NONE); // 选中的话为轮播图广告，关闭的话状态为2
                showRightTip(getResources().getString(R.string.setSuccessStr));
                break;
            case R.id.baseSetting_weichat_swch:
                ShareUtils.getInstance().putBoolean(ShareKey.IS_OPEN_ALIPAY_PAY, weichatSwch.isChecked()); // 启用微信支付宝支付
                showRightTip(getResources().getString(R.string.setSuccessStr));
                break;
            case R.id.baseSetting_stopSell_swch:
                ShareUtils.getInstance().putBoolean(ShareKey.IS_STOP_SELLING, stopSellSwch.isChecked()); // 停止售卖
                showRightTip(getResources().getString(R.string.setSuccessStr));
                break;
            case R.id.baseSetting_showGoodsInfo_swch:
                ShareUtils.getInstance().putBoolean(ShareKey.IS_SHOW_GOODS_INFO, showGoodsInfoSwch.isChecked()); // 可展示商品详情
                showRightTip(getResources().getString(R.string.setSuccessStr));
                break;
            case R.id.baseSetting_offlineRestart_swch:
                ShareUtils.getInstance().putBoolean(ShareKey.IS_OFFLINE_RESTART_APP, offlineRestartSwch.isChecked()); // 断网重启app
                showRightTip(getResources().getString(R.string.setSuccessStr));
                break;
            case R.id.baseSetting_shop_type:
                ShareUtils.getInstance().putInt(ShareKey.IS_SHOP_TYPE, IS_SHOP_TYPE == 1 ? 2 : 1); //  按货道展示：按商品类型展示
                showRightTip(getResources().getString(R.string.setSuccessStr));
                break;
        }
    }

    @Override
    public void doEventBusResult(MsgEventBus event) {

    }
}