package com.ruike.alisurface.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ruike.alisurface.R;

/**
 * Author: voodoo
 * CreateDate: 2020-05-19 019下午 01:11
 * Description: 自定义Dialog对话框
 * <p>基础弹窗提示直接使用此封装类即可，其他布局直接自写布局传入View
 */
public class CustomDialog extends Dialog {

    public CustomDialog(Context context) {
        super(context);
    }

    public CustomDialog(Context context, int theme) {
        super(context, theme);
    }

    public static class Builder {
        private Context context;
        private String title;
        private String messageStr;
        private String positiveButtonText;
        private String negativeButtonText;
        private View contentView;
        private boolean cancel;
        private OnClickListener positiveButtonClickListener;
        private OnClickListener negativeButtonClickListener;

        public Builder(Context context) {
            this.context = context;
        }

        /**
         * 设置对话框标题
         */
        public Builder setTitle(int title) {
            return setTitle((String) context.getText(title));
        }

        /**
         * 设置对话框标题
         */
        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        /**
         * 设置对话框消息
         */
        public Builder setMessage(int message) {
            return setMessage((String) context.getText(message));
        }

        /**
         * 设置对话框消息
         */
        public Builder setMessage(String message) {
            this.messageStr = message;
            return this;
        }

        /**
         * 设置自己的展示View
         */
        public Builder setContentView(View view) {
            this.contentView = view;
            return this;
        }

        /**
         * 设置OK按钮文字以及点击监听事件
         */
        public Builder setPositiveButton(int positiveButtonText, OnClickListener listener) {
            return setPositiveButton((String) context.getText(positiveButtonText), listener);
        }

        /**
         * 设置OK按钮文字以及点击监听事件
         */
        public Builder setPositiveButton(String positiveButtonText, OnClickListener listener) {
            this.positiveButtonText = positiveButtonText;
            this.positiveButtonClickListener = listener;
            return this;
        }

        /**
         * 设置NO按钮文字以及点击监听事件
         */
        public Builder setNegativeButton(int negativeButtonText, OnClickListener listener) {
            return setNegativeButton((String) context.getText(negativeButtonText), listener);
        }

        /**
         * 设置NO按钮文字以及点击监听事件
         */
        public Builder setNegativeButton(String negativeButtonText, OnClickListener listener) {
            this.negativeButtonText = negativeButtonText;
            this.negativeButtonClickListener = listener;
            return this;
        }

        /**
         * 设置点击弹窗窗体外侧是否关闭该弹窗
         *
         * @param cancel true：可以关闭  false：不可关闭
         */
        public Builder setTouchOutsideCloseDialog(boolean cancel) {
            this.cancel = cancel;
            return this;
        }

        /**
         * 创建Dialog并返回，让UI层决定什么时候显示和销毁
         */
        public CustomDialog create() {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // 初始化对话框的自定义主题
            final CustomDialog dialog = new CustomDialog(context, R.style.Dialog);
//            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            View layout = inflater.inflate(R.layout.view_dialog_baselayout, null);
            dialog.addContentView(layout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

            // 设置提示框标题
            TextView titleTv = layout.findViewById(R.id.baseDialog_title_tv);
            if (title == null || title.isEmpty()) {
                titleTv.setVisibility(View.GONE);
            } else {
                titleTv.setText(title);
            }

            // 设置确认按钮
            Button positiveButton = layout.findViewById(R.id.baseDialog_positiveButton_btn);
            if (positiveButtonText != null && !positiveButtonText.isEmpty()) {
                positiveButton.setText(positiveButtonText);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if (positiveButtonClickListener != null) {
                            positiveButtonClickListener.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
                        }
                        dialog.dismiss();
                    }
                });
            } else {
                // 如果没有确认按钮则设置按钮的visibility为GONE
                positiveButton.setVisibility(View.GONE);
            }

            // 设置取消按钮
            Button negativeButton = layout.findViewById(R.id.baseDialog_negativeButton_btn);
            if (negativeButtonText != null && !negativeButtonText.isEmpty()) {
                negativeButton.setText(negativeButtonText);
                negativeButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if (negativeButtonClickListener != null) {
                            negativeButtonClickListener.onClick(dialog, DialogInterface.BUTTON_NEGATIVE);
                        }
                        dialog.dismiss();
                    }
                });
            } else {
                // 如果没有取消按钮则设置按钮的visibility为GONE
                negativeButton.setVisibility(View.GONE);
            }

            // 如果俩按钮全部隐藏了，那就直接隐藏按钮的父布局
            if (positiveButton.getVisibility() == View.GONE && negativeButton.getVisibility() == View.GONE) {
                layout.findViewById(R.id.baseDialog_button_llayout).setVisibility(View.GONE);
            }

            // 设置内容消息
            if (messageStr != null) {
                ((TextView) layout.findViewById(R.id.baseDialog_message_tv)).setText(Html.fromHtml(messageStr));
            } else if (contentView != null) {
                // 如果没有设置提示消息，并且contentView不为空的话直接设置传入的View作为body
                LinearLayout contentLlayout = layout.findViewById(R.id.baseDialog_content_llayout);
                contentLlayout.removeAllViews();
                contentLlayout.addView(contentView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            } else {
                layout.findViewById(R.id.baseDialog_content_llayout).setVisibility(View.GONE);
            }

            dialog.setContentView(layout);
            dialog.setCanceledOnTouchOutside(cancel);
            return dialog;
        }
    }

}
