package com.ruike.alisurface.ui.mainPage;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.ImageView;

import com.ruike.alisurface.Constant;
import com.ruike.alisurface.R;
import com.ruike.alisurface.Serials.MsgEventBus;
import com.ruike.alisurface.base.BaseAty;
import com.ruike.alisurface.mqtt.MqttOperation;
import com.ruike.alisurface.utils.DeviceUtils;
import com.ruike.alisurface.utils.ZXingUtils;
import com.voodoo.lib_utils.GsonUtils;
import com.voodoo.lib_utils.imageLoader.ImageLoader;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;

/**
 * Author: voodoo
 * CreateDate: 2020-07-10 029 下午 03:47
 * Description: 绑定机器界面
 */
public class BindMachineActivity extends BaseAty {

    @BindView(R.id.bindMachine_bindUrlQrCode_imgv)
    ImageView bindMachineBindUrlQrCodeImgv;
    @BindView(R.id.bindMachine_snQrCode_imgv)
    ImageView bindMachineSnQrCodeImgv;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_bind_machine;
    }

    @Override
    protected void initViews() {

    }

    @Override
    protected void initData() {
        initTitleBar(true);
        ImageLoader.loadImage(this, createQrCode("http://m.shouhuojiyun.com"), bindMachineBindUrlQrCodeImgv);

        HashMap<String, String> map = new HashMap<>();
        map.put("version", "Android");
        map.put("sn", DeviceUtils.getDeviceSn());
        String jsonSN = GsonUtils.MapToJson(map);
        ImageLoader.loadImage(this, createQrCode(jsonSN), bindMachineSnQrCodeImgv);
        bindMachineSnQrCodeImgv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Map<String, Object> coflist = new HashMap<String, Object>();
//                coflist.put("sn", DeviceUtils.getDeviceSn());
//                String data = GsonUtils.StringMapToJson(coflist);
//                MqttOperation.publishMessage(Constant.Mqtt.BINDSUSCCESS, data);
            }
        });

    }

    @Override
    public void doEventBusResult(MsgEventBus event) {

    }

    /**
     * 创建二维码
     *
     * @param contentStr 二维码内容
     * @return Bitmap
     */
    private Bitmap createQrCode(String contentStr) {
        return ZXingUtils.createQRImage(contentStr, 200, 200, BitmapFactory.decodeResource(getResources(), 0));
    }

}
