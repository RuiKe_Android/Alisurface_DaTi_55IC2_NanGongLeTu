package com.ruike.alisurface.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ruike.alisurface.R;
import com.ruike.alisurface.bean.GoodsBean;
import com.voodoo.lib_frame.tip.ToastTip;
import com.voodoo.lib_utils.L;
import com.voodoo.lib_utils.imageLoader.ImageLoader;

import java.util.List;

public class Buhu_SlotsAdapter extends RecyclerView.Adapter<Buhu_SlotsAdapter.ViewHolder> {

    Context context;
    List<GoodsBean> list;

    public Buhu_SlotsAdapter(Context context, List<GoodsBean> list) {
        this.context = context;
        this.list = list;
    }

    public void setData(List<GoodsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_bh, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GoodsBean item = list.get(position);
        L.i("item=="+position);
        ImageLoader.loadImage(context, item.getProductimg(),holder. img_spicon);
        holder.tv_spname.setText(item.getProductname());
        holder.tv_slotid.setText("货道：" + item.getIndex() + "号");
        holder.tv_price.setText(item.getPrice() + "");
        holder.tv_store.setText(item.getCount() + "");


        holder.tvadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (item.getCount() < item.getMaxcount()) {
                    item.setCount((item.getCount() + 1));
                    list.set(position, item);
                    notifyDataSetChanged();
                } else {
                    ToastTip.makeText(context, "已到最大库存数量", ToastTip.LENGTH_LONG);
                }
            }
        });

        holder.tvsub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (item.getCount() > 0) {
                    item.setCount((item.getCount() - 1));
                    list.set(position, item);
                    notifyDataSetChanged();
                } else {
                    ToastTip.makeText(context, "库存数量已为零", ToastTip.LENGTH_LONG);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }


      class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_spname, tv_slotid, tv_price, tv_store ;
        ImageView img_spicon, tvadd, tvsub;
        public ViewHolder(View convertView) {
            super(convertView);

            img_spicon = convertView.findViewById(R.id.img_spicon);
            tv_spname = convertView.findViewById(R.id.tv_spname);
            tv_slotid = convertView.findViewById(R.id.tv_slotid);
            tv_price = convertView.findViewById(R.id.tv_price);
            tv_store = convertView.findViewById(R.id.tv_store);
            tvadd = convertView.findViewById(R.id.kc_add);
            tvsub = convertView.findViewById(R.id.kc_sub);
        }

    }



}
