package com.ruike.alisurface.mqtt;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.voodoo.lib_utils.L;

/**
 * Author: voodoo
 * CreateDate: 2020-03-27 027 上午 10:08
 * Description: Mqtt 网络状态广播监听服务
 */
public class MqttNetBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        try {
            Log.i("chaozi", "onReceive: 网络变化");
            // MQTTService.handler.sendEmptyMessage(-99);
        } catch (Exception e) {
            L.i("onReceive: " + e);
            e.printStackTrace();
        }

    }
}
