package com.ruike.alisurface.project_DaTi.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Author: voodoo
 * CreateDate: 2020/07/24 024 11:39 上午
 * Description: 题目作答详情
 */
public class AnswerQuestionBean implements Serializable {

    /**
     * mchid : 6368038234803670
     * open_id : osGjrs3KEOrPAKYow5x2jgL9peFw
     * answer_list : [{"question_index":1,"question_id":"1393041346952052736","submit_answer":[1,2,3]},{"question_index":2,"question_id":"1417662359429857280","submit_answer":[1,2,3,4]},{"question_index":3,"question_id":"1393021495164420096","submit_answer":[1,2,3,4]},{"question_index":4,"question_id":"1393045742247100416","submit_answer":[1,2,3,4]}]
     * start_time : 2021-10-25 10:02
     */
    String mchid;
    String open_id;
    String start_time;
    List<AnswerListBean> answer_list;

    public String getMchid() {
        return mchid;
    }

    public void setMchid(String mchid) {
        this.mchid = mchid;
    }

    public String getOpen_id() {
        return open_id;
    }

    public void setOpen_id(String open_id) {
        this.open_id = open_id;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public List<AnswerListBean> getAnswer_list() {
        return answer_list;
    }

    public void setAnswer_list(List<AnswerListBean> answer_list) {
        this.answer_list = answer_list;
    }

    public static class AnswerListBean {

        /**
         * question_index : 1
         * question_id : 1393041346952052736
         * submit_answer : [1,2,3]
         */
        int question_index;
        String question_id;
        List<Integer> submit_answer;

        public int getQuestion_index() {
            return question_index;
        }

        public void setQuestion_index(int question_index) {
            this.question_index = question_index;
        }

        public String getQuestion_id() {
            return question_id;
        }

        public void setQuestion_id(String question_id) {
            this.question_id = question_id;
        }

        public List<Integer> getSubmit_answer() {
            return submit_answer;
        }

        public void setSubmit_answer(List<Integer> submit_answer) {
            this.submit_answer = submit_answer;
        }

        @Override
        public String toString() {
            return "AnswerListBean{" +
                    "question_index=" + question_index +
                    ", question_id='" + question_id + '\'' +
                    ", submit_answer=" + submit_answer +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "AnswerQuestionBean{" +
                "mchid='" + mchid + '\'' +
                ", open_id='" + open_id + '\'' +
                ", start_time='" + start_time + '\'' +
                ", answer_list=" + answer_list +
                '}';
    }
}
