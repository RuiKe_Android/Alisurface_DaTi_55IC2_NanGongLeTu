package com.ruike.alisurface.project_DaTi;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.ruike.alisurface.R;

/**
 * 每题回答之后展示结果的Toast
 */
public class AnswerResultToast extends Toast {

    public AnswerResultToast(Context context) {
        super(context);
    }

    public static AnswerResultToast makeText(Context context, int duration) {
        AnswerResultToast resule = new AnswerResultToast(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.toast_answer_result, null);
        ImageView imgv = view.findViewById(R.id.answerResult_type_imgv);
        imgv.setImageResource(R.drawable.icon_right_tip);

        resule.setView(view);
        // 设置位置，此处为垂直居中
        resule.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        resule.setDuration(duration);

        return resule;
    }

    /**
     * 设置图标
     *
     * @param tipBitmap Bitmap
     */
    public void setIcon(int tipBitmap) {
        if (getView() == null) {
            throw new RuntimeException("Toast未被创建，请创建。。。");
        }
        ImageView iv = getView().findViewById(R.id.answerResult_type_imgv);
        if (iv == null) {
            throw new RuntimeException("ImageView未被创建，请创建。。。");
        }
        iv.setImageResource(tipBitmap);
    }

}
