package com.ruike.alisurface.project_DaTi.bean;

import java.util.List;

/**
 * Author: voodoo
 * CreateDate: 2020/07/22 022 09:39 上午
 * Description: 答题机问题对象
 */
public class QuestionBean {

    /**
     * question_index : 2
     * question_id : 1417662359429857280
     * question_content : 机动车驾驶人在实习期内可以单独驾驶大型客车。
     * question_type : 0
     * question_type_str : 单选
     * options : [{"option_content":"正确","option_content_en":null,"sort":1,"is_true":false},{"option_content":"错误","option_content_en":null,"sort":2,"is_true":true}]
     * answer_value : 2
     */

    int question_index; // 题目序号
    String question_id; // 题目ID
    String question_content; // 题干
    int question_type; // 题类型，单选、多选
    String question_type_str; // 题类型描述：“单选”、“多选”
    String answer_value; // 一个正确答案的集合
    List<OptionsBean> options; // 选项

    public int getQuestion_index() {
        return question_index;
    }

    public void setQuestion_index(int question_index) {
        this.question_index = question_index;
    }

    public String getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }

    public String getQuestion_content() {
        return question_content;
    }

    public void setQuestion_content(String question_content) {
        this.question_content = question_content;
    }

    public int getQuestion_type() {
        return question_type;
    }

    public void setQuestion_type(int question_type) {
        this.question_type = question_type;
    }

    public String getQuestion_type_str() {
        return question_type_str;
    }

    public void setQuestion_type_str(String question_type_str) {
        this.question_type_str = question_type_str;
    }

    public String getAnswer_value() {
        return answer_value;
    }

    public void setAnswer_value(String answer_value) {
        this.answer_value = answer_value;
    }

    public List<OptionsBean> getOptions() {
        return options;
    }

    public void setOptions(List<OptionsBean> options) {
        this.options = options;
    }

    public static class OptionsBean {

        /**
         * option_content : 正确
         * sort : 1
         * is_true : false
         */
        String option_content; // 选项内容
        int sort; // 排序编号
        boolean is_true; // 此选项是否正确
        boolean isSelect; // 本地添加 -- 是否选中

        public String getOption_content() {
            return option_content;
        }

        public void setOption_content(String option_content) {
            this.option_content = option_content;
        }

        public int getSort() {
            return sort;
        }

        public void setSort(int sort) {
            this.sort = sort;
        }

        public boolean isIs_true() {
            return is_true;
        }

        public void setIs_true(boolean is_true) {
            this.is_true = is_true;
        }

        public boolean isSelect() {
            return isSelect;
        }

        public void setSelect(boolean select) {
            isSelect = select;
        }

        @Override
        public String toString() {
            return "OptionsBean{" +
                    "option_content='" + option_content + '\'' +
                    ", sort=" + sort +
                    ", is_true=" + is_true +
                    ", isSelect=" + isSelect +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "QuestionBean{" +
                "question_index=" + question_index +
                ", question_id='" + question_id + '\'' +
                ", question_content='" + question_content + '\'' +
                ", question_type=" + question_type +
                ", question_type_str='" + question_type_str + '\'' +
                ", answer_value='" + answer_value + '\'' +
                ", options=" + options +
                '}';
    }
}
