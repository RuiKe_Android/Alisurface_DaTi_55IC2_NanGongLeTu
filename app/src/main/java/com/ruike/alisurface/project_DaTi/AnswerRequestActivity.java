package com.ruike.alisurface.project_DaTi;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.ruike.alisurface.R;
import com.ruike.alisurface.Serials.MsgEventBus;
import com.ruike.alisurface.base.BaseAty;
import com.ruike.alisurface.bean.CardOrderBean;
import com.ruike.alisurface.bean.ShopDetailBean;
import com.ruike.alisurface.project_DaTi.bean.ResultAnswerBean;
import com.ruike.alisurface.ui.payPage.PayShowShopActivity;
import com.voodoo.lib_utils.GsonUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Author: voodoo
 * CreateDate: 2020/07/23 023 09:16 上午
 * Description: 答题结果提示
 */
public class AnswerRequestActivity extends BaseAty {

    @BindView(R.id.answerResult_number_tv)
    TextView numberTv; // 一共多少道题，答对多少道答错多少道 （您共答 4 题，正确 3 题，错误 1 题）
    @BindView(R.id.answerResult_result_tv)
    TextView resultTv; // 结果展示 （未/达到领取要求）
    @BindView(R.id.answerResult_next_tv)
    TextView nextTv; // 按钮 （领取商品/错题详情）

    boolean isOutGoods = false; // 是否可以出货（是否达到出货要求）
    private ResultAnswerBean resultAnswerBean;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_answer_results;
    }

    @Override
    protected void initViews() {
        initTitleBar(false);
        startCountdownTimer(30, null, null);
    }

    @Override
    protected void initData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            resultAnswerBean = (ResultAnswerBean) bundle.getSerializable("resultAnswer");
        }

        if (resultAnswerBean != null) {

            isOutGoods = resultAnswerBean.isCan_out();

            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("您共答 ")
                    .append(resultAnswerBean.getTotal_question())
                    .append(" 题，正确 ")
                    .append(resultAnswerBean.getSuccess_question())
                    .append(" 题，错误 ")
                    .append(resultAnswerBean.getError_question())
                    .append(" 题");
            numberTv.setText(stringBuffer.toString());
            resultTv.setText(isOutGoods ? "达到出货要求" : "未达到出货要求");
            nextTv.setText(isOutGoods ? "领取商品" : "返回主界面");
        }
    }

    @Override
    public void doEventBusResult(MsgEventBus event) {

    }

    @OnClick(R.id.answerResult_next_tv)
    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.answerResult_next_tv:
                if (isOutGoods) {

                    ShopDetailBean shopDetailBean = new ShopDetailBean(resultAnswerBean.getOrder_id());
                    shopDetailBean.setTotal_fee(0);
                    shopDetailBean.setPay_fee(0);
                    shopDetailBean.setPromotion_fee(0);
                    shopDetailBean.setDiscount_fee(0);

                    List<ShopDetailBean.OrderDetailsBean> shopOrderDetailsBeanList = new ArrayList<>();
                    shopOrderDetailsBeanList.clear();

                    ShopDetailBean.OrderDetailsBean shopOrderDetailsBean = new ShopDetailBean.OrderDetailsBean();
                    shopOrderDetailsBean.setSlotTypeId(resultAnswerBean.getSlotTypeId()); // 货道类型
                    shopOrderDetailsBean.setSlotIndex(resultAnswerBean.getOut_slot()); // 出货货道编号
                    shopOrderDetailsBean.setQuantity(1); // 出货数量
                    shopOrderDetailsBeanList.add(shopOrderDetailsBean);

                    shopDetailBean.setDetailOrderList(shopOrderDetailsBeanList);

                    Bundle bundle = new Bundle();
                    bundle.putString("pay_type", "ansertQuestion");
                    bundle.putSerializable("orderrBeanData", shopDetailBean);
                    startActivity(PayShowShopActivity.class, bundle);
                    finish();
                } else {
                    startCountdownTimer(0, null, null);
//                    Bundle bundle = new Bundle();
//                    bundle.putSerializable("resultAnswer", resultAnswerBean);
//                    startActivity(AnswerErrorInfoActivity.class, bundle);
                    finish();
                }
                break;
        }
    }
}
