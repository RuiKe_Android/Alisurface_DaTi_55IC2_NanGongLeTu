package com.ruike.alisurface.project_DaTi.http;

import com.ruike.alisurface.Constant;
import com.ruike.alisurface.project_DaTi.bean.AnswerQuestionBean;
import com.ruike.alisurface.utils.ShareKey;
import com.voodoo.lib_frame.base.BaseView;
import com.voodoo.lib_frame.httpTools.ApiTools;
import com.voodoo.lib_frame.httpTools.RequestParams;
import com.voodoo.lib_utils.GsonUtils;
import com.voodoo.lib_utils.ShareUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Author: voodoo
 * CreateDate: 2020/07/23 023 01:53 下午
 * Description: 答题的网络请求操作类
 */
public class QuestionHttpTools {

    /**
     * 根据策略获取题目
     *
     * @param apiListener 回调
     */
    public static void getQuestion(BaseView apiListener) {
        ApiTools apiTools = new ApiTools();
        JSONObject params = new JSONObject();
        try {
            params.put("mch_id", ShareUtils.getInstance().getString(ShareKey.MCH_ID));
            params.put("open_id", ShareUtils.getInstance().getString(ShareKey.QUESTION_UID));
            apiTools.postJsonRequest(Constant.HttpUrl.QUESTION_URL + "get_question", params.toString(), apiListener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 提交答题
     *
     * @param answerQuestionBean 要提交的答案对象
     * @param apiListener        回调
     */
    public static void submitAnswer(AnswerQuestionBean answerQuestionBean, BaseView apiListener) {
        ApiTools apiTools = new ApiTools();
        apiTools.postJsonRequest(Constant.HttpUrl.QUESTION_URL + "submit_answer", GsonUtils.model2Json(answerQuestionBean), apiListener);
    }

    /**
     * 确认订单出货状态
     *
     * @param order_id    订单号
     * @param isEnd       是否出货完成
     * @param outParams   出货详情
     * @param apiListener 回调
     */
    public static void confirmQuestionRrder(String order_id, boolean isEnd, String outParams, BaseView apiListener) {
        ApiTools apiTools = new ApiTools();
        RequestParams params = new RequestParams();
        params.put("order_id", order_id);
        params.put("is_end", isEnd);
        params.put("out_params", outParams);
        apiTools.getRequest(Constant.HttpUrl.QUESTION_URL + "confirm_question_order", params, apiListener);
    }

}
