package com.ruike.alisurface.project_DaTi.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ruike.alisurface.R;
import com.ruike.alisurface.project_DaTi.bean.QuestionBean;
import com.voodoo.lib_utils.L;

import java.util.List;

/**
 * Author: voodoo
 * CreateDate: 2020/07/22 022 10:06 上午
 * Description: 答题机题目中选项适配器
 */
public class QuestionOptionListAdapter extends RecyclerView.Adapter<QuestionOptionListAdapter.ViewHolder> {

    Context context;
    List<QuestionBean.OptionsBean> list;
    boolean isSingle;  // 是否是单选
    ViewHolder beforeViewHolder = null; // 前一次点击的列表项
    QuestionBean.OptionsBean beforeOptionsBean = null; // 上一次操作的列表项

    public QuestionOptionListAdapter(Context context, List<QuestionBean.OptionsBean> list, boolean isSingle) {
        this.context = context;
        this.list = list;
        this.isSingle = isSingle;
    }

    /**
     * 设置选项列表并更新展示
     *
     * @param list     选项列表
     * @param isSingle 设置是否是单选
     */
    public void setList(List<QuestionBean.OptionsBean> list, boolean isSingle) {
        beforeViewHolder = null; // 设置列表的时候就已经换题了
        beforeOptionsBean = null;
        this.list = list;
        this.isSingle = isSingle;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_question_option, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        QuestionBean.OptionsBean optionsBean = list.get(i);

        viewHolder.optionShowTv.setText(optionsBean.getOption_content());

        boolean isSelected = optionsBean.isSelect();
        viewHolder.optionRootLlayout.setSelected(isSelected);

        viewHolder.itemView.setOnClickListener(v -> {
            setSelected(i, optionsBean, viewHolder);
        });

    }

    /**
     * 列表项点击选中状态设置
     *
     * @param clickIndex  点击的列表下标
     * @param optionsBean 点击的列表项对象
     * @param viewHolder  点击的列表项的ViewHolder
     */
    private void setSelected(int clickIndex, QuestionBean.OptionsBean optionsBean, ViewHolder viewHolder) {
        L.i("点击的下标：" + clickIndex, "点击的选项：" + optionsBean.toString());
        if (beforeViewHolder != null) { // 非第一次选择项
            boolean selected = viewHolder.optionRootLlayout.isSelected();
            if (beforeViewHolder == viewHolder) { // 如果本次选择的和上次一样， 那么更改选中状态
                optionsBean.setSelect(!selected);
                viewHolder.optionRootLlayout.setSelected(!selected);
                viewHolder.optionShowTv.setSelected(!selected);
                if (onItemSelectedListener != null) {
                    onItemSelectedListener.onSelectChange(clickIndex, !selected, String.valueOf(optionsBean.getSort()));
                }
            } else { // 本次选择和上次不一样
                if (isSingle) { // 单选
                    // 清除上次选择的项的状态
                    beforeOptionsBean.setSelect(false);
                    beforeViewHolder.optionRootLlayout.setSelected(false);
                    beforeViewHolder.optionShowTv.setSelected(false);
                    if (onItemSelectedListener != null) {
                        onItemSelectedListener.onSelectChange(clickIndex, false, String.valueOf(optionsBean.getSort()));
                    }
                }
                optionsBean.setSelect(!selected);
                viewHolder.optionRootLlayout.setSelected(!selected);
                viewHolder.optionShowTv.setSelected(!selected);
                if (onItemSelectedListener != null) {
                    onItemSelectedListener.onSelectChange(clickIndex, !selected, String.valueOf(optionsBean.getSort()));
                }
            }
        } else { // 第一次选择 直接选中选项
            optionsBean.setSelect(true);
            viewHolder.optionRootLlayout.setSelected(true);
            viewHolder.optionShowTv.setSelected(true);
            if (onItemSelectedListener != null) {
                onItemSelectedListener.onSelectChange(clickIndex, true, String.valueOf(optionsBean.getSort()));
            }
        }
        beforeViewHolder = viewHolder; // 将本次点击的列表项ViewHolder临时保存起来
        beforeOptionsBean = optionsBean; // 将本次点击的列表项的对象保存起来
    }

    public OnItemSelectedListener onItemSelectedListener;

    public void setOnItemSelectedListener(OnItemSelectedListener onItemSelectedListener) {
        this.onItemSelectedListener = onItemSelectedListener;
    }

    public interface OnItemSelectedListener {
        void onSelectChange(int clickIndex, boolean isSelected, String sortStr);
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout optionRootLlayout;
        TextView optionShowTv;

        public ViewHolder(View itemView) {
            super(itemView);

            optionRootLlayout = itemView.findViewById(R.id.itemQuestionOption_optionRoot_llayout);
            optionShowTv = itemView.findViewById(R.id.itemQuestionOption_optionShow_tv);

        }

    }

}
