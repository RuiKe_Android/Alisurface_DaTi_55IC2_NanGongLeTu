package com.ruike.alisurface.project_DaTi.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Author: voodoo
 * CreateDate: 2020/07/24 024 01:50 下午
 * Description: 答题详情回传对象
 */
public class ResultAnswerBean implements Serializable {

    /**
     * can_out : false
     * total_question : 4
     * success_question : 2
     * error_question : 2
     * details : [{"question_id":"1285474302816305152","question_content":"题目啦啦啦啦啦","question_index":1,"is_right":false,"answer_content":[{"sort":1,"option_content":"选项A"},{"sort":3,"option_content":"选项C"}]},{"question_id":"1285804618193977344","question_content":"一二三四五六七八九十一二三四五六七八九十一二三四五六七八九十一二三四五六七八九十一二三四五六七八九十一二三四五六七八九十一二三四五六七八九十一二三四五六七八九十一二三四五六七八九十一二三四五六七八九十","question_index":2,"is_right":false,"answer_content":[{"sort":1,"option_content":"111"},{"sort":2,"option_content":"222"},{"sort":3,"option_content":"333"},{"sort":4,"option_content":"444"},{"sort":5,"option_content":"555"},{"sort":6,"option_content":"666"},{"sort":7,"option_content":"777"},{"sort":8,"option_content":"888"},{"sort":9,"option_content":"999"},{"sort":10,"option_content":"000"}]},{"question_id":"1285474054878412800","question_content":"题目","question_index":3,"is_right":true,"answer_content":[{"sort":1,"option_content":"选项A"},{"sort":2,"option_content":"选项B"}]},{"question_id":"1285474054878412800","question_content":"题目","question_index":4,"is_right":true,"answer_content":[{"sort":1,"option_content":"选项A"},{"sort":2,"option_content":"选项B"}]}]
     */

    private boolean can_out;
    private int total_question;
    private int success_question;
    private int error_question;
    private List<DetailsBean> details;

    private String order_id; // 订单号，只有答题成功才返回该字段
    private int out_slot; // 出货货道，只有答题成功才返回该字段
    private int slotTypeId; // 货道类型，只有答题成功才返回该字段

    public boolean isCan_out() {
        return can_out;
    }

    public void setCan_out(boolean can_out) {
        this.can_out = can_out;
    }

    public int getTotal_question() {
        return total_question;
    }

    public void setTotal_question(int total_question) {
        this.total_question = total_question;
    }

    public int getSuccess_question() {
        return success_question;
    }

    public void setSuccess_question(int success_question) {
        this.success_question = success_question;
    }

    public int getError_question() {
        return error_question;
    }

    public void setError_question(int error_question) {
        this.error_question = error_question;
    }

    public List<DetailsBean> getDetails() {
        return details;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public int getOut_slot() {
        return out_slot;
    }

    public void setOut_slot(int out_slot) {
        this.out_slot = out_slot;
    }

    public int getSlotTypeId() {
        return slotTypeId;
    }

    public void setSlotTypeId(int slotTypeId) {
        this.slotTypeId = slotTypeId;
    }

    public void setDetails(List<DetailsBean> details) {
        this.details = details;
    }

    public static class DetailsBean implements Serializable {
        /**
         * question_id : 1285474302816305152
         * question_content : 题目啦啦啦啦啦
         * question_index : 1
         * is_right : false
         * answer_content : [{"sort":1,"option_content":"选项A"},{"sort":3,"option_content":"选项C"}]
         */

        private String question_id;
        private String question_content;
        private int question_index;
        private boolean is_right;
        private List<AnswerContentBean> answer_content;

        public String getQuestion_id() {
            return question_id;
        }

        public void setQuestion_id(String question_id) {
            this.question_id = question_id;
        }

        public String getQuestion_content() {
            return question_content;
        }

        public void setQuestion_content(String question_content) {
            this.question_content = question_content;
        }

        public int getQuestion_index() {
            return question_index;
        }

        public void setQuestion_index(int question_index) {
            this.question_index = question_index;
        }

        public boolean isIs_right() {
            return is_right;
        }

        public void setIs_right(boolean is_right) {
            this.is_right = is_right;
        }

        public List<AnswerContentBean> getAnswer_content() {
            return answer_content;
        }

        public void setAnswer_content(List<AnswerContentBean> answer_content) {
            this.answer_content = answer_content;
        }

        public static class AnswerContentBean implements Serializable {
            /**
             * sort : 1
             * option_content : 选项A
             */

            private int sort;
            private String option_content;

            public int getSort() {
                return sort;
            }

            public void setSort(int sort) {
                this.sort = sort;
            }

            public String getOption_content() {
                return option_content;
            }

            public void setOption_content(String option_content) {
                this.option_content = option_content;
            }
        }
    }
}
