package com.ruike.alisurface.project_DaTi.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ruike.alisurface.R;
import com.ruike.alisurface.project_DaTi.bean.QuestionBean;
import com.ruike.alisurface.project_DaTi.bean.ResultAnswerBean;
import com.voodoo.lib_utils.NumberFormatUtil;

import java.util.List;

/**
 * Author: voodoo
 * CreateDate: 2020/07/22 022 10:06 上午
 * Description: 错题展示列表适配器
 */
public class QuestionErrorInfoAdapter extends RecyclerView.Adapter<QuestionErrorInfoAdapter.ViewHolder> {

    Context context;
    List<ResultAnswerBean.DetailsBean> list;
    ViewGroup parentView;

    public QuestionErrorInfoAdapter(Context context, List<ResultAnswerBean.DetailsBean> list) {
        this.context = context;
        this.list = list;
    }

    public void setList(List<ResultAnswerBean.DetailsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        parentView = viewGroup;
        View view = LayoutInflater.from(context).inflate(R.layout.item_error_question, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        ResultAnswerBean.DetailsBean bean = list.get(i);

        viewHolder.numberTv.setText(new StringBuffer("第").append(NumberFormatUtil.formatInteger(bean.getQuestion_index())).append("题"));
        viewHolder.contentTv.setText(new StringBuffer("\u0003\u0003\u0003\u0003\u0003\u0003").append(bean.getQuestion_content()));

        List<ResultAnswerBean.DetailsBean.AnswerContentBean> answerContentBeans = bean.getAnswer_content();
        viewHolder.answerContentLlayout.removeAllViews();
        for (ResultAnswerBean.DetailsBean.AnswerContentBean answerContentBean : answerContentBeans) {
            TextView textView = new TextView(context);

            // 设置TextView展示的格式： A   选项A的内容
            textView.setText(new StringBuffer(String.valueOf((char) (64 + answerContentBean.getSort()))).append("\u0003\u0003").append(answerContentBean.getOption_content()));
            textView.setTextSize(25);
            textView.setMaxLines(1);
            textView.setEllipsize(TextUtils.TruncateAt.END);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 5, 0, 5);
            textView.setLayoutParams(layoutParams);
            viewHolder.answerContentLlayout.addView(textView);
        }

    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView numberTv;
        TextView contentTv;
        LinearLayout answerContentLlayout;

        public ViewHolder(View itemView) {
            super(itemView);

            numberTv = itemView.findViewById(R.id.itemErrorQuestion_number_tv);
            contentTv = itemView.findViewById(R.id.itemErrorQuestion_content_tv);
            answerContentLlayout = itemView.findViewById(R.id.itemErrorQuestion_answerContent_llayout);

        }

    }

}
