package com.voodoo.lib_databases.exception;

/**
 * 创建者：voodoo_jie
 * <br>创建时间：2019/12/02 002 下午 09:26:35
 * <br>功能描述：Afinal异常信息
 */
public class AfinalException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public AfinalException() {
        super();
    }

    public AfinalException(String msg) {
        super(msg);
    }

    public AfinalException(Throwable ex) {
        super(ex);
    }

    public AfinalException(String msg, Throwable ex) {
        super(msg,ex);
    }

}
