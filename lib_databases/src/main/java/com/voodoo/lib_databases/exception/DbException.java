package com.voodoo.lib_databases.exception;

/**
 * 创建者：voodoo_jie
 * <br>创建时间：2019/12/02 002 下午 09:25:21
 * <br>功能描述：数据库异常
 */
public class DbException extends AfinalException {

    private static final long serialVersionUID = 1L;

    public DbException() {}

    public DbException(String msg) {
        super(msg);
    }

    public DbException(Throwable ex) {
        super(ex);
    }

    public DbException(String msg, Throwable ex) {
        super(msg,ex);
    }

}
