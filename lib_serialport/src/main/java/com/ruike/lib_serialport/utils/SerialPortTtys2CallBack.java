package com.ruike.lib_serialport.utils;

import com.ruike.lib_serialport.callback.SerialCallBack;

/**
 * 串口2回调类
 */
public class SerialPortTtys2CallBack {

    private static SerialCallBack mCallBack;

    public static void setCallBack(SerialCallBack callBack) {
        mCallBack = callBack;
    }

    public static void onSerialPortOpenListener(boolean isOpenSuccess, String portName, String msg) {
        mCallBack.onSerialPortOpenListener(isOpenSuccess, portName, msg);
    }

    public static void doCallBackMethod(String info, byte[] resultBytes) {
        mCallBack.onSerialPortData(info, resultBytes);
    }

}