package com.ruike.lib_serialport.callback;

/**
 * @Params: 一个串口数据回调接口
 */
public interface SerialCallBack {

    /**
     * 开启串口结果监听回调
     *
     * @param isOpenSuccess 是否打开成功
     * @param portName      串口地址名称
     * @param msg           回传的消息
     */
    void onSerialPortOpenListener(boolean isOpenSuccess, String portName, String msg);

    /**
     * 串口数据回传回调
     *
     * @param serialPortData String的回传方式
     * @param resultBytes    回传的byte数组，数据和serialPortData一致，只是另一种数据格式，按需选择解析
     */
    void onSerialPortData(String serialPortData, byte[] resultBytes);

}