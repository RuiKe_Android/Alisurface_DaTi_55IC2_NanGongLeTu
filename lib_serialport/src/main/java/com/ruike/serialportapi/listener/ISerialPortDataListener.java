package com.ruike.serialportapi.listener;

/**
 * @author F1ReKing
 * @date 2019/11/1 10:58
 * @Description 串口消息监听
 */
public interface ISerialPortDataListener {

    /**
     * 回传数据
     *
     * @param bytes 回传的byte数组
     */
    void onDataReceived(byte[] bytes);

    /**
     * 发送的数据
     *
     * @param bytes 发送的byte数组
     */
    void onDataSend(byte[] bytes);

}
