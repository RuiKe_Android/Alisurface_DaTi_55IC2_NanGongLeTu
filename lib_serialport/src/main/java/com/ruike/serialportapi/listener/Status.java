package com.ruike.serialportapi.listener;

/**
 * @author HuangYH
 * @date 2019/12/2 18:17
 * @Description
 */
public enum Status {
    NO_READ_WRITE_PERMISSION,
    OPEN_FAIL
}
