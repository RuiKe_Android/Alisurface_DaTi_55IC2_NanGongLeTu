package com.ruike.serialportapi.listener;

import java.io.File;

/**
 * @author F1ReKing
 * @date 2019/11/1 10:49
 * @Description 串口打开状态监听
 */
public interface IOpenSerialPortListener {

    /**
     * 串口打开成功
     */
    void onSuccess(File device);

    /**
     * 串口打开失败
     */
    void onFail(File device, Status status);
}
