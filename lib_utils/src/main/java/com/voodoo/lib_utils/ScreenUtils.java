package com.voodoo.lib_utils;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

/**
 * Author: voodoo
 * CreateDate: 2020-03-25 025 下午 10:10
 * Description: 屏幕相关工具类
 */
public class ScreenUtils {

    /**
     * 获取屏幕宽
     *
     * @return int数组  int[0]：屏幕宽度值/像素  int[1]：屏幕宽度值/dpi
     */
    public static int[] getScreenWidth(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;         // 屏幕宽度（像素）
        float density = dm.density;         // 屏幕密度（0.75 / 1.0 / 1.5）
        int screenWidth = (int) (width / density);  // 屏幕宽度(dp)

        int[] widths = new int[2];
        widths[0] = width;
        widths[1] = screenWidth;
        return widths;
    }

    /**
     * 获取屏幕高
     *
     * @return int数组  int[0]：屏幕高度值/像素  int[1]：屏幕高度值/dpi
     */
    public static int[] getScreenHeight(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);
        int height = dm.heightPixels; // 屏幕高度（像素）
        float density = dm.density; // 屏幕密度（0.75 / 1.0 / 1.5）
        int screenHeight = (int) (height / density); // 屏幕高度(dp)

        int[] heidhts = new int[2];
        heidhts[0] = height;
        heidhts[1] = screenHeight;
        return heidhts;
    }

    /**
     * 获取屏幕密度
     *
     * @return int数组  int[0]：屏幕密度值  int[1]：屏幕密度值/dpi
     */
    public static float[] getScreenDensitys(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);
        float density = dm.density; // 屏幕密度（0.75 / 1.0 / 1.5）
        float densityDpi = dm.densityDpi; // 屏幕密度dpi（120 / 160 / 240）

        float[] densitys = new float[2];
        densitys[0] = density;
        densitys[1] = densityDpi;
        return densitys;
    }


}
