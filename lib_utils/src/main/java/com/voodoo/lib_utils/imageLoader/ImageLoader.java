package com.voodoo.lib_utils.imageLoader;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.FutureTarget;
import com.bumptech.glide.request.RequestOptions;
import com.voodoo.lib_utils.L;
import com.voodoo.lib_utils.R;
import com.voodoo.lib_utils.imageLoader.transformation.BlurTransformation;
import com.voodoo.lib_utils.imageLoader.transformation.GrayscaleTransformation;
import com.voodoo.lib_utils.imageLoader.transformation.RoundedCornersTransformation;
import com.voodoo.lib_utils.AssetsUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;

/**
 * Author: voodoo
 * CreateDate: 2019/06/02 002 下午 10:50
 * Description: Glide工具类
 * 功能包括加载图片，圆形图片，圆角图片，指定圆角图片，模糊图片，灰度图片等等。
 * 目前我只加了这几个常用功能，其他请参考glide-transformations这个开源库。
 */
public class ImageLoader {

    private static final int placeholder = R.drawable.img_image_loading;
    private static final int error = R.drawable.img_image_load_error;

    /**
     * 默认加载图片
     *
     * @param context   上下文
     * @param url       图片地址url
     * @param imageView {@link ImageView}
     */
    public static void loadImage(Context context, String url, ImageView imageView) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(placeholder)
                .error(error)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(context).load(url).apply(options).into(imageView);
    }

    /**
     * 默认加载图片
     *
     * @param context   上下文
     * @param imgResId  资源图片
     * @param imageView {@link ImageView}
     */
    public static void loadImage(Context context, int imgResId, ImageView imageView) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(placeholder)
                .error(error)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(context).load(imgResId).apply(options).into(imageView);
    }

    /**
     * 加载Bitmap图片
     *
     * @param context   上下文
     * @param bitmap    Bitmap
     * @param imageView {@link ImageView}
     */
    public static void loadImage(Context context, Bitmap bitmap, ImageView imageView) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] bytes = baos.toByteArray();
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(placeholder)
                .error(error)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(context).load(bytes).apply(options).into(imageView);
    }

    /**
     * 加载指定图片大小，无论ImageView多大，Glide都会将图片设置成 width * height 大小的图片
     * 如果想加载一张图片的原始尺寸的话，可以使用Target.SIZE_ORIGINAL关键字----override(Target.SIZE_ORIGINAL)
     *
     * @param context   上下文
     * @param url       图片地址url
     * @param imageView {@link ImageView}
     * @param width     图片宽
     * @param height    图片高
     */
    public static void loadImageSize(Context context, String url, ImageView imageView, int width, int height) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(placeholder)
                .error(error)
                .override(width, height)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(context).load(url).apply(options).into(imageView);
    }

    /**
     * 加载圆形图片
     *
     * @param context   上下文
     * @param url       图片Url
     * @param imageView {@link ImageView}
     */
    public static void loadCircleImage(Context context, String url, ImageView imageView) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .circleCrop() // 设置圆形
                .placeholder(placeholder)
                .error(error)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(context).load(url).apply(options).into(imageView);
    }

    /**
     * 加载圆角图片
     *
     * @param context    上下文
     * @param url        图片Url
     * @param imageView  {@link ImageView}
     * @param radius     圆角半径
     * @param cornerType {@link RoundedCornersTransformation.CornerType}设置类别 ex.全圆角，左上圆角，左下圆角，上圆角，下圆角，对角圆角...
     */
    public static void loadRoundCircleImage(Context context, String url, ImageView imageView, int radius, RoundedCornersTransformation.CornerType cornerType) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .circleCrop() // 设置圆形
                .placeholder(placeholder)
                .error(error)
                .bitmapTransform(new RoundedCornersTransformation(radius, 0, cornerType))
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(context).load(url).apply(options).into(imageView);

    }

    /**
     * 加载模糊图片（自定义透明度）
     *
     * @param context   上下文
     * @param url       图片Url
     * @param imageView {@link ImageView}
     * @param blur      模糊度，一般1-100够了，越大越模糊
     */
    public static void loadBlurImage(Context context, String url, ImageView imageView, int blur) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(placeholder)
                .error(error)
                .bitmapTransform(new BlurTransformation(blur))
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(context).load(url).apply(options).into(imageView);
    }

    /**
     * 加载灰度(黑白)图片（自定义透明度）
     *
     * @param context   上下文
     * @param url       图片Url
     * @param imageView {@link ImageView}
     */
    public static void loadBlackImage(Context context, String url, ImageView imageView) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(placeholder)
                .error(error)
                .bitmapTransform(new GrayscaleTransformation())
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(context).load(url).apply(options).into(imageView);
    }

    /**
     * 下载图片，此处测试暂时失败
     *
     * @param context 上下文
     * @param url     图片Url
     */
    public static void downloadImage(final Context context, final String url) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    FutureTarget<File> target = Glide.with(context)
                            .asFile()
                            .load(url)
                            .submit();
                    final File imageFile = target.get();
                    L.i("logcat", "下载图片保存路径=" + imageFile.getPath());
                    ((Activity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(context, imageFile.getPath(), Toast.LENGTH_LONG).show();
                        }
                    });
                } catch (Exception e) {
                    L.e("图片下载出现异常：" + e.toString());
                }
            }
        }).start();
    }

    /**
     * 清除加载图片
     *
     * @param context   上下文
     * @param imageView ImageView
     */
    public static void clearImage(final Context context, ImageView imageView) {
        Glide.with(context).clear(imageView);
    }

}