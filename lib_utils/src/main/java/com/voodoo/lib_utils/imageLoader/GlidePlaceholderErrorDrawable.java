package com.voodoo.lib_utils.imageLoader;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;

/**
 * Author: voodoo
 * CreateDate: 2019/08/30 030 下午 02:57:20
 * Description: Glide进行placeholder和image进行过渡时,使用普通的资源placeholder会变形
 * 而这个类对图片资源进行了处理,使过渡前后placeholder一致
 */
public class GlidePlaceholderErrorDrawable extends Drawable {

    private final Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final float[] mMatrixValues = new float[9];
    private final int mWidth, mHeight;
    private final Bitmap mResource;

    public GlidePlaceholderErrorDrawable(Resources res, @DrawableRes int resource) {
        this(BitmapFactory.decodeResource(res, resource));
    }

    public GlidePlaceholderErrorDrawable(Bitmap resource) {
        this.mHeight = resource.getHeight();
        this.mWidth = resource.getWidth();
        this.mResource = resource;
    }

    @Override
    public int getMinimumHeight() {
        return mHeight;
    }

    @Override
    public int getMinimumWidth() {
        return mWidth;
    }

    @Override
    public void draw(Canvas canvas) {
        // canvas.getMatrix()这个方法已经@Deprecated了,但是这里要实现功能不得不用,缩放,位移啊,数据都在matrix里了
        Matrix matrix = canvas.getMatrix();
        matrix.getValues(mMatrixValues);
        // 由于缩放的中心是在左上角,而不是图片中心,故需要再平衡一下因为缩放造成的位移
        mMatrixValues[Matrix.MTRANS_X] = ((canvas.getWidth() - mWidth) / 2 - mMatrixValues[Matrix.MTRANS_X]) / mMatrixValues[Matrix.MSCALE_X];
        mMatrixValues[Matrix.MTRANS_Y] = ((canvas.getHeight() - mHeight) / 2 - mMatrixValues[Matrix.MTRANS_Y]) / mMatrixValues[Matrix.MSCALE_Y];
        // 尺寸反向缩放
        mMatrixValues[Matrix.MSCALE_X] = 1 / mMatrixValues[Matrix.MSCALE_X];
        mMatrixValues[Matrix.MSCALE_Y] = 1 / mMatrixValues[Matrix.MSCALE_Y];
        matrix.setValues(mMatrixValues);

        // 如果画布的宽高小于资源图的宽高
        if (mResource.getWidth() > canvas.getWidth() || mResource.getHeight() > canvas.getHeight()) {
            // 计算它们宽高的比值
            double widthRatio = canvas.getWidth() / (mResource.getWidth() * 1.0);
            double heightRatio = canvas.getHeight() / (mResource.getHeight() * 1.0);
            double ratio = 1; // 最终要使用到的比值
            if (widthRatio < heightRatio) {
                ratio = widthRatio;
            } else if (widthRatio > heightRatio) {
                ratio = heightRatio;
            } else {
                ratio = widthRatio;
            }
            // 根据先前的比值计算一下最新的资源图片的宽高
            int newWidth = (int) (mResource.getWidth() * ratio);
            int newHeight = (int) (mResource.getHeight() * ratio);

            // 根据计算出的新宽高创建出对应大小的资源图片
            Bitmap newBitmap = Bitmap.createScaledBitmap(mResource, newWidth, newHeight, true);
            int left = (canvas.getWidth() - newWidth) / 2;
            int top = (canvas.getHeight() - newHeight) / 2;
            // 将新的资源图绘制到指定Rect（第二个参数）的区域内，创建出来一个图片的绘制区域（第三个参数），居中
            canvas.drawBitmap(newBitmap, new Rect(0, 0, canvas.getWidth(), canvas.getHeight()), new Rect(left, top, left + newWidth, top + newHeight), mPaint);

        } else {
            canvas.drawBitmap(mResource, matrix, mPaint);
        }
    }

    @Override
    public void setAlpha(int i) {
    }

    @Override
    public void setColorFilter(ColorFilter colorFilter) {
    }

    @Override
    public int getOpacity() {
        return PixelFormat.UNKNOWN;
    }

}
