package com.voodoo.lib_utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Author: voodoo
 * CreateDate: 2020-03-26 026 下午 02:34
 * Description: 文件操作类
 */
public class FileUtils {

    /**
     * 将字符串写入文件
     *
     * @param filePath 文件
     * @param content  写入内容
     * @param isAppend 是否追加在文件末
     * @return {@code true}: 写入成功<br>{@code false}: 写入失败
     */
    public static boolean writeFileFromString(String filePath, String content, boolean isAppend) {
        if (filePath.isEmpty() || content.isEmpty()) {
            return false;
        }
        try {
            File file = new File(filePath);
            if (!file.exists()) {
                file.getParentFile().mkdirs(); // 创建文件目录
                file.createNewFile(); // 创建文件filename,只创建文件，不创建文件夹
            }
            FileWriter fileWriter = new FileWriter(file.getAbsoluteFile(), isAppend);
            BufferedWriter bw = new BufferedWriter(fileWriter);
            bw.write(content);
            bw.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
    /**
     * 获取文件夹下文件列表
     *
     * @param folderPath 文件夹的路径
     * @return 文件或文件夹的File列表集合
     */
    public static File[] getFolderFileList(String folderPath) {
        if (folderPath.isEmpty()) {
            return null;
        }
        File file = new File(folderPath);
        if (!file.exists() || file.isFile()) {
            return null;
        }
        String[] fileNamelist = file.list();
        File[] fileList = new File[fileNamelist.length];
        for (int i = 0; i < fileNamelist.length; i++) {
            fileList[i] = new File(folderPath + File.separator + fileNamelist[i]);
        }
        return fileList;
    }


    /**
     *  创建文件夹
     *
     * @param filePath 文件夹绝对地址

     */
    public static void CteateFileFromString(String filePath) {
        if (filePath.isEmpty()) {
            return;
        }
        File file = new File(filePath);
        if (!file.exists()) {
            file.mkdirs(); // 创建文件目录
        }
        return;
    }

}
