package com.voodoo.lib_utils;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Author: voodoo
 * CreateDate: 2020-03-27 027 下午 08:23
 * Description: Assets目录下文件操作
 */
public class AssetsUtils {

    private static MediaPlayer mediaPlayer; // 播放资源音频

    /**
     * 获取Assets文件夹下的图片Bitmap
     *
     * @param context     上下文
     * @param imgFileName 文件名
     * @return Bitmap
     */
    public static Bitmap getBitmap(Context context, String imgFileName) {
        Bitmap bitmap = null;
        try {
            InputStream inputStream = context.getAssets().open(imgFileName);
            bitmap = BitmapFactory.decodeStream(inputStream);
        } catch (IOException e) {
            L.e("获取Assets下图片异常：" + e.toString());
        }
        return bitmap;
    }

    /**
     * 获取Assets目录下的文件Url
     *
     * @param fileName 文件名称
     * @return Url  file:///android_asset/loading.gif
     */
    public static String getFileUrl(String fileName) {
        return "file:///android_asset/" + fileName;
    }

    /**
     * 读取Assets下text文件内容
     *
     * @param textFileName Text文件的名称
     * @return 文件内容
     */
    public static String getTextContent(Context context, String textFileName) {
        String fileContent = "";
        try {
            InputStream is = context.getAssets().open(textFileName);
            int lenght = is.available();
            byte[] buffer = new byte[lenght];
            is.read(buffer);
            fileContent = new String(buffer, "utf8");
        } catch (IOException e) {
            L.e("获取Assets下文件内容异常：" + e.toString());
            fileContent = "#获取文件内容异常：IOException";
        }
        return fileContent;
    }

    /**
     * 获取Assets文件中key对应的value值
     *
     * @param context      上下文
     * @param textFileName 文件名称
     * @param key          KEY
     * @param defultValue  默认值
     * @return String
     */
    public static String getJsonValueForKey(Context context, String textFileName, String key, String defultValue) {
        String textContent = getTextContent(context, textFileName);
        if (textContent.isEmpty() || key.isEmpty()) {
            return defultValue;
        }
        try {
            JSONObject contentJson = new JSONObject(textContent);
            return contentJson.optString(key, defultValue);
        } catch (JSONException e) {
            L.e("文件中内容不是Json格式的数据或Json格式异常");
            return defultValue;
        }
    }

    /**
     * 播放Assets下音频文件
     *
     * @param musicFileName 音频资源文件名称
     */
    public static void playMusic(Context context, String musicFileName) {
        try {
            AssetFileDescriptor afd = context.getAssets().openFd(musicFileName);
            if (mediaPlayer == null) {
                mediaPlayer = new MediaPlayer();
            } else {
                mediaPlayer.reset();
            }
            mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException e) {
            L.e("播放Assets下音频文件异常：" + e.toString());
        }
    }

    /**
     * 播放Assets下音频文件
     *
     * @param
     */
    public static File getFile(Context context, String fileName) {
        try {
            InputStream is = context.getClass().getResourceAsStream("/assets/" + fileName);
            FileOutputStream fos = context.openFileOutput(fileName, Context.MODE_PRIVATE + Context.MODE_WORLD_READABLE);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = is.read(buffer)) != -1) {
                fos.write(buffer, 0, len);
            }
            fos.flush();
            is.close();
            fos.close();
            File f = new File(context.getFilesDir().getPath() + "/" + fileName);
            return f;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
