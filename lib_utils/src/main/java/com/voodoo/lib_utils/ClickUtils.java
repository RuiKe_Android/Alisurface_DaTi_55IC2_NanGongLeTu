package com.voodoo.lib_utils;

/**
 * Created by admin on 2019/11/26.
 */

public class ClickUtils {

    private final static int DELAY = 500;
    private static long lastClickTime = 0;

    /**
     * 判断两次点击间隔，防止重复快速的点击
     *
     * @return true：两次点击间隔超过1000毫秒  false：1000毫秒内点击两次
     */
    public static boolean isFastClick() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - lastClickTime > DELAY) {
            lastClickTime = currentTime;
            return true;
        }
        return false;
    }

}
