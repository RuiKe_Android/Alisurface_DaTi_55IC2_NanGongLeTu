package com.voodoo.lib_utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Map;

/**
 * Author: voodoo
 * CreateDate: 2019/08/29 029 下午 04:24:03
 * Description: sharedPreferences工具类
 */
public class ShareUtils {

    private static final String SHARED_NAME = "RKSP";
    private SharedPreferences sharedPreferences;
    private static ShareUtils shareUtils;

    public static ShareUtils getInstance() {
        return shareUtils;
    }

    public static void init(Context context) {
        if (shareUtils == null) {
            synchronized (ShareUtils.class) {
                if (shareUtils == null) {
                    shareUtils = new ShareUtils(context.getApplicationContext());
                }
            }
        }
    }

    public ShareUtils(Context context) {
        sharedPreferences = context.getSharedPreferences( SHARED_NAME, Context.MODE_PRIVATE);
    }

    // =============================== put ===============================

    /**
     * 存入 String 值
     *
     * @param keyStr   Key
     * @param valueStr Value
     */
    public void putString(String keyStr, String valueStr) {
        sharedPreferences.edit().putString(keyStr, valueStr).apply();
    }

    /**
     * 存入 Int 值
     *
     * @param keyStr   Key
     * @param valueStr Value
     */
    public void putInt(String keyStr, int valueStr) {
        sharedPreferences.edit().putInt(keyStr, valueStr).apply();
    }

    /**
     * 存入 Boolean 值
     *
     * @param keyStr   Key
     * @param valueStr Value
     */
    public void putBoolean(String keyStr, boolean valueStr) {
        sharedPreferences.edit().putBoolean(keyStr, valueStr).apply();
    }

    /**
     * 存入 Float 值
     *
     * @param keyStr   Key
     * @param valueStr Value
     */
    public void putFloat(String keyStr, float valueStr) {
        sharedPreferences.edit().putFloat(keyStr, valueStr).apply();
    }

    /**
     * 存入 Long 值
     *
     * @param keyStr   Key
     * @param valueStr Value
     */
    public void putLong(String keyStr, long valueStr) {
        sharedPreferences.edit().putLong(keyStr, valueStr).apply();
    }

    // =============================== get ===============================

    /**
     * 读取 String 的值
     *
     * @param keyStr 保存的Key
     * @return 保存的Value
     */
    public String getString(String keyStr) {
        return getString(keyStr, "");
    }

    /**
     * 读取 Int 的值
     *
     * @param keyStr 保存的Key
     * @return 保存的Value
     */
    public int getInt(String keyStr) {
        return getInt(keyStr, 0);
    }

    /**
     * 读取 Boolean 的值
     *
     * @param keyStr 保存的Key
     * @return 保存的Value
     */
    public boolean getBoolean(String keyStr) {
        return getBoolean(keyStr, false);
    }

    /**
     * 读取 Float 的值
     *
     * @param keyStr 保存的Key
     * @return 保存的Value
     */
    public float getFloat(String keyStr) {
        return getFloat(keyStr, 0F);
    }

    /**
     * 读取 Long 的值
     *
     * @param keyStr 保存的Key
     * @return 保存的Value
     */
    public long getLong(String keyStr) {
        return getLong(keyStr, 0L);
    }

    // =============================== KEY, DEF_VALUE ===============================

    /**
     * 读取 String 的值
     *
     * @param keyStr   保存的Key
     * @param defValue 默认Value
     * @return 保存的Value
     */
    public String getString(String keyStr, String defValue) {
        return sharedPreferences.getString(keyStr, defValue);
    }

    /**
     * 读取 Int 的值
     *
     * @param keyStr   保存的Key
     * @param defValue 默认Value
     * @return 保存的Value
     */
    public int getInt(String keyStr, int defValue) {
        return sharedPreferences.getInt(keyStr, defValue);
    }

    /**
     * 读取 Boolean 的值
     *
     * @param keyStr   保存的Key
     * @param defValue 默认Value
     * @return 保存的Value
     */
    public boolean getBoolean(String keyStr, boolean defValue) {
        return sharedPreferences.getBoolean(keyStr, defValue);
    }

    /**
     * 读取 Float 的值
     *
     * @param keyStr   保存的Key
     * @param defValue 默认Value
     * @return 保存的Value
     */
    public float getFloat(String keyStr, float defValue) {
        return sharedPreferences.getFloat(keyStr, defValue);
    }

    /**
     * 读取 Long 的值
     *
     * @param keyStr   保存的Key
     * @param defValue 默认Value
     * @return 保存的Value
     */
    public long getLong(String keyStr, long defValue) {
        return sharedPreferences.getLong(keyStr, defValue);
    }

    // =============================== 其他 ===============================

    /**
     * 删除单个key
     *
     * @param keyStr 要删除的键值
     */
    public void delShare(String keyStr) {
        sharedPreferences.edit().remove(keyStr).apply();
    }

    /**
     * 删除全部的key
     */
    public void delAll() {
        sharedPreferences.edit().clear().apply();
    }

    /**
     * 查询某个key是否已经存在
     *
     * @param key key关键字
     * @return 包含返回true；反之返回false
     */
    public boolean containsKey(String key) {
        return sharedPreferences.contains(key);
    }

    /**
     * 获取所有的键值对
     */
    public Map<String, ?> getAll() {
        return sharedPreferences.getAll();
    }

}
